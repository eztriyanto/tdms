﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using static TDMS.Mobile.Layout.GlobalVar;

namespace TDMS.Mobile.Layout
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AboutPage : ContentPage
	{
    public static string SiteId { get => CurrentSetting.SiteData.SiteId; }
    public static string SiteName { get => CurrentSetting.SiteData.SiteName; }
    public static string Server { get => CurrentSetting.Server; }
    public static string Version { get => "Versi " + AppInfo.VersionString + 
        " build " + AppInfo.BuildString; }

    public AboutPage()
		{
			InitializeComponent();

      BindingContext = this;
    }

    protected override void OnAppearing()
    {
      base.OnAppearing();

      ctlNavBar.SetMenuVisibility(false);
    }
  }
}
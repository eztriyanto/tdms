﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace TDMS.Mobile.Layout
{
  [XamlCompilation(XamlCompilationOptions.Compile)]
  public partial class ScanQRPage : ContentPage
  {
    public ZXing.Result Result { get; set; }
    public bool IsAnalyzing { get; set; }

    public Command ScanResultCommand
    {
      get
      {
        return new Command(() =>
        {
          IsAnalyzing = false;

          MainThread.BeginInvokeOnMainThread(async () =>
          {
            MessagingCenter.Send<object, string>(this, "Scanned", Result?.Text);
            await GlobalVar.mainPage.Navigation.PopAsync().ConfigureAwait(false);
          });
        });
      }
    }
    public ScanQRPage()
    {
      InitializeComponent();

      vwCamera.Options = new ZXing.Mobile.MobileBarcodeScanningOptions { 
        PossibleFormats = new System.Collections.Generic.List<ZXing.BarcodeFormat> { 
          ZXing.BarcodeFormat.QR_CODE
        }
      };

      BindingContext = this;
    }

    private void vwCameraOverlay_FlashButtonClicked(Button sender, EventArgs e)
    {
      vwCamera.IsTorchOn = !vwCamera.IsTorchOn;
    }
  }
}

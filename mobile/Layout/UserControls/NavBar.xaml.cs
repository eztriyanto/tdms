﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace TDMS.Mobile.Layout.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NavBar : ContentView
	{
		private string _ImgSource = "logout";
		public string ImgSource
		{
			protected set
			{
				if (_ImgSource != value)
				{
					_ImgSource = value;
					OnPropertyChanged(nameof(ImgSource));
				}
			}
			get { return _ImgSource; }
		}
		public void SetMenuVisibility(bool visible)
		{
			imgMenu.IsVisible = visible;
		}
		public void SetMenuToClose()
		{
			imgMenu.IsVisible = true;
			ImgSource = "cross";
		}

		public NavBar()
		{
			InitializeComponent();

			BindingContext = this; 
		}

    private async void ImgMenu_Tapped(object sender, EventArgs e)
    {
			await GlobalFunc.AnimateOnTap((Image)sender).ConfigureAwait(false);

			if (ImgSource == "logout")
			{
				GlobalFunc.SaveSetting("SessionId", "");
				MainThread.BeginInvokeOnMainThread(() => {
					var stacks = Application.Current.MainPage.Navigation.NavigationStack;
					foreach (var stack in stacks)
          {
						if (stack.GetType() == typeof(MainPage))
							foreach (var tp in ((MainPage)stack).TankList)
								tp.Dispose();
					}
				});

				MainThread.BeginInvokeOnMainThread(() => {
					GlobalVar.mainPage = new LoginPage();
					Application.Current.MainPage = new NavigationPage(GlobalVar.mainPage);
				});
			}
			else
			{
				DependencyService.Get<Services.IService>().CloseApp();
			}
    }
  }
}
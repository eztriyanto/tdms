﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace TDMS.Mobile.Layout.Controls
{
  public class GradientColorFrame : Frame
  {
    public string StartColor { get; set; }
    public string EndColor { get; set; }
  }
}

﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

using Newtonsoft.Json;
using TDMS.Library.Models;

namespace TDMS.Mobile.Layout.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TankPicker : ContentView, IDisposable
  {
    private StoppableTimer TimerData;

    private Tank _TankData = null;
    public Tank TankData
    {
      get => _TankData;
      set
      {
        _TankData = value;
        OnPropertyChanged(nameof(TankData));
      }
    }

    private void SetTank()
    {
      if (TankData != null)
      {
        MainThread.BeginInvokeOnMainThread(() => {
          var maxBlock = frmTank.HeightRequest + frmTank.Margin.Top;
          imgTank.HeightRequest = (int)Math.Floor((double)((100 - TankData.UllageInPercent) / 100) * maxBlock);
          imgTank.Source = GlobalFunc.ProductColor(TankData.Grade.ProductName);
        });
      }
    }

    public TankPicker(Tank tankData)
		{
			InitializeComponent ();

      TankData = tankData;
      SetTank();

      TimerData = new StoppableTimer(TimeSpan.FromSeconds(GlobalConst.TankRefresh), TimerData_Elapsed);
      if (TankData.IsGauge)
        TimerData.Start();
      
      BindingContext = this;      
    }

    private async void TimerData_Elapsed()
    {
      try
      {
        var resp = await GlobalVar.api.GetTank(TankData.Number).ConfigureAwait(false);
        if (resp.status == 200 && resp.data != null)
        {
          TankData = JsonConvert.DeserializeObject<Tank>(resp.data.ToString());
          SetTank();
        }
      }
      catch { }
    }

    private async void IBtnAdd_Tapped(object sender, EventArgs e)
    {
      await GlobalFunc.AnimateOnTap((Image)sender).ConfigureAwait(false);

      if (TankData != null)
      {
        if (!TankData.IsOnline)
        {
          GlobalFunc.ToastWarning($"Tank {TankData.Name} offline, please check ATG probe " + 
            "or console connection");
          return;
        }

        GlobalFunc.Navigate2Page(new TankDeliveryPage(TankData));
        return;
      }

      GlobalFunc.ToastError("Invalid tank data");
    }

    public void Dispose()
    {
      TimerData?.Stop();
      TimerData?.Dispose();

      GC.SuppressFinalize(this);
    }
  }
}
﻿using System;
using Xamarin.Forms;

namespace TDMS.Mobile.Layout
{
  public partial class QRDisplayPage : ContentPage
  {
    public QRDisplayPage()
    {
      InitializeComponent();

      imgBarcode.BarcodeValue = "{\"depot\":\"TBBM TEGAL\",\"trans\":\"PT.PATRA NIAGA\",\"noPol\":\"B1234CBF\",\"supir\":\"Rahmat Amin\",\"noLO\":\"45012345\",\"prod\":\"PERTAMAX TURBO\",\"vol\":\"8\"}";
    }

    protected override void OnAppearing()
    {
      base.OnAppearing();
    }
  }
}

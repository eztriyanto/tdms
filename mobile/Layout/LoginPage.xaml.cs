﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

using Acr.UserDialogs;
using Newtonsoft.Json;

namespace TDMS.Mobile.Layout
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
    public LoginPage()
		{
			InitializeComponent();

      ctlNavBar.SetMenuToClose();
      txtServer.Text = GlobalVar.CurrentSetting.Server;
      txtId.Text = GlobalVar.CurrentSetting.UserId;
    }

    protected override void OnAppearing()
    {
      base.OnAppearing();

#if DEBUG
      txtServer.IsReadOnly = false;
#else
      txtServer.IsReadOnly = true;
#endif
    }
    protected override bool OnBackButtonPressed()
    {
      DependencyService.Get<Services.IService>().CloseApp();
      return true;
    }

    private async void DivLogin_Tapped(object sender, EventArgs e)
    {
      await GlobalFunc.AnimateOnTap((Frame)sender).ConfigureAwait(false);

      if (string.IsNullOrEmpty(txtId.Text) || string.IsNullOrEmpty(txtPwd.Text))
      {
        GlobalFunc.ToastWarning("Invalid user id or password");
        return;
      }

#if DEBUG
      GlobalVar.CurrentSetting.Server = txtServer.Text;
#endif

      UserDialogs.Instance.ShowLoading("Verifying");
      var resp = await GlobalVar.api.Login(txtId.Text, txtPwd.Text).ConfigureAwait(false);
      UserDialogs.Instance.HideLoading();

      if (resp.status == 200 && resp.data != null)
      {
        try
        {
          UserDialogs.Instance.ShowLoading("Getting Configuration Data");

          var att = JsonConvert.DeserializeObject<Library.Models.Attendant>(resp.data.ToString());

          GlobalVar.CurrentSetting.AttendantId = att.Id;
          GlobalVar.CurrentSetting.UserId = att.LogonId;
          GlobalVar.CurrentSetting.SessionId = att.SessionId;

          //Get Site information
          resp = await GlobalVar.api.GetSite().ConfigureAwait(false);
          if (resp.status == 200 && resp.data != null)
            GlobalVar.CurrentSetting.SiteData = JsonConvert.DeserializeObject<Library.Models.Site>(resp.data.ToString());

          Task.Run(() => { GlobalFunc.SaveSetting(); }).Wait();
          
          UserDialogs.Instance.HideLoading();

          MainThread.BeginInvokeOnMainThread(() =>
          {
            GlobalVar.mainPage = new MainPage();
            Application.Current.MainPage = new NavigationPage(GlobalVar.mainPage);
          });
        }
        catch (Exception ex)
        {
          UserDialogs.Instance.HideLoading();
          GlobalFunc.ToastError(Library.Helper.ErrorMsg(ex));
        }
      }
      else
      {
        GlobalFunc.ToastError(resp.errors[0].message);
      }
    }
  }
}
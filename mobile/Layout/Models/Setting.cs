﻿using System;

namespace TDMS.Mobile.Layout.Models
{
  public class Setting
  {
    public Setting()
    {
      Server = "";
      UserId = "";
      AttendantId = -1;
      SessionId = "";
      SiteData = new Library.Models.Site
      {
        SiteId = "",
        SiteName = "",
        SiteAddress = ""
      };
    }

    public string Server { get; set; }
    public int AttendantId { get; set; }
    public string UserId { get; set; }
    public string SessionId { get; set; }
    public Library.Models.Site SiteData { get; set; }
  }
}

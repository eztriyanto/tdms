﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDMS.Mobile.Layout.Models
{
  public class QRData
  {
    public string depot { get; set; } = "";
    public string trans { get; set; } = "";
    public string noPol { get; set; } = "";
    public string supir { get; set; } = "";
    public string noLO { get; set; } = "";
    public string prod { get; set; } = "";
    public double vol { get; set; } = 0;

    public QRData()
    {

    }
  }
}

﻿using System;
using System.Threading;
using Xamarin.Forms;

namespace TDMS.Mobile.Layout
{
  public class StoppableTimer : IDisposable
  {
    private bool isDisposed;
    private readonly TimeSpan timespan;
    private readonly Action callback;

    private CancellationTokenSource cancellation;

    public StoppableTimer(TimeSpan timespan, Action callback)
    {
      this.timespan = timespan;
      this.callback = callback;
      this.cancellation = new CancellationTokenSource();
    }

    public void Start()
    {
      CancellationTokenSource cts = this.cancellation; // safe copy
      Device.StartTimer(this.timespan, () => {
        if (cts.IsCancellationRequested) return false;
        this.callback.Invoke();
        return true; // false for one time behavior or true for periodic
      });
    }

    public void Stop()
    {
      Interlocked.Exchange(ref this.cancellation, new CancellationTokenSource()).Cancel();
    }

    protected virtual void Dispose(bool disposing)
    {
      if (isDisposed) return;

      if (disposing)
      {
        cancellation.Dispose();
      }

      isDisposed = true;
    }
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
  }
}

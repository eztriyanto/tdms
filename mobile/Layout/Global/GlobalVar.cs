﻿using System;
using System.Net;
using TDMS.Mobile.Layout.Controllers;
using Xamarin.Forms;

namespace TDMS.Mobile.Layout
{
  public static class GlobalVar
  {
    public static readonly System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

    internal static ApiConsumer api = new ApiConsumer(); 

    internal static ContentPage mainPage = null;

    internal static string IPAddress
    {
      get
      {
        IPAddress[] adresses = Dns.GetHostAddresses(Dns.GetHostName());

        if (adresses != null && adresses[0] != null)
        {
          var result = adresses[0].ToString();
          return result.Substring(0, result.LastIndexOf('.') + 1) + "3";
        }
        else
        {
          return "";
        }
      }
    }

    internal static Models.Setting CurrentSetting = new Models.Setting();

    internal static bool IsSessionExpired
    {
      get
      {
        if (CurrentSetting != null && !string.IsNullOrEmpty(CurrentSetting.SessionId))
        {
          var resp = api.SessionValidate().GetAwaiter().GetResult();
          return (resp.status != 200);
        }

        return true;
      }
    }
  }
}

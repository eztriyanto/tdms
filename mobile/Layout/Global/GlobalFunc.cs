﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Essentials;

using Newtonsoft.Json;
using Acr.UserDialogs;

using static TDMS.Mobile.Layout.GlobalConst;
using static TDMS.Mobile.Layout.GlobalVar;

namespace TDMS.Mobile.Layout
{
  public static class ProgressBarEx
  {
    internal static BindableProperty AnimatedProgressProperty =
       BindableProperty.CreateAttached("AnimatedProgress",
                                       typeof(double),
                                       typeof(ProgressBar),
                                       0.0d,
                                       BindingMode.OneWay,
                                       propertyChanged: (b, o, n) =>
                                       ProgressBarProgressChanged((ProgressBar)b, (double)n));

    private static void ProgressBarProgressChanged(ProgressBar progressBar, double progress)
    {
      ViewExtensions.CancelAnimations(progressBar);
      progressBar.ProgressTo((double)progress, 250, Easing.Linear);
    }
  }

  public static class GlobalFunc
  {    
    public static bool SaveSetting()
    {
      if (CurrentSetting != null)
      {
        Preferences.Set("AttendantId", CurrentSetting.AttendantId);
        Preferences.Set("UserId", CurrentSetting.UserId);
        Preferences.Set("SessionId", CurrentSetting.SessionId);
        Preferences.Set("Site", JsonConvert.SerializeObject(CurrentSetting.SiteData));
        Preferences.Set("Server", string.IsNullOrEmpty(CurrentSetting.Server) ? "" : CurrentSetting.Server);
        return true;
      }

      return false;
    }
    public static bool SaveSetting(string key, string value)
    {
      if (!string.IsNullOrEmpty(key) && value != null)
      {
        Preferences.Set(key, value);
        return true;
      }

      return false;
    }
    public static Models.Setting ReadSetting()
    {
      return new Models.Setting
      {
        AttendantId = Preferences.Get("AttendantId", -1),
        UserId = Preferences.Get("UserId", ""),
        SessionId = Preferences.Get("SessionId", ""),
        SiteData = JsonConvert.DeserializeObject<Library.Models.Site>(Preferences.Get("Site", JsonConvert.SerializeObject(new Library.Models.Site()))),
        Server = Preferences.Get("Server", "")
      };
    }

    public static string ProductLogo(string ProductName)
    {
      switch(ProductName.ToUpper())
      {
        case "VIGAS":
          return "vigas";

        case "BIO_SOLAR":
        case "SOLAR":
          return "bio_solar";

        case "PREMIUM":
          return "premium";

        case "PERTALITE":
          return "pertalite";

        case "PERTAMAX":
          return "pertamax";

        case "PERTAMAX_TURBO":
          return "pertamax_turbo";

        case "PERTAMAX_RACING":
          return "pertamax_racing";

        case "DEXLITE":
          return "dexlite";

        case "PERTAMINA_DEX":
          return "pertamina_dex";

        default:
          return "no_product";
      }
    }

    public static string ProductColor(string ProductName)
    {
      switch (ProductName.ToUpper())
      {
        case "VIGAS":
          return "vigas_block";

        case "SOLAR_NPSO":
        case "BIO_SOLAR":
        case "SOLAR":
          return "bio_solar";

        case "PREMIUM":
          return "premium_block";

        case "PERTALITE":
        case "PERTALITE_KHUSUS":
          return "pertalite_block";

        case "PERTAMAX":
          return "pertamax_block";

        case "PERTAMAX_TURBO":
          return "pertamax_turbo_block";

        case "PERTAMAX_RACING":
          return "pertamax_racing_block";

        case "DEXLITE":
          return "dexlite";

        case "PERTAMINA_DEX":
          return "pertamina_dex_block";

        default:
          return "no_product";
      }
    }

    public async static Task AnimateOnTap(Image Obj)
    {
      var scaleUp = Obj.ScaleTo(1.2, 100);
      var fadeOutAnimationTask = Obj.FadeTo(0, 100);

      await Task.WhenAll(scaleUp, fadeOutAnimationTask).ConfigureAwait(false);

      var scaleDown = Obj.ScaleTo(1, 100);
      var fadeIn = Obj.FadeTo(1, 100);

      await Task.WhenAll(scaleDown, fadeIn).ConfigureAwait(false);
    }
    public async static Task AnimateOnTap(Frame Obj)
    {
      var scaleUp = Obj.ScaleTo(1.2, 100);
      var fadeOutAnimationTask = Obj.FadeTo(0, 100);

      await Task.WhenAll(scaleUp, fadeOutAnimationTask).ConfigureAwait(false);

      var scaleDown = Obj.ScaleTo(1, 100);
      var fadeIn = Obj.FadeTo(1, 100);

      await Task.WhenAll(scaleDown, fadeIn).ConfigureAwait(false);
    }
    public async static Task AnimateOnTap(StackLayout Obj)
    {
      var scaleUp = Obj.ScaleTo(1.2, 100);
      var fadeOutAnimationTask = Obj.FadeTo(0, 100);

      await Task.WhenAll(scaleUp, fadeOutAnimationTask).ConfigureAwait(false);

      var scaleDown = Obj.ScaleTo(1, 100);
      var fadeIn = Obj.FadeTo(1, 100);

      await Task.WhenAll(scaleDown, fadeIn).ConfigureAwait(false);
    }
    public async static Task AnimateOnTap(Controls.TankPicker Obj)
    {
      var scaleUp = Obj.ScaleTo(1.2, 100);
      var fadeOutAnimationTask = Obj.FadeTo(0, 100);

      await Task.WhenAll(scaleUp, fadeOutAnimationTask).ConfigureAwait(false);

      var scaleDown = Obj.ScaleTo(1, 100);
      var fadeIn = Obj.FadeTo(1, 100);

      await Task.WhenAll(scaleDown, fadeIn).ConfigureAwait(false);
    }

    public static void Navigate2Root()
    {
      MainThread.BeginInvokeOnMainThread(async () =>
      {
        await Application.Current.MainPage.Navigation.PopToRootAsync().ConfigureAwait(false);
      });
    }
    public static void Navigate2Page(Page page, string LoadingMessage = LoadingMessage)
    {
      MainThread.BeginInvokeOnMainThread(async () =>
      {
        var stack = Application.Current.MainPage.Navigation.NavigationStack;
        if (stack[stack.Count - 1].GetType() != page.GetType())
        {
          UserDialogs.Instance.ShowLoading(LoadingMessage);
          await Application.Current.MainPage.Navigation.PushAsync(page).ConfigureAwait(false);
          UserDialogs.Instance.HideLoading();
        }
      });
    }
    public static void Navigate2ModalPage(Page page, string LoadingMessage = LoadingMessage)
    {
      MainThread.BeginInvokeOnMainThread(async () =>
      {
        UserDialogs.Instance.ShowLoading(LoadingMessage);
        await Application.Current.MainPage.Navigation.PushModalAsync(page).ConfigureAwait(false);
        UserDialogs.Instance.HideLoading();
      });
    }

    private static void DisplayToast(ToastConfig cfg)
    {
      cfg.Duration = TimeSpan.FromSeconds(5);
      cfg.Position = ToastPosition.Top;

      MainThread.BeginInvokeOnMainThread(() => {
        UserDialogs.Instance.Toast(cfg);
      });
    }
    public static void ToastError(string Message)
    {
      var cfg = new ToastConfig(Message)
      {
        MessageTextColor = System.Drawing.Color.White,
        BackgroundColor = System.Drawing.Color.FromArgb(217, 83, 79)
      };

      DisplayToast(cfg);
    }
    public static void ToastError(Exception ex)
    {
      var msg = "Error !!!";
      if (ex != null)
        msg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;

      ToastError(msg);
    }
    public static void ToastWarning(string Message)
    {
      var cfg = new ToastConfig(Message)
      {
        MessageTextColor = System.Drawing.Color.White,
        BackgroundColor = System.Drawing.Color.FromArgb(240, 173, 78)
      };

      DisplayToast(cfg);
    }
    public static void ToastSuccess(string Message)
    {
      var cfg = new ToastConfig(Message)
      {
        MessageTextColor = System.Drawing.Color.White,
        BackgroundColor = System.Drawing.Color.FromArgb(92, 184, 92)
      };

      DisplayToast(cfg);
    }
  }
}

﻿using System;

namespace TDMS.Mobile.Layout
{
  public static class GlobalConst
  {
    public const string token = "gQQRoTBLVUGRJ4YGBjIEzw";

    public const string LoadingMessage = "Loading...";

    public const string dtFormat = "yyyy-MM-dd HH:mm:ss";

    public const int TankRefresh = 15; //in seconds
  }
}

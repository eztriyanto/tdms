﻿using System;
using System.Collections.Generic;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Essentials;
using TDMS.Mobile.Layout.Controls;

namespace TDMS.Mobile.Layout
{
  public partial class MainPage : ContentPage
  {
    public List<TankPicker> TankList = new List<TankPicker>();

    public string SiteName { get => "SPBU " + GlobalVar.CurrentSetting.SiteData.SiteId; }

    public string SiteAddress { get => GlobalVar.CurrentSetting.SiteData.SiteAddress; }

    private async void GetTanks()
    {
      try
      {
        var resp = await GlobalVar.api.GetTanks().ConfigureAwait(false);
        if (resp.status == 200 && resp.data != null)
        {
          TankList = new List<TankPicker>();
          var Tanks = JsonConvert.DeserializeObject<List<Library.Models.Tank>>(resp.data.ToString());
          if (Tanks != null && Tanks.Count > 0)
          {
            foreach (var tank in Tanks)
            {
              var t = new TankPicker(tank);
              TankList.Add(t);
            }
          }

          MainThread.BeginInvokeOnMainThread(() => {
            lsTank.Children.Clear();
            foreach (var tv in TankList)
              lsTank.Children.Add(tv);
          });
        }
      }
      catch (Exception ex)
      {
        GlobalFunc.ToastError(ex);
      }
    }

    public MainPage()
    {
      InitializeComponent();

      UserDialogs.Instance.ShowLoading("Preparing Display");
      ctlNavBar.SetMenuVisibility(true);
      GetTanks();
      UserDialogs.Instance.HideLoading();

      BindingContext = this;
    }

    protected override void OnAppearing()
    {
      base.OnAppearing();

      if (GlobalVar.IsSessionExpired)
      {
        GlobalFunc.ToastWarning("Session expired. Please login again");

        MainThread.BeginInvokeOnMainThread(() => {
          GlobalVar.mainPage = new LoginPage();
          Application.Current.MainPage = new NavigationPage(GlobalVar.mainPage);
        });
      }
    }
    protected override bool OnBackButtonPressed()
    {
      DependencyService.Get<Services.IService>().CloseApp();
      return true;
    }
    protected override void OnDisappearing()
    {
      base.OnDisappearing();
    }
  }
}

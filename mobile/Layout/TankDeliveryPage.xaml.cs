﻿using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Essentials;

using Newtonsoft.Json;
using Acr.UserDialogs;

using TDMS.Library.Models;

namespace TDMS.Mobile.Layout
{
  public partial class TankDeliveryPage : ContentPage, IDisposable
  {
    private bool isDisposed;
    private StoppableTimer TimerData;
    private StoppableTimer TimerBlink;

    private Tank _TankData = null;
    public Tank TankData
    {
      get => _TankData;
      set
      {
        _TankData = value;
        OnPropertyChanged(nameof(TankData));
      }
    }

    private TankDelivery _DeliveryData = null;
    public TankDelivery DeliveryData
    {
      get => _DeliveryData;
      set
      {
        _DeliveryData = value;
        OnPropertyChanged(nameof(DeliveryData));
      }
    }

    private async Task GetDelivery()
    {
      var resp = await GlobalVar.api.GetTankDelivery(TankData.DeliveryId).ConfigureAwait(false);
      if (resp.status == 200 && resp.data != null)
      {
        DeliveryData = JsonConvert.DeserializeObject<TankDelivery>(resp.data.ToString());
        MainThread.BeginInvokeOnMainThread(() =>
        {
          txtDONumber.Text = DeliveryData.OriginalInvoiceNumber;
          txtDOVolume.Text = $"{DeliveryData.DispatchedVolume:N0}";
          txtDOVolume.StyleId = $"{DeliveryData.DispatchedVolume:F3}";
          txtSupplyPoint.Text = DeliveryData.DeliveryNoteNum;
          txtSupplier.Text = DeliveryData.DeliveryDetail;
          txtVehicleNumber.Text = DeliveryData.TankerIdCode;
          txtDriverName.Text = DeliveryData.DriverIdCode;
          txtDensity.Text = "0.0000";
        });
      }
    }

    private void SetTank()
    {
      if (TankData != null)
      {
        MainThread.BeginInvokeOnMainThread(() => {
          var maxBlock = frmTank.HeightRequest - frmTank.Margin.Top - frmTank.Margin.Bottom;
          imgTank.HeightRequest = Math.Floor((double)((100 - TankData.UllageInPercent) / 100) * maxBlock);
          imgTank.Source = GlobalFunc.ProductColor(TankData.Grade.ProductName);

          if (TankData.IsDelivering)
          {
            var dropVol = (double)TankData.Volume - DeliveryData.ReceivedVolAtRefTemp;
            lblDropVolume.Text = $"{dropVol:N2} L";

            //Check Loss/Gain
            var lg = Math.Abs(dropVol - DeliveryData.DispatchedVolume) / DeliveryData.DispatchedVolume * 100;
            lblDropVolume.TextColor = Color.Red;
            if (lg <= 0.5)
              lblDropVolume.TextColor = Color.Green;
          }

          divStartInfo.IsVisible = TankData.IsDelivering;
        });
      }
    }

    private void ScannerMessanging()
    {
      MessagingCenter.Subscribe<object, string>(this, "Scanned", (sender, arg) =>
      {
        var dt = new Models.QRData();
        try
        {
          dt = JsonConvert.DeserializeObject<Models.QRData>(arg);
        }
        catch { }

        if (string.IsNullOrEmpty(dt.noLO))
          GlobalFunc.ToastError("Invalid QR Code");
        else
          GlobalFunc.ToastSuccess("Scanning QR Code successfully");

        MainThread.BeginInvokeOnMainThread(() =>
        {
          var vol = dt.vol * 1000;
          txtDONumber.Text = dt.noLO;
          txtDOVolume.Text = $"{vol:N0}";
          txtDOVolume.StyleId = $"{vol:F3}";
          txtSupplyPoint.Text = dt.depot;
          txtSupplier.Text = dt.trans;
          txtVehicleNumber.Text = dt.noPol;
          txtDriverName.Text = dt.supir;
          txtDensity.Text = "0.0000";
        });
      });
    }

    public TankDeliveryPage(Tank tankData)
    {
      InitializeComponent();

      UserDialogs.Instance.ShowLoading("Preparing Display");

      TankData = tankData;
      if (TankData.IsDelivering)
      {
        GetDelivery().Wait();
        btnActionText.Text = $"Finish Tank Delivery #{TankData.DeliveryId}";
      }
      else
      {
        btnActionText.Text = "Start Tank Delivery";
      }

      SetTank();

      if (TankData.IsGauge)
      {
        if (TimerData == null)
          TimerData = new StoppableTimer(TimeSpan.FromSeconds(GlobalConst.TankRefresh), TimerData_Elapsed);

        TimerData.Start();
      }
      
      if (TimerBlink == null)
        TimerBlink = new StoppableTimer(TimeSpan.FromSeconds(1), TimerBlink_Elapsed);

      TimerBlink.Start();
      UserDialogs.Instance.HideLoading();

      BindingContext = this;

      ScannerMessanging();
    }

    private async void TimerData_Elapsed()
    {
      try
      {
        var resp = await GlobalVar.api.GetTank(TankData.Number).ConfigureAwait(false);
        if (resp.status == 200 && resp.data != null)
        {
          TankData = JsonConvert.DeserializeObject<Tank>(resp.data.ToString());
          SetTank();
        }
      }
      catch { }
    }

    private void TimerBlink_Elapsed()
    {
      MainThread.BeginInvokeOnMainThread(() => {
        lblVolumeInfo.IsVisible = !lblVolumeInfo.IsVisible;
      });
    }

    protected override void OnAppearing()
    {
      base.OnAppearing();
    }
    protected override bool OnBackButtonPressed()
    {
      this.Dispose();

      return false;
    }
    protected override void OnDisappearing()
    {
      base.OnDisappearing();
    }

    private async void DivScan_Tapped(object sender, EventArgs e)
    {
      await GlobalFunc.AnimateOnTap((Frame)sender).ConfigureAwait(false);

      GlobalFunc.Navigate2Page(new ScanQRPage());

      this.Dispose();
    }

    private async void DivAction_Tapped(object sender, EventArgs e)
    {
      await GlobalFunc.AnimateOnTap((Frame)sender).ConfigureAwait(false);

      //Start delivering
      if (btnActionText.Text.StartsWith("Start"))
      {
        //Check DO Data
        if (string.IsNullOrEmpty(txtDONumber.Text))
        {
          GlobalFunc.ToastWarning("Invalid delivery order data, please scan valid QR Code");
          return;
        }

        MainThread.BeginInvokeOnMainThread(async () => {
          var confirm = await DisplayAlert("Confirmation", "Are you sure want to start tank delivery " +
            $"for tank {TankData.Name}, please make sure STOCK is correct?", "Yes", "No").ConfigureAwait(false);
          if (confirm)
          {
            try
            {
              var resp = await GlobalVar.api.StartTankDelivery(TankData.Id, (double)TankData.Volume,
                DeliveryData.DispatchedVolume, DeliveryData.OriginalInvoiceNumber,
                DeliveryData.DriverIdCode, DeliveryData.TankerIdCode,
                DeliveryData.DeliveryNoteNum, DeliveryData.DeliveryDetail,
                GlobalVar.CurrentSetting.AttendantId).ConfigureAwait(false);
              if (resp.status == 200)
              {
                GlobalFunc.Navigate2Root();
                GlobalFunc.ToastSuccess($"Delivery for tank #{TankData.Number} successfully started");
                this.Dispose();
              }
              else
              {
                GlobalFunc.ToastWarning(resp.errors[0].message);
              }
            }
            catch (Exception ex)
            {
              GlobalFunc.ToastError(ex);
            }
          }
        });
      }

      //Finish delivering
      else
      {
        MainThread.BeginInvokeOnMainThread(async () => {
          //Check Drop Volume 
          var EndStock = 0;

          var confirm = await DisplayAlert("Confirmation", "Are you sure want to finish tank delivery " + 
            $"for tank {TankData.Name}, please make sure STOCK is correct ?", "Yes", "No").ConfigureAwait(false);
          if (confirm)
          {
            try
            {
              var resp = await GlobalVar.api.FinishTankDelivery(TankData.Id, TankData.DeliveryId, EndStock).ConfigureAwait(false);
              if (resp.status == 200)
              {
                GlobalFunc.Navigate2Root();
                GlobalFunc.ToastSuccess($"Delivery #{TankData.DeliveryId} at tank #{TankData.Number} has been finished");
                this.Dispose();
              }
              else
              {
                GlobalFunc.ToastWarning(resp.errors[0].message);
              }
            }
            catch (Exception ex)
            {
              GlobalFunc.ToastError(ex);
            }
          }
        });
      }
    }

    private async void DivVoid_Tapped(object sender, EventArgs e)
    {
      await GlobalFunc.AnimateOnTap((Frame)sender).ConfigureAwait(false);

      MainThread.BeginInvokeOnMainThread(async () => {
        var confirm = await DisplayAlert("Confirmation", "Are you sure want to cancel tank delivery " + 
          $"for tank {TankData.Name}?", "Yes", "No").ConfigureAwait(false);
        if (confirm)
        {
          try
          {
            var resp = await GlobalVar.api.VoidTankDelivery(TankData.DeliveryId).ConfigureAwait(false);
            if (resp.status == 200)
            {
              GlobalFunc.Navigate2Root();
              GlobalFunc.ToastSuccess($"Delivery #{TankData.DeliveryId} at tank #{TankData.Number} has been cancelled");
              this.Dispose();
            }
            else
            {
              GlobalFunc.ToastWarning(resp.errors[0].message);
            }
          }
          catch (Exception ex)
          {
            GlobalFunc.ToastError(ex);
          }
        }
      });
    }

    protected virtual void Dispose(bool disposing)
    {
      if (isDisposed) return;

      if (disposing)
      {
        TimerData?.Stop();
        TimerData?.Dispose();

        TimerBlink?.Stop();
        TimerBlink?.Dispose();
      }

      isDisposed = true;
    }
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
  }
}

﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TDMS.Mobile.Layout
{
  public partial class MyApp : Application
  {
    private void CheckSession()
    {
      if (GlobalVar.IsSessionExpired)
        GlobalVar.mainPage = new LoginPage();
      else
        GlobalVar.mainPage = new MainPage();

      MainPage = new NavigationPage(GlobalVar.mainPage);
    }

    public MyApp()
    {
      InitializeComponent();

      GlobalFunc.SaveSetting("Server", GlobalVar.IPAddress);
      GlobalVar.CurrentSetting = GlobalFunc.ReadSetting();

      GlobalVar.mainPage = new SplashPage();
      MainPage = new NavigationPage(GlobalVar.mainPage);
    }

    protected override void OnStart()
    {
      CheckSession();
    }

    protected override void OnSleep()
    {
      GlobalFunc.SaveSetting();
    }

    protected override void OnResume()
    {
      GlobalFunc.ReadSetting();
      CheckSession();
    }
  }
}

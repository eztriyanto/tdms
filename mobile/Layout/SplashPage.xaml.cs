﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;

namespace TDMS.Mobile.Layout
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SplashPage : ContentPage
	{
    public SplashPage ()
		{
			InitializeComponent();
    }

    protected override void OnAppearing()
    {
      base.OnAppearing();
    }
    protected override bool OnBackButtonPressed()
    {
      return true;
    }
  }
}
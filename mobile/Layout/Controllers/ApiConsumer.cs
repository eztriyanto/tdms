﻿using System;
using System.Net.Http;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Acr.UserDialogs.Infrastructure;
using Android.Accounts;
using Java.IO;
using Newtonsoft.Json;

using TDMS.Library.Models;

namespace TDMS.Mobile.Layout.Controllers
{
  public class ApiConsumer : IDisposable
  {
    private bool isDisposed;
    private HttpClient Client;

    private ApiResponse HttpCatchMessage()
    {
      return new ApiResponse(599, "error", "The request failed due to an underlying issue " +
          "such as network connectivity, DNS failure, server certificate validation or timeout");
    }

    public ApiConsumer()
    {
      Client = new HttpClient { Timeout = TimeSpan.FromSeconds(5) };
    }

    private async Task<ApiResponse> DoCommand(string cmd, object request)
    {
      var endPoint = new Uri($"http://{GlobalVar.CurrentSetting.Server}:13501/api/{cmd}");
      var result = new ApiResponse(501, "error", "Internal server error");
      try
      {
        using var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
        HttpResponseMessage resp = await Client.PostAsync(endPoint, content).ConfigureAwait(false);
        if (resp.IsSuccessStatusCode && resp.Content != null)
        {
          var body = await resp.Content.ReadAsStringAsync().ConfigureAwait(false);
          result = JsonConvert.DeserializeObject<ApiResponse>(body);
        }
        else
        {
          result = new ApiResponse(599, "error", resp.StatusCode.ToString());
        }
      }
      catch (ArgumentNullException)
      {
        result = HttpCatchMessage();
      }
      catch (InvalidOperationException)
      {
        result = HttpCatchMessage();
      }
      catch (HttpRequestException)
      {
        result = HttpCatchMessage();
      }
      catch (TaskCanceledException)
      {
        result = HttpCatchMessage();
      }

      return result;
    }

    public async Task<ApiResponse> SessionValidate()
    {
      var request = new ApiRequest
      {
        token = GlobalConst.token,
        sessionId = GlobalVar.CurrentSetting.SessionId
      };
      return await DoCommand("session", request).ConfigureAwait(false);
    }

    public async Task<ApiResponse> Login(string UserId, string Password)
    {
      if (string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
        return new ApiResponse(403, "error", "Invalid user id or password");

      else
      {
        var request = new ApiLoginRequest
        {
          token = GlobalConst.token,
          attId = UserId,
          attPwd = Library.Controllers.SecurityController.Encrypt(Password)
        };
        return await DoCommand("login", request).ConfigureAwait(false);
      }
    }

    public async Task<ApiResponse> GetSite()
    {
      var request = new ApiRequest
      {
        token = GlobalConst.token,
        sessionId = GlobalVar.CurrentSetting.SessionId
      };
      return await DoCommand("site", request).ConfigureAwait(false);
    }

    public async Task<ApiResponse> GetTanks()
    {
      var request = new ApiRequest
      {
        token = GlobalConst.token,
        sessionId = GlobalVar.CurrentSetting.SessionId
      };
      return await DoCommand("tanks", request).ConfigureAwait(false);
    }

    public async Task<ApiResponse> GetTank(int TankNumber)
    {
      var request = new ApiTankRequest
      {
        token = GlobalConst.token,
        sessionId = GlobalVar.CurrentSetting.SessionId,
        tankNumber = TankNumber
      };
      return await DoCommand("tank", request).ConfigureAwait(false);
    }

    public async Task<ApiResponse> GetTankDelivery(int DeliveryId)
    {
      var request = new ApiTankDeliveryRequest
      {
        token = GlobalConst.token,
        sessionId = GlobalVar.CurrentSetting.SessionId,
        deliveryId = DeliveryId
      };
      return await DoCommand("tank/delivery", request).ConfigureAwait(false);
    }
    public async Task<ApiResponse> StartTankDelivery(int TankId, double Stock, 
      double DOVolume, string DONumber, string DriverName, string VehicleNumber, 
      string Supplier, string SupplyPoint, int AttendantId)
    {
      var request = new ApiStartTankDeliveryRequest
      {
        token = GlobalConst.token,
        sessionId = GlobalVar.CurrentSetting.SessionId,
        tankId = TankId,
        stock = Stock,
        doNumber = DONumber,
        doVolume = DOVolume,
        driverName = DriverName,
        vehicleNumber = VehicleNumber,
        supplier = Supplier,
        supplyPoint = SupplyPoint,
        attendantId = AttendantId
      };
      return await DoCommand("tank/delivery/start", request).ConfigureAwait(false);
    }
    public async Task<ApiResponse> FinishTankDelivery(int TankId, int DeliveryId, double Stock)
    {
      var request = new ApiFinishTankDeliveryRequest
      {
        token = GlobalConst.token,
        sessionId = GlobalVar.CurrentSetting.SessionId,
        tankId = TankId,
        deliveryId = DeliveryId,
        stock = Stock
      };
      return await DoCommand("tank/delivery/finish", request).ConfigureAwait(false);
    }

    public async Task<ApiResponse> VoidTankDelivery(int DeliveryId)
    {
      var request = new ApiVoidTankDeliveryRequest
      {
        token = GlobalConst.token,
        sessionId = GlobalVar.CurrentSetting.SessionId,
        deliveryId = DeliveryId
      };
      return await DoCommand("tank/delivery/void", request).ConfigureAwait(false);
    }

    protected virtual void Dispose(bool disposing)
    {
      if (isDisposed) return;

      if (disposing)
      {
        Client.Dispose();
      }

      isDisposed = true;
    }
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
  }
}

﻿using System;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TDMS.Mobile.Layout.Controls.GradientColorFrame), typeof(TDMS.Mobile.Droid.GradientColorFrameRenderer))]
namespace TDMS.Mobile.Droid
{
  public class GradientColorFrameRenderer : VisualElementRenderer<Frame>
  {
    private string StartColor { get; set; }
    private string EndColor { get; set; }

    [Obsolete("Forms 2.4 support")]
    public GradientColorFrameRenderer()
    {
    }

    public GradientColorFrameRenderer(Context context) : base(context)
    {
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>")]
    protected override void DispatchDraw(global::Android.Graphics.Canvas canvas)
    {
      Color sc = Color.FromHex(this.StartColor);
      Color ec = Color.FromHex(this.EndColor);

      #region for Vertical Gradient
      //var gradient = new Android.Graphics.LinearGradient(0, 0, 0, Height,
      #endregion

      #region for Horizontal Gradient
      var gradient = new Android.Graphics.LinearGradient(0, 0, Width, 0,
      #endregion

             sc.ToAndroid(),
             ec.ToAndroid(),
             Android.Graphics.Shader.TileMode.Mirror);

      var paint = new Android.Graphics.Paint()
      {
        Dither = true,
      };
      paint.SetShader(gradient);
      canvas.DrawPaint(paint);
      base.DispatchDraw(canvas);
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "<Pending>")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>")]
    protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
    {
      base.OnElementChanged(e);

      if (e.OldElement != null || Element == null)
      {
        return;
      }
      try
      {
        var stack = e.NewElement as Layout.Controls.GradientColorFrame;
        this.StartColor = stack.StartColor;
        this.EndColor = stack.EndColor;
      }
      catch { }
    }
  }
}
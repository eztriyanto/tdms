﻿using System;
using Android.Content;
using Android.OS;
using Android.Content.PM;
using Android.App;

[assembly: Xamarin.Forms.Dependency(typeof(TDMS.Mobile.Droid.MyService))]

namespace TDMS.Mobile.Droid
{
  public class MyService : Layout.Services.IService
  {
    public MyService() 
    { 
    
    }

    public void CloseApp()
    {
      Process.KillProcess(Process.MyPid());
    }
  }
}
﻿using System;

using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Views;

namespace TDMS.Mobile.Droid
{
  [Activity(Label = "Custoday Transfer", Icon = "@mipmap/icon", Theme = "@style/AppTheme", MainLauncher = true, LaunchMode = LaunchMode.SingleTop, ScreenOrientation = ScreenOrientation.Portrait)]
  public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
  {
    protected override void OnCreate(Bundle savedInstanceState)
    {
      TabLayoutResource = Resource.Layout.Tabbar;
      ToolbarResource = Resource.Layout.Toolbar;

      base.OnCreate(savedInstanceState);

      Xamarin.Essentials.Platform.Init(this, savedInstanceState);

      Acr.UserDialogs.UserDialogs.Init(this);

      ZXing.Net.Mobile.Forms.Android.Platform.Init();

      global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
      FFImageLoading.Forms.Platform.CachedImageRenderer.Init(true);
      LoadApplication(new Layout.MyApp());
    }

    protected override void OnResume()
    {
      base.OnResume();

      if (ZXing.Net.Mobile.Android.PermissionsHandler.NeedsPermissionRequest(this))
        ZXing.Net.Mobile.Android.PermissionsHandler.RequestPermissionsAsync(this);
    }

    protected override void OnDestroy()
    {
      base.OnDestroy();
    }

    public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
    {
      Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
      global::ZXing.Net.Mobile.Android.PermissionsHandler.OnRequestPermissionsResult(requestCode, permissions, grantResults);

      base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }
}
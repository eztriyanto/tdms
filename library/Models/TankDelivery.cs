﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDMS.Library.Models
{
  public class TankDelivery
  {
    [Display(Name = "Number", GroupName = "Hidden")]
    public long Index { get; set; } = 0;

    [Display(Name = "Tank Delivery Id", GroupName = "Export")]
    public int TankDeliveryId { get; set; } = 0;

    [Display(Name = "Site Id", GroupName = "Export")]
    public string SiteId { get; set; } = "";

    [Display(Name = "Site Name", GroupName = "Export")]
    public string SiteName { get; set; } = "";

    [Display(Name = "Site Address", GroupName = "Export")]
    public string SiteAddress { get; set; } = "";

    [Display(Name = "Tank Id", GroupName = "Hidden")]
    public int TankId { get; set; } = 0;

    [Display(Name = "Tank Name", GroupName = "Export")]
    public string TankName { get; set; } = "";

    [Display(Name = "Grade Id", GroupName = "Hidden")]
    public int GradeId { get; set; } = 0;

    [Display(Name = "Material Number", GroupName = "Export")]
    public string MaterialNumber { get; set; } = "";

    [Display(Name = "Product Name", GroupName = "Export")]
    public string GradeName { get; set; } = "";

    [Display(Name = "Oil Company Code", GroupName = "Export")]
    public string OilCompanyCode { get; set; } = "";

    [Display(Name = "Start Date", GroupName = "Export")]
    public DateTime RecordEntryTS { get; set; } = DateTime.MinValue;

    [Display(Name = "Finish Date", GroupName = "Export")]
    public DateTime DropDateTime { get; set; } = DateTime.MinValue;

    [Display(Name = "DO Number", GroupName = "Export")]
    public string OriginalInvoiceNumber { get; set; } = "";

    [Display(Name = "DO Volume", GroupName = "Export")]
    public double DispatchedVolume { get; set; } = 0;

    [Display(Name = "Depot", GroupName = "Export")]
    public string DeliveryNoteNum { get; set; } = "";

    [Display(Name = "Transportir", GroupName = "Export")]
    public string DeliveryDetail { get; set; } = "";

    [Display(Name = "Driver Name", GroupName = "Export")]
    public string DriverIdCode { get; set; } = "";

    [Display(Name = "Vehicle Number", GroupName = "Export")]
    public string TankerIdCode { get; set; } = "";

    [Display(Name = "Density", GroupName = "Export")]
    public double VarianceAtRefTemp { get; set; } = 0;

    [Display(Name = "Temperature", GroupName = "Export")]
    public double TemperatureVariance { get; set; } = 0;

    [Display(Name = "Start Stock", GroupName = "Export")]
    public double ReceivedVolAtRefTemp { get; set; } = 0;

    [Display(Name = "Finish Stock", GroupName = "Export")]
    public double DispatchedVolAtRefTemp { get; set; } = 0;

    [Display(Name = "Delivery Volume", GroupName = "Export")]
    public double DropVolume { get; set; } = 0;

    [Display(Name = "Sales Volume", GroupName = "Export")]
    public double TotalVariance { get; set; } = 0;

    [Display(Name = "Diff. Volume", GroupName = "Export")]
    public double DiffVolume { get => DropVolume - DispatchedVolume; }

    [Display(Name = "Attendant Name", GroupName = "Export")]
    public string UserName { get; set; } = "";

    [Display(Name = "Inserted Date", GroupName = "Export")]
    public DateTime CreatedDate { get; set; } = DateTime.MinValue;

    [Display(Name = "Last Update", GroupName = "Export")]
    public DateTime UpdateDate { get; set; } = DateTime.MinValue;


    //Not Export to CSV
    [Display(Name = "Delivery Type", GroupName = "Hidden")]
    public int TankMovementTypeId { get; set; } = 0;

    [Display(Name = "Attendant Id", GroupName = "Hidden")]
    public int UserId { get; set; } = 0;

    [Display(Name = "Shift Id", GroupName = "Hidden")]
    public int PeriodId { get; set; } = 0;

    [Display(Name = "Shift No.", GroupName = "Hidden")]
    public int PeriodNumber { get; set; } = 0;

    [Display(Name = "Delivery Volume Theoretical", GroupName = "Hidden")]
    public double DropVolumeTheo { get; set; } = 0;

    [Display(Name = "Product Price", GroupName = "Hidden")]
    public double UnitCostPrice { get; set; } = 0;
  }
}

﻿namespace TDMS.Library.Models
{
  public class Site
  {
    public string SiteId { get; set; } = "";
    public string SiteName { get; set; } = "";
    public string SiteAddress { get; set; } = "";
  }
}

﻿using System;

namespace TDMS.Library.Models
{
  public class HoseDelivery
  {
    public string SiteId { get; set; } = "";
    public string SiteName { get; set; } = "";
    public string SiteAddress { get; set; } = "";
    public DateTime DeliveryDate { get; set; } = DateTime.MinValue;
    public string OilCompanyCode { get; set; } = "";
    public string GradeName { get; set; } = "";
    public int TotalDelivery { get; set; } = 0;
    public int TotalPrePurchaseDelivery { get; set; } = 0;
    public int TotalOtherDelivery { get => TotalDelivery - TotalPrePurchaseDelivery;  }
  }
}

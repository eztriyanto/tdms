﻿using System;
using System.Collections.Generic;

namespace TDMS.Library.Models
{
  public class ApiErrorDetail
  {
    public string message { get; set; } = "";
  }

  public class ApiResponse
  {
    public ApiResponse() 
    {
    }

    public ApiResponse(int Status, string Title, string Msg)
    {
      status = Status;
      title = Title;
      errors = new List<ApiErrorDetail> { new ApiErrorDetail { message = Msg } };
    }
    public ApiResponse(int Status, string Title, string Msg, object Data)
    {
      status = Status;
      title = Title;
      errors = new List<ApiErrorDetail> { new ApiErrorDetail { message = Msg } };
      data = Data;
    }

    public string title { get; set; }
    public int status { get; set; }
    public List<ApiErrorDetail> errors { get; set; } = new List<ApiErrorDetail>();
    public object data { get; set; } = null;
  }
}

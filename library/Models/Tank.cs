﻿using System;

namespace TDMS.Library.Models
{
  public class Tank
  {
    public Tank()
    {
      Id = 0;
      Name = "";
      Number = 0;
      Type = 1;
      Grade = new Product();
      Capacity = 0;
      Volume = 0;
      Ullage = 0;
      LastReading = DateTime.MinValue;
      ProbeStatus = 1;
      DeliveryId = 0;
    }

    public int Id { get; set; }
    public string Name { get; set; }
    public int Number { get; set; }

    public int Type { get; set; }
    public Product Grade { get; set; }
    public decimal Capacity { get; set; }
    public decimal Volume { get; set; }
    public decimal Ullage { get; set; }
    public decimal UllageInPercent { get => Capacity > 0 ? Ullage / Capacity * 100 : 0; }
    public DateTime LastReading { get; set; }
    public int ProbeStatus { get; set; }
    public int DeliveryId { get; set; }
                                                      
    public bool IsGauge { get => Type == 2; }
    public bool IsOnline
    {
      get
      {
        if (Type != 1 && (DateTime.Compare(LastReading, DateTime.MinValue) == 0 || ProbeStatus != 2))
          return false;

        return true;
      }
    }
    public bool IsDelivering { get => DeliveryId > 0; }
  }
}

﻿using System;

namespace TDMS.Library.Models
{
  public class Attendant
  {
    public enum eRole {None = 0, Administrator = 1, Supervisor = 2, Shift_Leader = 3, Operator = 4 }

    public int Id { get; set; } = 0;
    public string LogonId { get; set; } = "";
    public string Password { get; set; } = "";
    public string Name { get; set; } = "";

    public eRole Role { get; set; }

    public string SessionId => Controllers.SecurityController.Encrypt($"{Id}#{LogonId}#{DateTime.Now.AddHours(6):yyyyssMMmmddHH}#{Password}#{(int)Role}");
  }
}

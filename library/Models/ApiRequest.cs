﻿using System;

namespace TDMS.Library.Models
{
  public class ApiRequest
  {
    public string token { get; set; }
    public string sessionId { get; set; }
  }

  public class ApiLoginRequest : ApiRequest
  {
    public string attId { get; set; }
    public string attPwd { get; set; }
  }

  public class ApiTankRequest : ApiRequest
  {
    public int tankNumber { get; set; }
  }

  public class ApiTankDeliveryRequest : ApiRequest
  {
    public int deliveryId { get; set; }
  }

  public class ApiStartTankDeliveryRequest : ApiRequest
  {
    public int tankId { get; set; }
    public double stock { get; set; }
    public double doVolume { get; set; } 
    public string doNumber { get; set; } 
    public string driverName { get; set; }
    public string vehicleNumber { get; set; }
    public string supplier { get; set; }
    public string supplyPoint { get; set; }
    public int attendantId { get; set; }
  }

  public class ApiFinishTankDeliveryRequest : ApiRequest
  {
    public int tankId { get; set; }
    public int deliveryId { get; set; }
    public double stock { get; set; }
  }
  public class ApiVoidTankDeliveryRequest : ApiRequest
  {
    public int deliveryId { get; set; }
  }
}

﻿using System;

namespace TDMS.Library.Models
{
  public class Product
  {
    public Product()
    {
      ProductId = 0;
      ProductName = "";
      CompanyCode = "";
      MaterialNumber = "";
      Price = 0;
    }

    public int ProductId { get; set; }
    public string ProductName { get; set; }
    public string CompanyCode { get; set; }
    public string MaterialNumber { get; set; }

    public decimal Price { get; set; }
  }
}

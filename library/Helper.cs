﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDMS.Library
{
  public class Helper
  {
    public static string ErrorMsg(Exception ex)
    {
      return ex.InnerException != null ? ex.InnerException.Message : ex.Message;
    }
  }
}

﻿using System;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using TDMS.Portal.Models;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Controllers
{
  public class HomeController : Controller
  {
    public IActionResult Index()
    {
      return View();
    }

    public IActionResult ChangePassword()
    {
      return View(new ChangePassword());
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult ChangePassword(ChangePassword cp, string cmd)
    {
      string result = "Invalid command type";
      if (ModelState.IsValid)
      {
        var mediumRegex = new Regex("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
        if (cmd == "Save")
        {
          User us = null;
          var user_id = HttpContext.Session.GetString("user_id");
          if (!string.IsNullOrEmpty(user_id))
            us = UserContext.GetItem(user_id);

          if (us == null)
            return RedirectToAction("Logout");

          else if (cp.OldPassword != us.Password)
            result = "Incorrect old password";

          else if (cp.NewPassword != cp.ConfirmPassword)
            result = "Please confirm with new password";

          else if (!mediumRegex.IsMatch(cp.NewPassword))
            result = "Password must contains six characters or more and has at least one lowercase " +
              "and one uppercase alphabetical character or has at least one lowercase " +
              "and one numeric character or has at least one uppercase and one numeric character";

          else
          {
            us.Password = cp.ConfirmPassword;
            result = UserContext.Save(us);
          }
        }
      }

      if (result == "")
        return RedirectToAction("Index").WithSuccess("Success", "Password has been changed");

      else
        return View(cp).WithDanger("Error", result);
    }

    public IActionResult Logout()
    {
      HttpContext.Session.Clear();
      return RedirectToAction("Index", "Login");
    }

    public IActionResult Error()
    {
      return View();
    }

    public IActionResult GetWeeklyGraphData(string startDate)
    {
      if (string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
        return Ok(new Table { total = 0, rows = new System.Collections.Generic.List<Chart.dataSet>() });

      DateTime sd;
      try
      {
        sd = DateTime.ParseExact(startDate, "yyyy-MM-dd", null);
      }
      catch
      {
        sd = DateTime.Now;
      }

      var dt = ChartContext.GetTankDeliveryWeeklyData(sd);
      return Ok(new Table
      {
        total = dt.Count,
        rows = dt
      });
    }
  }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TDMS.Portal.Contexts;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Models;

namespace TDMS.Portal.Controllers
{
  public class SupplyPointController : Controller
  {
    public IActionResult Index()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(SupplyPoint sp, string cmd)
    {
      string result = "Invalid command type";
      if (ModelState.IsValid)
      {
        if (sp.SupplyPointRegion == null || string.IsNullOrEmpty(sp.SupplyPointRegion.RegionId))
          result = "Select region first";

        else if (cmd == "Delete")
          result = SupplyPointContext.Delete(sp);

        else
          result = SupplyPointContext.Save(sp);
      }

      if (result == "")
        return RedirectToAction("Index").WithSuccess("Success", (cmd == "Delete" ? "Deleting" : "Saving") + 
          " depot " + sp.SupplyPointName);

      else
        return View(sp).WithDanger("Error", result);
    }

    public IActionResult Modify(string id)
    {
      string ID = string.IsNullOrEmpty(id) ? "" : id;
      SupplyPoint sp = SupplyPointContext.GetItem(ID) ?? new SupplyPoint();

      return View(sp);
    }

    public IActionResult GetData(int limit = 10, int offset = 0, string search = "")
    {
      if (Helper.getRole(HttpContext.Session.GetInt32("user_role")) == Models.User.RoleEnum.Administrator)
      {
        int totalData = SupplyPointContext.ItemsCount(search);
        if (totalData > 0)
          return Ok(new Table
          {
            total = totalData,
            rows = SupplyPointContext.GetItems(limit, Helper.OffsetToPage(limit, offset), search)
          });

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }

    [HttpGet]
    public ActionResult GetSupplyPoints(string regionId)
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        if (!string.IsNullOrWhiteSpace(regionId))
        {
          var result = new List<SelectListItem>();
          var spList = SupplyPointContext.GetAllItems(regionId);
          if (spList != null)
            foreach (var sp in spList)
              result.Add(new SelectListItem
              {
                Value = sp.SupplyPointId.ToString(),
                Text = sp.SupplyPointName
              });

          return Json(new SelectList(result, "Value", "Text"));
        }
      }

      return null;
    }
  }
}

﻿using System;
using System.Linq;
using System.Reflection;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using TDMS.Portal.Models;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Controllers
{
  public class TankDeliveryController : Controller
  {
    private Tuple<string, string, string, DateTime, DateTime, string, string> GetFilter()
    {
      string regionId = TempData.Peek("region")?.ToString() ?? "";
      string supplyPointId = TempData.Peek("supplyPoint")?.ToString() ?? "";
      string supplierId = TempData.Peek("supplier")?.ToString() ?? "";
      string search = TempData.Peek("search")?.ToString() ?? "";
      string product = TempData.Peek("product")?.ToString() ?? "";

      DateTime startDT = DateTime.Today;
      try
      {
        if (TempData.Peek("start") != null)
          startDT = (DateTime)TempData.Peek("start");
      }
      catch { }

      DateTime endDT = startDT;
      try
      {
        if (TempData.Peek("end") != null)
          endDT = (DateTime)TempData?.Peek("end");
      }
      catch { }
      
      return Tuple.Create(regionId, supplyPointId, supplierId, startDT, endDT, search, product);
    }

    public IActionResult Export()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        var p = GetFilter();

        //Header CSV
        var names = new List<string>();
        var exportProperties = new List<PropertyInfo>();
        var properties = typeof(TankDelivery).GetProperties();
        foreach (var item in properties)
        {
          MemberInfo property = typeof(TankDelivery).GetProperty(item.Name);
          var groupName = property.GetCustomAttribute<DisplayAttribute>()?.GroupName;
          if (!string.IsNullOrEmpty(groupName) && groupName == "Export")
          {
            var displayName = property.GetCustomAttribute<DisplayAttribute>()?.Name;
            names.Add(displayName ?? item.Name);

            exportProperties.Add(item);
          }
        }
        string header = string.Join(";", names);

        return new FileCallbackResult(new MediaTypeHeaderValue("text/csv"), async (outputStream, _) =>
        {
          //Items CSV
          if (exportProperties != null)
          {
            bool breakFlag = false;
            int page = 1;
            using (var sw = new System.IO.StreamWriter(outputStream))
            {
              await sw.WriteLineAsync(header);

              while (!breakFlag)
              {
                var data = TankDeliveryContext.GetItems(p.Item1, p.Item2, p.Item3, p.Item4, p.Item5, 500, page, p.Item6);
                breakFlag = (data == null || data.Count == 0);
                if (!breakFlag)
                {
                  foreach (var item in data)
                  {
                    var row = exportProperties.Select(n => n.GetValue(item, null))
                      .Select(n => n == null ? "null" : 
                        n.GetType().Name.Equals("String") ? $"\"{n.ToString()}\"" : n.ToString())
                      .Aggregate((a, b) => a + ";" + b);
                    await sw.WriteLineAsync(row);
                  }
                }
                await sw.FlushAsync();
                page += 1;
              }
            }
          }          
        })
        {
          FileDownloadName = $"Tank_Delivery_{p.Item4:yyyyMMdd}_{p.Item5:yyyyMMdd}.csv"
        };
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult Index(string region, string supplyPoint, string supplier, DateTime? start, DateTime? end)
    {
      TempData["region"] = region;
      TempData["supplyPoint"] = supplyPoint;
      TempData["supplier"] = supplier;
      TempData["start"] = start;
      TempData["end"] = end;
 
      return View();
    }

    public IActionResult Summary(string product, DateTime? start, DateTime? end)
    {
      TempData["product"] = product;
      TempData["start"] = start;
      TempData["end"] = end;

      var _dt = TankDeliveryContext.GetSummaryItems(product, start ?? DateTime.Today, end ?? DateTime.Today);
      return View(new Table {
        total = _dt.Count,
        totalNotFiltered = _dt.Count,
        rows = _dt
      });
    }

    public IActionResult SummaryExport()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        var p = GetFilter();

        //Header CSV
        string csv = "Product: " + (string.IsNullOrEmpty(p.Item7) ? "All Product" : ProductContext.GetItem(p.Item7)?.ProductName) + "\r\n";
        var names = new List<string>();
        var exportProperties = new List<PropertyInfo>();
        var properties = typeof(TankDeliverySummary).GetProperties();
        foreach (var item in properties)
        {
          MemberInfo property = typeof(TankDeliverySummary).GetProperty(item.Name);
          var groupName = property.GetCustomAttribute<DisplayAttribute>()?.GroupName;
          if (!string.IsNullOrEmpty(groupName) && groupName == "Export")
          {
            var displayName = property.GetCustomAttribute<DisplayAttribute>()?.Name;
            names.Add(displayName ?? item.Name);

            exportProperties.Add(item);
          }
        }
        csv += String.Join(";", names) + "\r\n";

        //Items CSV
        if (exportProperties != null)
        {
          var data = TankDeliveryContext.GetSummaryItems(p.Item7, p.Item4, p.Item5);
          using (var sw = new System.IO.StringWriter())
          {
            foreach (var item in data)
            {
              var row = exportProperties.Select(n => n.GetValue(item, null))
                .Select(n => n == null ? "null" : n.ToString())
                .Aggregate((a, b) => a + ";" + b); 
              sw.WriteLine(row);
            }
            csv += sw.ToString();

            //Add tolerance
            var idxPercent = data.Count - 1;
            var dataL = data[idxPercent].Loss_2 + data[idxPercent].Loss_1 +
               data[idxPercent].Loss_03 + data[idxPercent].Loss_015;
            var dataN = data[idxPercent].Loss_0 + data[idxPercent].Gain_0;
            var dataH = data[idxPercent].Gain_015 + data[idxPercent].Gain_03 +
              data[idxPercent].Gain_1 + data[idxPercent].Gain_2;

            csv += $"Tolerance;{dataL};{dataN};{dataH}\r\n";
          }
        }

        return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv",
          $"Tank_Delivery_Summary_{p.Item4:yyyyMMdd}_{p.Item5:yyyyMMdd}.csv");
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult SummaryBySite(string product, string region, string supplyPoint, string supplier, DateTime? start, DateTime? end)
    {
      TempData["product"] = product;
      TempData["region"] = region;
      TempData["supplyPoint"] = supplyPoint;
      TempData["supplier"] = supplier;
      TempData["start"] = start;
      TempData["end"] = end;

      return View();
    }

    public IActionResult SummaryBySiteExport()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        var p = GetFilter();

        //Header CSV
        string csv = "Product: " + (string.IsNullOrEmpty(p.Item7) ? "All Product" : ProductContext.GetItem(p.Item7)?.ProductName) + "\r\n";
        var names = new List<string>();
        var exportProperties = new List<PropertyInfo>();
        var properties = typeof(TankDeliverySummaryBySite).GetProperties();
        foreach (var item in properties)
        {
          MemberInfo property = typeof(TankDeliverySummaryBySite).GetProperty(item.Name);
          var groupName = property.GetCustomAttribute<DisplayAttribute>()?.GroupName;
          if (!string.IsNullOrEmpty(groupName) && groupName == "Export")
          {
            var displayName = property.GetCustomAttribute<DisplayAttribute>()?.Name;
            names.Add(displayName ?? item.Name);

            exportProperties.Add(item);
          }
        }
        csv += String.Join(";", names) + "\r\n";

        //Items CSV
        if (exportProperties != null)
        {
          bool breakFlag = false;
          int page = 1;
          while (!breakFlag)
          {
            var data = TankDeliveryContext.GetSummaryBySiteItems(p.Item7, p.Item1, p.Item2, p.Item3, p.Item4, p.Item5, 500, page, p.Item6);
            breakFlag = (data == null || data.Count == 0);
            if (!breakFlag)
            {
              using (var sw = new System.IO.StringWriter())
              {
                foreach (var item in data)
                {
                  var row = exportProperties.Select(n => n.GetValue(item, null))
                    .Select(n => n == null ? "null" : n.ToString())
                    .Aggregate((a, b) => a + ";" + b); sw.WriteLine(row);
                }
                csv += sw.ToString();
              }
              page += 1;
            }
          }
        }

        return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv",
          $"Tank_Delivery_Summary_By_SPBU_{p.Item4:yyyyMMdd}_{p.Item5:yyyyMMdd}.csv");
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult ListBySite()
    {
      return View();
    }

    public IActionResult ListBySiteExport()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        //Header CSV
        var exportProperties = typeof(TankDelivery).GetProperties()
          .Where(x => x.Name == "SiteId" || x.Name == "CreatedDate" || x.Name == "UpdateDate")
          .ToList();

        var csv = "Site Id;Inserted Date;Updated Date" + "\r\n";

        //Items CSV
        if (exportProperties != null)
        {
          string search = TempData.Peek("search")?.ToString() ?? "";
          bool breakFlag = false;
          int page = 1;
          while (!breakFlag)
          {
            var data = TankDeliveryContext.GetListBySiteItems(500, page, search);
            breakFlag = (data == null || data.Count == 0);
            if (!breakFlag)
            {
              using (var sw = new System.IO.StringWriter())
              {
                foreach (var item in data)
                {
                  var row = exportProperties.Select(n => n.GetValue(item, null))
                    .Select(n => n == null ? "null" : n.ToString())
                    .Aggregate((a, b) => a + ";" + b); sw.WriteLine(row);
                }
                csv += sw.ToString();
              }
              page += 1;
            }
          }
        }

        return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv",
          $"Tank_Delivery_Site_List_{DateTime.Now:yyyyMMdd_HHmmss}.csv");
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult GetData(int limit = 10, int offset = 0, string search = "", string sort = "", string order = "")
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        TempData["search"] = search;

        var p = GetFilter();
        int totalData = TankDeliveryContext.ItemsCount(p.Item1, p.Item2, p.Item3, p.Item4, p.Item5, p.Item6);
        if (totalData > 0)
        {
          string orderBy = "";
          if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(order))
            orderBy = "(drop_volume - dispatched_volume) " + order;

          return Ok(new Table
          {
            total = totalData,
            totalSite = TankDeliveryContext.SiteCount(p.Item1, p.Item2, p.Item3, p.Item4, p.Item5, p.Item6),
            rows = TankDeliveryContext.GetItems(p.Item1, p.Item2, p.Item3, p.Item4, p.Item5, limit, Helper.OffsetToPage(limit, offset), p.Item6, orderBy)
          });
        }

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult GetSummaryData()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        var p = GetFilter();
        return Ok(new Table
        {
          total = 8,
          totalSite = 8,
          rows = TankDeliveryContext.GetSummaryItems(p.Item7, p.Item4, p.Item5)
        });
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult GetSummaryBySiteData(int limit = 10, int offset = 0, string search = "", string sort = "", string order = "")
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        TempData["search"] = search;

        var p = GetFilter();
        int totalData = TankDeliveryContext.ItemsCountForSummaryBySite(p.Item7, p.Item1, p.Item2, p.Item3, p.Item4, p.Item5, p.Item6);
        if (totalData > 0)
        {
          string orderBy = "";
          if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(order))
            orderBy = "(total_volume - total_do_volume) " + order;
          
          return Ok(new Table
          {
            total = totalData,
            totalSite = totalData,
            rows = TankDeliveryContext.GetSummaryBySiteItems(p.Item7, p.Item1, p.Item2, p.Item3, p.Item4, p.Item5, limit, Helper.OffsetToPage(limit, offset), p.Item6, orderBy)
          });
        }

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult GetListBySiteData(int limit = 10, int offset = 0, string search = "")
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        TempData["search"] = search;

        string _search = TempData.Peek("search")?.ToString() ?? "";

        int totalData = TankDeliveryContext.ItemsCountForListBySite(_search);
        if (totalData > 0)
        {
          return Ok(new Table
          {
            total = totalData,
            totalSite = totalData,
            rows = TankDeliveryContext.GetListBySiteItems(limit, Helper.OffsetToPage(limit, offset), _search)
          });
        }

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }
  }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using TDMS.Portal.Models;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Controllers
{
  public class ProductController : Controller
  {
    public IActionResult Index()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(Product reg, string cmd)
    {
      string result = "Invalid command type";
      if (ModelState.IsValid)
      {
        if (cmd == "Delete")
          result = ProductContext.Delete(reg);

        else
          result = ProductContext.Save(reg);
      }

      if (result == "")
        return RedirectToAction("Index").WithSuccess("Success", (cmd == "Delete" ? "Deleting" : "Saving") + 
          " Product with id [" + reg.ProductId + "]");

      else
        return View(reg).WithDanger("Error", result);
    }

    public IActionResult Modify(string id)
    {
      string ID = string.IsNullOrEmpty(id) ? "" : id;
      Product reg = ProductContext.GetItem(ID) ?? new Product();

      return View(reg);
    }

    public IActionResult GetData(int limit = 10, int offset = 0, string search = "")
    {
      if (Helper.getRole(HttpContext.Session.GetInt32("user_role")) == Models.User.RoleEnum.Administrator)
      {
        int totalData = ProductContext.ItemsCount(search);
        if (totalData > 0)
          return Ok(new Table
          {
            total = totalData,
            rows = ProductContext.GetItems(limit, Helper.OffsetToPage(limit, offset), search)
          });

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }
  }
}

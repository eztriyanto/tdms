﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using TDMS.Portal.Models;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Controllers
{
  public class LoginController : Controller
  {
    public IActionResult Index()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(User us)
    {
      if (ModelState.IsValid)
      {
        var users = UserContext.GetAllItems()?.Where(x => x.Role == Models.User.RoleEnum.Administrator).ToList();
        if ((users == null || users.Count == 0) && (us.UserId == "admin" && us.Password == "admin"))
        {
          HttpContext.Session.SetString("user_id", us.UserId);
          HttpContext.Session.SetString("user_name", "System Adminstrator");
          HttpContext.Session.SetInt32("user_role", 10);
          HttpContext.Session.SetString("user_role_str", "SysAdmin");
          HttpContext.Session.SetString("user_access", "");

          return RedirectToAction("Modify", "User");
        }

        User user = UserContext.GetItem(us.UserId);
        if (user != null && user.Password == us.Password)
        {
          HttpContext.Session.SetString("user_id", user.UserId);
          HttpContext.Session.SetString("user_name", user.UserName);
          HttpContext.Session.SetInt32("user_role", (int)user.Role);
          HttpContext.Session.SetString("user_role_str", user.Role.ToString());
          HttpContext.Session.SetString("user_access", user.AccessId);

          return RedirectToAction("Index", "Home");
        }
      }

      return View(us).WithWarning("Warning","Invalid user id or password");
    }
  }
}

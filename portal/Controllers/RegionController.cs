﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using TDMS.Portal.Models;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Controllers
{
  public class RegionController : Controller
  {
    public IActionResult Index()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(Region reg, string cmd)
    {
      string result = "Invalid command type";
      if (ModelState.IsValid)
      {
        if (cmd == "Delete")
          result = RegionContext.Delete(reg);

        else
          result = RegionContext.Save(reg);
      }

      if (result == "")
        return RedirectToAction("Index").WithSuccess("Success", (cmd == "Delete" ? "Deleting" : "Saving") + 
          " region with id [" + reg.RegionId + "]");

      else
        return View(reg).WithDanger("Error", result);
    }

    public IActionResult Modify(string id)
    {
      string ID = string.IsNullOrEmpty(id) ? "" : id;
      Region reg = RegionContext.GetItem(ID) ?? new Region();

      return View(reg);
    }

    public IActionResult GetData(int limit = 10, int offset = 0, string search = "")
    {
      if (Helper.getRole(HttpContext.Session.GetInt32("user_role")) == Models.User.RoleEnum.Administrator)
      {
        int totalData = RegionContext.ItemsCount(search);
        if (totalData > 0)
          return Ok(new Table
          {
            total = totalData,
            rows = RegionContext.GetItems(limit, Helper.OffsetToPage(limit, offset), search)
          });

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }
  }
}

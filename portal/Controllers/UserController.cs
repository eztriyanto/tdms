﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

using TDMS.Portal.Models;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Controllers
{
  public class UserController : Controller
  {
    public string RoleEnumToString
    {
      get
      {
        return JsonConvert.SerializeObject(Enum.GetValues(typeof(User.RoleEnum)),
          new Newtonsoft.Json.Converters.StringEnumConverter());
      }
    }

    public IActionResult Index()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(User us, string cmd)
    {
      string result = "Invalid command type";
      if (ModelState.IsValid)
      {
        if (cmd == "Delete")
          result = UserContext.Delete(us);

        else
          result = UserContext.Save(us);
      }

      if (result == "")
      {
        if (HttpContext.Session.GetInt32("user_role") == 10)
          return Redirect("/Home/Logout");

        else
          return RedirectToAction("Index").WithSuccess("Success", (cmd == "Delete" ? "Deleting" : "Saving") +
            " user with id [" + us.UserId + "]");
      }
      else
        return View(us).WithDanger("Error", result);
    }

    public IActionResult Modify(string id)
    {
      string ID = string.IsNullOrEmpty(id) ? "" : id;
      User us = UserContext.GetItem(ID);
      if (us == null)
        us = new User();

      return View(us);
    }

    public IActionResult GetData(int limit = 10, int offset = 0, string search = "")
    {
      if (Helper.getRole(HttpContext.Session.GetInt32("user_role")) == Models.User.RoleEnum.Administrator)
      {
        int totalData = UserContext.ItemsCount(search);
        if (totalData > 0)
          return Ok(new Table
          {
            total = totalData,
            rows = UserContext.GetItems(limit, Helper.OffsetToPage(limit, offset), search)
          });

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult GetAccessList(string mode = "", string Id = "")
    {
      if (Helper.getRole(HttpContext.Session.GetInt32("user_role")) == Models.User.RoleEnum.Administrator)
      {
        var result = new List<DropDownListItem>();
        switch (mode)
        {
          case "Region":
            foreach (var reg in RegionContext.GetAllItems())
              result.Add(new DropDownListItem {
                text = reg.RegionName,
                value = reg.RegionId
              });
            break;

          case "Depot":
            foreach (var sp in SupplyPointContext.GetAllItems())
              result.Add(new DropDownListItem
              {
                text = sp.SupplyPointName,
                value = sp.SupplyPointId.ToString()
              });
            break;

          case "Transportir":
            foreach (var sp in SupplierContext.GetAllItems())
              result.Add(new DropDownListItem
              {
                text = sp.SupplierName,
                value = sp.SupplierId
              });
            break;
        }

        return Ok(result);
      }

      return RedirectToAction("Index", "Login");
    }
  }
}

﻿using System;
using System.Linq;
using System.Reflection;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using TDMS.Portal.Models;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Controllers
{
  public class HoseDeliveryController : Controller
  {
    private Tuple<string, DateTime, DateTime, string> GetFilter()
    {
      string gradeId = TempData.Peek("product")?.ToString() ?? "";
      string search = TempData.Peek("search")?.ToString() ?? "";

      DateTime startDT = DateTime.Today;
      try
      {
        if (TempData.Peek("start") != null)
          startDT = (DateTime)TempData.Peek("start");
      }
      catch { }

      DateTime endDT = startDT;
      try
      {
        if (TempData.Peek("end") != null)
          endDT = (DateTime)TempData?.Peek("end");
      }
      catch { }

      return Tuple.Create(gradeId, startDT, endDT, search);
    }

    public IActionResult Export()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        var p = GetFilter();

        //Header CSV
        var names = new List<string>();
        var exportProperties = new List<PropertyInfo>();
        var properties = typeof(HoseDelivery).GetProperties();
        foreach (var item in properties)
        {
          MemberInfo property = typeof(HoseDelivery).GetProperty(item.Name);
          var groupName = property.GetCustomAttribute<DisplayAttribute>()?.GroupName;
          if (!string.IsNullOrEmpty(groupName) && groupName == "Export")
          {
            var displayName = property.GetCustomAttribute<DisplayAttribute>()?.Name;
            names.Add(displayName ?? item.Name);

            exportProperties.Add(item);
          }
        }
        string header = string.Join(";", names);

        return new FileCallbackResult(new MediaTypeHeaderValue("text/csv"), async (outputStream, _) =>
        {
          //Items CSV
          if (exportProperties != null)
          {
            bool breakFlag = false;
            int page = 1;

            using (var sw = new System.IO.StreamWriter(outputStream))
            {
              await sw.WriteLineAsync(header);

              while (!breakFlag)
              {
                var data = HoseDeliveryContext.GetItems(p.Item1, p.Item2, p.Item3, 500, page, p.Item4);
                breakFlag = (data == null || data.Count == 0);
                if (!breakFlag)
                {
                  foreach (var item in data)
                  {
                    var row = exportProperties.Select(n => n.GetValue(item, null))
                      .Select(n => n == null ? "null" :
                        n.GetType().Name.Equals("String") ? $"\"{n.ToString()}\"" : n.ToString())
                      .Aggregate((a, b) => a + ";" + b);
                    await sw.WriteLineAsync(row);
                  }
                }
                await sw.FlushAsync();
                page += 1;
              }
            }
          }
        })
        {
          FileDownloadName = $"Hose_Delivery_{p.Item2:yyyyMMdd}_{p.Item3:yyyyMMdd}.csv"
        };
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult Index(string product, DateTime? start, DateTime? end)
    {
      TempData["product"] = "12";
      TempData["start"] = start;
      TempData["end"] = end;

      return View();
    }

    public IActionResult Summary(DateTime? start, DateTime? end)
    {
      TempData["start"] = start;
      TempData["end"] = end;

      return View();
    }

    private string SummaryCSV(DateTime start, DateTime end)
    {
      //Header CSV
      var names = new List<string>();
      var exportProperties = new List<PropertyInfo>();
      var properties = typeof(HoseDeliverySummary).GetProperties();
      foreach (var item in properties)
      {
        MemberInfo property = typeof(HoseDeliverySummary).GetProperty(item.Name);
        var groupName = property.GetCustomAttribute<DisplayAttribute>()?.GroupName;
        if (!string.IsNullOrEmpty(groupName) && groupName == "Export")
        {
          var displayName = property.GetCustomAttribute<DisplayAttribute>()?.Name;
          names.Add(displayName ?? item.Name);

          exportProperties.Add(item);
        }
      }
      string csv = String.Join(";", names) + "\r\n";

      //Items CSV
      if (exportProperties != null)
      {
        var data = HoseDeliveryContext.GetSummaryItems(start, end);
        using (var sw = new System.IO.StringWriter())
        {
          foreach (var item in data)
          {
            var row = exportProperties.Select(n => n.GetValue(item, null))
              .Select(n => n == null ? "null" : n.ToString())
              .Aggregate((a, b) => a + ";" + b); sw.WriteLine(row);
          }
          csv += sw.ToString();
        }
      }

      return csv;
    }
    private string SummaryClassificationCSV(DateTime start, DateTime end)
    {
      //Header CSV
      var names = new List<string>();
      var exportProperties = new List<PropertyInfo>();
      var properties = typeof(HoseDeliverySummaryClassification).GetProperties();
      foreach (var item in properties)
      {
        MemberInfo property = typeof(HoseDeliverySummaryClassification).GetProperty(item.Name);
        var groupName = property.GetCustomAttribute<DisplayAttribute>()?.GroupName;
        if (!string.IsNullOrEmpty(groupName) && groupName == "Export")
        {
          var displayName = property.GetCustomAttribute<DisplayAttribute>()?.Name;
          names.Add(displayName ?? item.Name);

          exportProperties.Add(item);
        }
      }
      string csv = String.Join(";", names) + "\r\n";

      //Items CSV
      if (exportProperties != null)
      {
        var data = HoseDeliveryContext.GetSummaryClassificationItems(start, end);
        using (var sw = new System.IO.StringWriter())
        {
          foreach (var item in data)
          {
            var row = exportProperties.Select(n => n.GetValue(item, null))
              .Select(n => n == null ? "null" : n.ToString())
              .Aggregate((a, b) => a + ";" + b); sw.WriteLine(row);
          }
          csv += sw.ToString();
        }
      }

      return csv;
    }
    public IActionResult SummaryExport()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        var p = GetFilter();
        var csv = SummaryCSV(p.Item2, p.Item3) +  Environment.NewLine + 
          SummaryClassificationCSV(p.Item2, p.Item3);

        return File(new System.Text.UTF8Encoding().GetBytes(csv), "text/csv", 
          $"Hose_Delivery_Summary_{p.Item2:yyyyMMdd}_{p.Item3:yyyyMMdd}.csv");
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult GetData(int limit = 10, int offset = 0, string search = "", string sort = "", string order = "")
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        TempData["search"] = search;

        var p = GetFilter();
        int totalData = HoseDeliveryContext.ItemsCount(p.Item1, p.Item2, p.Item3, p.Item4);
        if (totalData > 0)
        {
          string orderBy = "";
          if (!string.IsNullOrEmpty(sort) && !string.IsNullOrEmpty(order))
            orderBy = "(total_prepurchase/total_delivery * 100) " + order;

          return Ok(new Table
          {
            total = totalData,
            totalSite = HoseDeliveryContext.SiteCount(p.Item1, p.Item2, p.Item3, p.Item4),
            rows = HoseDeliveryContext.GetItems(p.Item1, p.Item2, p.Item3, limit, Helper.OffsetToPage(limit, offset), p.Item4, orderBy)
          });
        }

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult GetSummaryData()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        var p = GetFilter();
        return Ok(new Table
        {
          total = 8,
          totalSite = 8,
          rows = HoseDeliveryContext.GetSummaryItems(p.Item2, p.Item3)
        });
      }

      return RedirectToAction("Index", "Login");
    }

    public IActionResult GetSummaryClassificationData()
    {
      if (!string.IsNullOrEmpty(HttpContext.Session.GetString("user_id")))
      {
        var p = GetFilter();
        return Ok(new Table
        {
          total = 8,
          totalSite = 8,
          rows = HoseDeliveryContext.GetSummaryClassificationItems(p.Item2, p.Item3)
        });
      }

      return RedirectToAction("Index", "Login");
    }
  }
}

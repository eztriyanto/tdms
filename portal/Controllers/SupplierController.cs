﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using TDMS.Portal.Models;
using TDMS.Portal.Extensions.Alerts;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Controllers
{
  public class SupplierController : Controller
  {
    public IActionResult Index()
    {
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Index(Supplier sup, string cmd)
    {
      string result = "Invalid command type";
      if (ModelState.IsValid)
      {
        if (cmd == "Delete")
          result = SupplierContext.Delete(sup);

        else
          result = SupplierContext.Save(sup);
      }

      if (result == "")
        return RedirectToAction("Index").WithSuccess("Success", (cmd == "Delete" ? "Deleting" : "Saving") + 
          " transportir with id [" + sup.SupplierId + "]");

      else
        return View(sup).WithDanger("Error", result);
    }

    public IActionResult Modify(string id)
    {
      string ID = string.IsNullOrEmpty(id) ? "" : id;
      Supplier sup = SupplierContext.GetItem(ID) ?? new Supplier();

      return View(sup);
    }

    public IActionResult GetData(int limit = 10, int offset = 0, string search = "")
    {
      if (Helper.getRole(HttpContext.Session.GetInt32("user_role")) == Models.User.RoleEnum.Administrator)
      {
        int totalData = SupplierContext.ItemsCount(search);
        if (totalData > 0)
          return Ok(new Table
          {
            total = totalData,
            rows = SupplierContext.GetItems(limit, Helper.OffsetToPage(limit, offset), search)
          });

        return Ok(new Table { rows = new List<string>() });
      }

      return RedirectToAction("Index", "Login");
    }
  }
}

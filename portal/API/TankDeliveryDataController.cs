﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using TDMS.Portal.Contexts;
using TDMS.Portal.Models.Api;

namespace TDMS.Portal.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class TankDeliveryDataController : ControllerBase
  {
    [HttpPost]
    public Response Post([FromBody] TankDeliveryDataRequest body)
    {
      try
      {
        Response resp = null;

        var respBody = Api.checkRequest(body, HttpContext.Request.Headers["Authorization"]);
        if (respBody == null)
        {
          if (body == null || string.IsNullOrEmpty(body.doNumber))
            resp = new Response
            {
              status = 500,
              title = "Invalid parameter(s)"
            };

          else
          {
            //Processing data
            var result = TankDeliveryContext.GetItem(body.doNumber);
            if (result != null)
              resp = new Response
              {
                status = 200,
                title = "success",
                data = result
              };

            else
              resp = new Response
              {
                status = 500,
                title = "Error saving data",
                errors = new List<error> { new error { message = "Can't get tank delivery data" } }
              };
          }
        }
        else
        {
          resp = new Response
          {
            title = respBody.title,
            status = respBody.status,
            errors = respBody.errors
          };
        }
        
        return resp;
      }
      catch (Exception ex)
      {
        var msg = ex.Message;
        if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
          msg = ex.InnerException.Message;

        return new Response
        {
          status = 599,
          title = "Error occurred",
          errors = new List<error> { new error { message = msg } }
        };
      }
    }
  }
}

﻿using System;
using System.Collections.Generic;

namespace TDMS.Portal.Models.Api
{
  public class Request
  {
    public string token { get; set; }
    public string siteId { get; set; }
  }

  public class SendTankDeliveryDataRequest : Request
  {
    public List<TankDelivery> items { get; set; }
  }

  public class SendHoseDeliveryDataRequest : Request
  {
    public List<HoseDelivery> items { get; set; }
  }

  public class TankDeliveryDataRequest : Request
  {
    public string doNumber { get; set; }
  }
}

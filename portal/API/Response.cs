﻿using System;
using System.Collections.Generic;

namespace TDMS.Portal.Models.Api
{
  public class error
  {
    public string message { get; set; } = "";
  }

  public class Response
  {
    public string title { get; set; }
    public int status { get; set; }
    public List<error> errors { get; set; } = new List<error>();

    public object data { get; set; }
  }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using TDMS.Portal.Contexts;
using TDMS.Portal.Models.Api;

namespace TDMS.Portal.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class SendHoseDeliveryController : ControllerBase
  {
    [HttpPost]
    public Response Post([FromBody] SendHoseDeliveryDataRequest body)
    {
      try
      {
        var resp = Api.checkRequest(body);
        if (resp == null)
        {
          if (body == null)
            resp = new Response
            {
              status = 500,
              title = "Invalid parameter(s)"
            };

          else
          {
            //Processing data
            var result = HoseDeliveryContext.Save(body.items);
            if (string.IsNullOrEmpty(result))
              resp = new Response
              {
                status = 200,
                title = "success"
              };

            else
              resp = new Response
              {
                status = 500,
                title = "Error saving data",
                errors = new List<error> { new error { message = result } }
              };
          }
        }

        return resp;
      }
      catch (Exception ex)
      {
        var msg = ex.Message;
        if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
          msg = ex.InnerException.Message;

        return new Response
        {
          status = 599,
          title = "Error occurred",
          errors = new List<error> { new error { message = msg } }
        };
      }
    }
  }
}

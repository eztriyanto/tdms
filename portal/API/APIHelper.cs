﻿using System;
using System.IO;
using Newtonsoft.Json;

using TDMS.Portal.Models.Api;

namespace TDMS.Portal
{
  public class Api
  {
    private static ConfigJson GetApiSetting()
    {
      ConfigJson result = JsonConvert.DeserializeObject<ConfigJson>(File.ReadAllText($"{AppContext.BaseDirectory}config.json"));
      return result;
    }

    static bool Authenticate(string token)
    {
      bool isAuthenticated = false;
      try
      {
        if (!string.IsNullOrEmpty(token))
          isAuthenticated = token.Equals(GetApiSetting()?.TokenAPI);
      }
      catch { }

      return isAuthenticated;
    }

    static bool Authenticate(string token, string auth)
    {
      bool isAuthenticated = false;
      if (Authenticate(token))
      {
        if (!string.IsNullOrEmpty(auth) && auth.StartsWith("basic", StringComparison.OrdinalIgnoreCase))
        {
          var credentialstring = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(auth.Substring("Basic ".Length).Trim()));
          var credentials = credentialstring.Split(':');
          if (credentials[0] == GetApiSetting()?.ApiUser && credentials[1] == GetApiSetting()?.ApiPassword)
            isAuthenticated = true;
        }
      }

      return isAuthenticated;
    }

    public static Response checkRequest(Request body)
    {
      if (body == null)
        return new Response
        {
          status = 400,
          title = "Invalid data"
        };

      if (!Authenticate(body.token))
        return new Response
        {
          status = 403,
          title = "Invalid token"
        };

      return null;
    }

    public static Response checkRequest(Request body, string auth)
    {
      if (body == null)
        return new Response
        {
          status = 400,
          title = "Invalid data"
        };

      if (string.IsNullOrEmpty(auth) || !Authenticate(body.token, auth))
        return new Response
        {
          status = 403,
          title = "Access denied"
        };

      return null;
    }
  }
}

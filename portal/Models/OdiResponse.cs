﻿using System;
using System.ComponentModel.DataAnnotations;
using TDMS.Portal.Contexts;

namespace TDMS.Portal.Models
{
  public class OdiResponse
  {
    public string status { get; set; } = "";

    public string message { get; set; } = "";
  }
}

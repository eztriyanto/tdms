﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDMS.Portal.Models
{
  public class HoseDeliverySummary
  {
    [Display(Name = "MOR", GroupName = "Export")]
    public string MOR { get; set; } = "";

    [Display(Name = "Total SPBU", GroupName = "Hidden")]
    public int TotalSite { get; set; } = 0;

    [Display(Name = "Total SPBU BIO SOLAR", GroupName = "Export")]
    public int TotalSite12 { get; set; } = 0;

    [Display(Name = "Transaksi Prepurchase BIO SOLAR", GroupName = "Export")]
    public int TotalPrePurchase12 { get; set; } = 0;

    [Display(Name = "Total Transaksi BIO SOLAR", GroupName = "Export")]
    public int TotalDelivery12 { get; set; } = 0;

    [Display(Name = "Perf PrePurchase BIO SOLAR", GroupName = "Export")]
    public double PerfPrePurchase12 { get => TotalDelivery12 != 0 ? (double)TotalPrePurchase12 / TotalDelivery12 * 100 : 0; }

    [Display(Name = "Total SPBU PREMIUM", GroupName = "Hidden")]
    public int TotalSite11 { get; set; } = 0;
    
    [Display(Name = "Transaksi Prepurchase PREMIUM", GroupName = "Hidden")]
    public int TotalPrePurchase11 { get; set; } = 0;

    [Display(Name = "Total Transaksi PREMIUM", GroupName = "Hidden")]
    public int TotalDelivery11 { get; set; } = 0;

    [Display(Name = "Perf PrePurchase PREMIUM", GroupName = "Hidden")]
    public double PerfPrePurchase11 { get => TotalDelivery11 != 0 ? (double)TotalPrePurchase11 / TotalDelivery11 * 100 : 0; }
  }
}

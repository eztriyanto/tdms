﻿namespace TDMS.Portal.Models
{
  public class Chart
  {
    public class dataSet
    {
      public string label { get; set; } = "";
      public double value { get; set; } = 0;
    }
  }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDMS.Portal.Models
{
  public class HoseDeliverySummaryClassification
  {
    [Display(Name = "Klasifikasi", GroupName = "Export")]
    public string Display { get; set; } = "";

    [Display(Name = "MOR 1 BS", GroupName = "Export")]
    public int Mor_1_12 { get; set; } = 0;

    [Display(Name = "MOR 1 PR", GroupName = "Hidden")]
    public int Mor_1_11 { get; set; } = 0;

    [Display(Name = "MOR 2 BS", GroupName = "Export")]
    public int Mor_2_12 { get; set; } = 0;

    [Display(Name = "MOR 2 PR", GroupName = "Hidden")]
    public int Mor_2_11 { get; set; } = 0;

    [Display(Name = "MOR 3 BS", GroupName = "Export")]
    public int Mor_3_12 { get; set; } = 0;

    [Display(Name = "MOR 3 PR", GroupName = "Hidden")]
    public int Mor_3_11 { get; set; } = 0;

    [Display(Name = "MOR 4 BS", GroupName = "Export")]
    public int Mor_4_12 { get; set; } = 0;

    [Display(Name = "MOR 4 PR", GroupName = "Hidden")]
    public int Mor_4_11 { get; set; } = 0;

    [Display(Name = "MOR 5 BS", GroupName = "Export")]
    public int Mor_5_12 { get; set; } = 0;

    [Display(Name = "MOR 5 PR", GroupName = "Hidden")]
    public int Mor_5_11 { get; set; } = 0;

    [Display(Name = "MOR 6 BS", GroupName = "Export")]
    public int Mor_6_12 { get; set; } = 0;

    [Display(Name = "MOR 6 PR", GroupName = "Hidden")]
    public int Mor_6_11 { get; set; } = 0;

    [Display(Name = "MOR 7 BS", GroupName = "Export")]
    public int Mor_7_12 { get; set; } = 0;

    [Display(Name = "MOR 7 PR", GroupName = "Hidden")]
    public int Mor_7_11 { get; set; } = 0;

    [Display(Name = "MOR 8 BS", GroupName = "Export")]
    public int Mor_8_12 { get; set; } = 0;

    [Display(Name = "MOR 8 PR", GroupName = "Hidden")]
    public int Mor_8_11 { get; set; } = 0;

    [Display(Name = "TOTAL BS", GroupName = "Export")]
    public int Total_12 { get => Mor_1_12 + Mor_2_12 + Mor_3_12 + Mor_4_12 + Mor_5_12 + Mor_6_12 + Mor_7_12 + Mor_8_12; }

    [Display(Name = "TOTAL PR", GroupName = "Hidden")]
    public int Total_11 { get => Mor_1_11 + Mor_2_11 + Mor_3_11 + Mor_4_11 + Mor_5_11 + Mor_6_11 + Mor_7_11 + Mor_8_11; }
  }
}

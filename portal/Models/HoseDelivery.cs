﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDMS.Portal.Models
{
  public class HoseDelivery
  {
    [Display(Name = "Site Id", GroupName = "Export")]
    public string SiteId { get; set; } = "";

    [Display(Name = "Site Name", GroupName = "Export")]
    public string SiteName { get; set; } = "";

    [Display(Name = "Site Address", GroupName = "Export")]
    public string SiteAddress { get; set; } = "";

    [Display(Name = "Delivery Date", GroupName = "Export")]
    public DateTime DeliveryDate { get; set; } = DateTime.MinValue;

    [Display(Name = "Oil Company Code", GroupName = "Export")]
    public string OilCompanyCode { get; set; } = "";

    [Display(Name = "Product", GroupName = "Export")]
    public string GradeName { get; set; } = "";

    [Display(Name = "Material Number", GroupName = "Export")]
    public string MaterialNumber { get; set; } = "";

    [Display(Name = "Total PrePurchase Delivery", GroupName = "Export")]
    public int TotalPrePurchaseDelivery { get; set; } = 0;

    [Display(Name = "Total Other Delivery", GroupName = "Hidden")]
    public int TotalOtherDelivery { get => TotalDelivery - TotalPrePurchaseDelivery; }

    [Display(Name = "Total Delivery", GroupName = "Export")]
    public int TotalDelivery { get; set; } = 0;

    [Display(Name = "Perf PrePurchase", GroupName = "Export")]
    public double PerfPrePurchase { get => TotalDelivery != 0 ? (double)TotalPrePurchaseDelivery / TotalDelivery * 100 : 0; }

    [Display(Name = "Inserted Date", GroupName = "Export")]
    public DateTime CreatedDate { get; set; } = DateTime.MinValue;

    [Display(Name = "Last Update", GroupName = "Export")]
    public DateTime UpdateDate { get; set; } = DateTime.MinValue;
  }
}

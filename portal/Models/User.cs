﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace TDMS.Portal.Models
{
  public class User
  {
    public enum RoleEnum { Transportir = 1, Depot = 2, Region = 3, Super_User = 8, Administrator = 9 }

    public string UserId { get; set; } = "";
    public string Password { get; set; } = "";
    public string UserName { get; set; } = "";
    public RoleEnum Role { get; set; } = RoleEnum.Transportir;
    public bool IsActive { get; set; } = true;

    public string AccessId { get; set; } = "";
    public string AccessName { get; set; } = "";
  }
}

﻿using System.Linq;

namespace TDMS.Portal.Models
{
  public class Table
  {
    public long total { get; set; } = 0;
    public long totalNotFiltered { get; set; } = 0;
    public int totalSite { get; set; } = 0;
    public object rows { get; set; } = null;
  }

}

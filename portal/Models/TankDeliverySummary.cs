﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDMS.Portal.Models
{
  public class TankDeliverySummary
  {
    [Display(Name = "MOR", GroupName = "Export")]
    public string MOR { get; set; } = "";

    [Display(Name = "Total SPBU", GroupName = "Hidden")]
    public int TotalSite { get; set; } = 0;

    [Display(Name = "Transaksi Tank Delivery", GroupName = "Export")]
    public int TotalDelivery { get; set; } = 0;

    [Display(Name = "Loss < -2%", GroupName = "Export")]
    public double Loss_2 { get; set; } = 0;

    [Display(Name = "Loss -2% to -1%", GroupName = "Export")]
    public double Loss_1 { get; set; } = 0;

    [Display(Name = "Loss -1% to -0.3%", GroupName = "Export")]
    public double Loss_03 { get; set; } = 0;

    [Display(Name = "Loss -0.3% to -0.15%", GroupName = "Export")]
    public double Loss_015 { get; set; } = 0;

    [Display(Name = "Loss -0.15% to 0%", GroupName = "Export")]
    public double Loss_0 { get; set; } = 0;

    [Display(Name = "Gain 0% to 0.15%", GroupName = "Export")]
    public double Gain_0 { get; set; } = 0;

    [Display(Name = "Gain 0.15% to 0.3%", GroupName = "Export")]
    public double Gain_015 { get; set; } = 0;

    [Display(Name = "Gain 0.3% to 1%", GroupName = "Export")]
    public double Gain_03 { get; set; } = 0;

    [Display(Name = "Gain 1% to 2%", GroupName = "Export")]
    public double Gain_1 { get; set; } = 0;

    [Display(Name = "Gain > 2%", GroupName = "Export")]
    public double Gain_2 { get; set; } = 0;
  }
}

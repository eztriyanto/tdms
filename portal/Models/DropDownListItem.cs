﻿namespace TDMS.Portal.Models
{
  public class DropDownListItem
  {
    public string text { get; set; } = "";
    public string value { get; set; } = "";
    public bool selected { get; set; } = false;
  }

}

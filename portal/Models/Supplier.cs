﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace TDMS.Portal.Models
{
  public class Supplier
  {
    public string SupplierId { get; set; } = "";
    public string SupplierName { get; set; } = "";
    public string SupplierAltName { get; set; } = "";
    public List<string> SupplierAltNameList
    {
      get
      {
        if (!string.IsNullOrEmpty(SupplierAltName))
          return SupplierAltName.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();

        return new List<string>();
      }
    }
  }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace TDMS.Portal.Models
{
  public class SupplyPoint
  {
    public string SupplyPointId { get; set; } = "";
    public string SupplyPointName { get; set; } = "";
    public string SupplyPointAltName { get; set; } = "";
    public Region SupplyPointRegion { get; set; } = new Region();

    public List<string> SupplyPointAltNameList 
    {
      get 
      {
        if (!string.IsNullOrEmpty(SupplyPointAltName))
          return SupplyPointAltName.Split(';', StringSplitOptions.RemoveEmptyEntries).ToList();

        return new List<string>();
      }
    }
  }
}

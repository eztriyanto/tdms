﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDMS.Portal
{
  public class ConfigJson
  {
    public string ConnectionString { get; set; }
    public string TokenAPI { get; set; }
    public string ApiUser { get; set; }
    public string ApiPassword { get; set; }
    public string OdiEndpoint { get; set; }
    public string OdiUserId { get; set; }
    public string OdiPassword { get; set; }
    public int OdiIntervalInSecond { get; set; }
  }
}

﻿namespace TDMS.Portal.Models
{
  public class Region
  {
    public string RegionId { get; set; } = "";
    public string RegionName { get; set; } = "";
    public int RegionNumber { get; set; } = 0;
  }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TDMS.Portal.Models
{
  public class TankDeliverySummaryBySite
  {
    [Display(Name = "Site Id", GroupName = "Export")]
    public string SiteId { get; set; } = "";

    [Display(Name = "Total DO Volume", GroupName = "Export")]
    public double TotalDOVolume { get; set; } = 0;

    [Display(Name = "Total Volume", GroupName = "Export")]
    public double TotalVolume { get; set; } = 0;

    [Display(Name = "Diff. Volume", GroupName = "Export")]
    public double DiffVolume { get => TotalVolume - TotalDOVolume; }

    [Display(Name = "Loss / Gain (%)", GroupName = "Export")]
    public double diffInPercent { get => TotalDOVolume == 0 ? 0 : DiffVolume / TotalDOVolume * 100 ; }
  }
}

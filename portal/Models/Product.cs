﻿using System;

namespace TDMS.Portal.Models
{
  public class Product
  {
    public Product()
    {
      ProductId = "";
      ProductName = "";
      MaterialNumber = "";
    }

    public string ProductId { get; set; }
    public string ProductName { get; set; }
    public string MaterialNumber { get; set; }
  }
}

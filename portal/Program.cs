using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TDMS.Portal.BackgroundServices;

namespace TDMS.Portal
{
  public class Program
  {
    public static void Main(string[] args)
    {
      CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
      Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(webBuilder => { 
          webBuilder.UseStartup<Startup>();
          //webBuilder.ConfigureKestrel((context, options) => {
          //  options.AllowSynchronousIO = true;
          //});
        });
        //.ConfigureServices(services => { services.AddHostedService<ODIService>(); });
  }
}

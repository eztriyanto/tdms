﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TDMS.Portal
{
  public class Helper
  {
    private static byte seed = 0x33;

    public static string Title = "Tank Delivery Monitoring System";

    public const string SQLDate = "yyyy-MM-dd";

    #region Styles
    public const string FormControl = "form-control";
    public const string FormControlCol = "form-control col";
    public const string FormControlSelect = "form-control custom-select";
    public const string FormControlSelectCol = "form-control custom-select col";
    public const string FormControlButtonDark = "btn btn-dark btn-icon-split";
    public const string FormControlButtonSuccess = "btn btn-success btn-icon-split";
    public const string FormControlButtonDanger = "btn btn-danger btn-icon-split";
    public const string FormControlButtonWarning = "btn btn-warning btn-icon-split";
    public const string FormControlButtonPrimary = "btn btn-primary btn-icon-split";
    public const string FormControlButtonInfo = "btn btn-info btn-icon-split";
    public const string FormControlNumber = "form-control number";
    public const string FormControlNumberCol = "form-control number col";
    #endregion
    
    public static string Encrypt(string text)
    {
      byte[] data = System.Text.Encoding.UTF8.GetBytes(text);
      for (int i = 0; i < data.Length; i++)
        data[i] = (byte)(data[i] ^ seed);

      return Convert.ToBase64String(data);
    }
    public static string Decrypt(string text)
    {
      byte[] data = Convert.FromBase64String(text);
      for (int i = 0; i < data.Length; i++)
        data[i] = (byte)(data[i] ^ seed);

      return System.Text.Encoding.UTF8.GetString(data);
    }

    public static Models.User.RoleEnum getRole(object value)
    {
      if (value != null)
        return (Models.User.RoleEnum)value;

      return Models.User.RoleEnum.Transportir;
    }
    public static object getAttrs(bool ReadOnly, bool Required, string cls = FormControl)
    {
      var length = new Dictionary<string, object>();

      if (ReadOnly && Required)
        return new { @class = cls, @readonly = "readonly", @required = "required" };

      else if (ReadOnly && !Required)
        return new { @class = cls, @readonly = "readonly" };

      else if (!ReadOnly && Required)
        return new { @class = cls, @required = "required" };

      return new { @class = cls };
    }
    public static object getAttrs(string id, bool ReadOnly, bool Required, string cls = FormControl)
    {
      var length = new Dictionary<string, object>();
      var _id = id ?? Guid.NewGuid().ToString();

      if (ReadOnly && Required)
        return new { @id = _id, @class = cls, @readonly = "readonly", @required = "required" };

      else if (ReadOnly && !Required)
        return new { @id = _id, @class = cls, @readonly = "readonly" };

      else if (!ReadOnly && Required)
        return new { @id = _id, @class = cls, @required = "required" };

      return new { @id = _id, @class = cls };
    }

    public static int OffsetToPage(int limit = 10, int offset = 0)
    {
      if (limit > 0)
        return (int)Math.Ceiling((double)(offset / limit)) + 1;

      return 0;
    }
  }
}

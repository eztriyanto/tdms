USE [DB_Tank_Monitoring]
GO
/****** Object:  Table [dbo].[tblM_User]    Script Date: 28/04/2020 19:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblM_User](
	[User_ID] [varchar](100) NOT NULL,
	[User_Name] [varchar](100) NOT NULL,
	[User_Password] [varchar](1000) NOT NULL,
	[User_Role] [varchar](2) NOT NULL,
	[User_Access_1] [varchar](MAX) NOT NULL,
	[User_Access_2] [varchar](MAX) NOT NULL,
	[Active] [bit] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Created_Date] [datetime] NOT NULL,
	[Update_Date] [datetime] NULL,
 CONSTRAINT [PK_tblM_User] PRIMARY KEY CLUSTERED 
(
	[User_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblM_Grade]    Script Date: 29/04/2020 03:59:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblM_Grade](
	[Oil_Company_Code] [varchar](2) NOT NULL,
	[Grade_Name] [varchar](50) NOT NULL,
	[Material_Number] [varchar](20) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Created_Date] [datetime] NOT NULL,
	[Update_Date] [datetime] NULL,
 CONSTRAINT [PK_tblM_Grade] PRIMARY KEY CLUSTERED 
(
	[Oil_Company_Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblM_Region]    Script Date: 29/04/2020 04:11:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblM_Region](
	[Region_ID] [varchar](10) NOT NULL,
	[Region_Name] [varchar](100) NOT NULL,
	[Region_Number] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Created_Date] [datetime] NOT NULL,
	[Update_Date] [datetime] NULL,
 CONSTRAINT [PK_tblM_Region] PRIMARY KEY CLUSTERED 
(
	[Region_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblM_Supply_Point]    Script Date: 29/04/2020 04:14:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblM_Supply_Point](
	[Supply_Point_ID] [varchar](10) NOT NULL,
	[Supply_Point_Name] [varchar](100) NOT NULL,
	[Supply_Point_Alt_Name] [varchar](MAX) NOT NULL,
	[Region_ID] [varchar](10) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Created_Date] [datetime] NOT NULL,
	[Update_Date] [datetime] NULL,
 CONSTRAINT [PK_tblM_Supply_Point] PRIMARY KEY CLUSTERED 
(
	[Supply_Point_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblM_Supply_Point]  WITH CHECK ADD  CONSTRAINT [FK_tblM_Supply_Point_tblM_Region] FOREIGN KEY([Region_ID])
REFERENCES [dbo].[tblM_Region] ([Region_ID])
GO

ALTER TABLE [dbo].[tblM_Supply_Point] CHECK CONSTRAINT [FK_tblM_Supply_Point_tblM_Region]
GO
/****** Object:  Table [dbo].[tblM_Supplier]    Script Date: 04/05/2020 07:16:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblM_Supplier](
	[Supplier_ID] [varchar](10) NOT NULL,
	[Supplier_Name] [varchar](100) NOT NULL,
	[Supplier_Alt_Name] [varchar](MAX) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Created_Date] [datetime] NOT NULL,
	[Update_Date] [datetime] NULL,
 CONSTRAINT [PK_tblM_Supplier] PRIMARY KEY CLUSTERED 
(
	[Supplier_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblT_Tank_Delivery]    Script Date: 28/04/2020 19:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblT_Tank_Delivery_Temp](
	[ID] [bigint] NOT NULL,
	[Tank_Delivery_ID] [int] NOT NULL,
	[Site_ID] [varchar](15) NOT NULL,
	[Site_Name] [varchar](100) NOT NULL,
	[Site_Address] [varchar](100) NOT NULL,
	[Tank_ID] [int] NOT NULL,
	[Tank_Name] [varchar](100) NOT NULL,
	[Grade_ID] [int] NOT NULL,
	[Grade_Name] [varchar](100) NOT NULL,
	[Oil_Company_Code] [varchar](10) NOT NULL,
	[Period_ID] [int] NOT NULL,
	[Period_Number] [int] NOT NULL,
	[Drop_Date_Time] [datetime] NOT NULL,
	[Record_Entry_TS] [datetime] NOT NULL,
	[Drop_Volume] [decimal](15, 4) NOT NULL,
	[Delivery_Note_Num] [varchar](10) NOT NULL,
	[Drop_Volume_Theo] [decimal](15, 4) NOT NULL,
	[Unit_Cost_Price] [money] NOT NULL,
	[Driver_ID_Code] [varchar](10) NOT NULL,
	[Tanker_ID_Code] [varchar](10) NOT NULL,
	[Delivery_Detail] [varchar](40) NOT NULL,
	[Dispatched_Volume] [decimal](15, 4) NOT NULL,
	[Original_Invoice_Number] [varchar](10) NOT NULL,
	[Received_Vol_At_Ref_Temp] [decimal](15, 4) NOT NULL,
	[Dispatched_Vol_At_Ref_Temp] [decimal](15, 4) NOT NULL,
	[Total_Variance] [decimal](15, 4) NOT NULL,
	[Variance_At_Ref_Temp] [decimal](15, 4) NOT NULL,
	[Temperature_Variance] [decimal](15, 4) NOT NULL,
	[Tank_Movement_Type_ID] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[User_Name] [varchar](100) NOT NULL,
	[Deleted] [bit] NOT NULL,
	[Created_Date] [datetime] NOT NULL,
	[Update_Date] [datetime] NULL,
 CONSTRAINT [PK_tblT_Tank_Delivery] PRIMARY KEY NONCLUSTERED 
(
	[Tank_Delivery_ID] ASC,
	[Site_ID] ASC,
	[Tank_ID] ASC,
	[Oil_Company_Code] ASC,
	[Drop_Date_Time] ASC,
	[Record_Entry_TS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblR_Tank_Delivery_Sync]    Script Date: 14/08/2020 12:48:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tblR_Tank_Delivery_Sync](
	[ID] [bigint] NOT NULL,
	[Tank_Delivery_Index] [bigint] NOT NULL,
	[Sync_Date] [datetime] NOT NULL,
 CONSTRAINT [PK_tblR_Tank_Delivery_Sync] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblT_Hose_Delivery]    Script Date: 20/08/2020 09:13:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tblT_Hose_Delivery](
	[Site_ID] [varchar](15) NOT NULL,
	[Site_Name] [varchar](100) NOT NULL,
	[Site_Address] [varchar](100) NOT NULL,
	[Delivery_Date] [datetime] NOT NULL,
	[Oil_Company_Code] [varchar](10) NOT NULL,
	[Grade_Name] [varchar](100) NOT NULL,
	[Total_Delivery] [int] NOT NULL,
	[Total_PrePurchase] [int] NOT NULL,
	[Created_Date] [datetime] NOT NULL,
	[Update_Date] [datetime] NULL,
 CONSTRAINT [PK_tblT_Hose_Delivery] PRIMARY KEY CLUSTERED 
(
	[Site_ID] ASC,
	[Delivery_Date] ASC,
	[Oil_Company_Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vw_Supply_Point]    Script Date: 29/04/2020 05:10:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_Supply_Point]
AS
SELECT a.Supply_Point_ID, a.Supply_Point_Name, a.Supply_Point_Alt_Name,  
	a.Region_ID, a.Deleted, a.Created_Date, a.Update_Date, 
	ISNULL(b.Region_Name, '') AS Region_Name
FROM   dbo.tblM_Supply_Point AS a LEFT OUTER JOIN
       dbo.tblM_Region AS b ON a.Region_ID = b.Region_ID
GO
/****** Object:  UserDefinedFunction [dbo].[fc_Weekly_Sum]    Script Date: 28/04/2020 19:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[fc_Weekly_Sum] 
(	
	@start date
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT CAST(Drop_Date_Time AS DATE) AS Del_Date, SUM(Drop_Volume) / 1000 AS Del_Volume
	FROM tblT_Tank_Delivery
	WHERE Deleted=0 AND CAST(Drop_Date_Time AS DATE) BETWEEN DATEADD(dd,-7,@start) AND @start
	GROUP BY CAST(Drop_Date_Time AS DATE)
)

GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Tank_Delivery_Summary]    Script Date: 07/09/2020 12:05:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Get_Tank_Delivery_Summary]
	@oil_company_code varchar(10),
	@start datetime,
	@end datetime
AS
BEGIN
	SET NOCOUNT ON;

	IF @oil_company_code IS NULL OR @oil_company_code  = ''
	BEGIN
		SELECT mor, '' oil_company_code,
			COUNT(DISTINCT site_id) AS total_site,
			COUNT(site_id) AS total_delivery,
			SUM(CASE WHEN loss_gain < -2 THEN 1 ELSE 0 END) AS loss_2,
			SUM(CASE WHEN loss_gain < -1 AND loss_gain >= -2 THEN 1 ELSE 0 END) AS loss_1,
			SUM(CASE WHEN loss_gain < -0.3 AND loss_gain >= -1 THEN 1 ELSE 0 END) AS loss_03,
			SUM(CASE WHEN loss_gain < -0.15 AND loss_gain >= -0.3 THEN 1 ELSE 0 END) AS loss_015,
			SUM(CASE WHEN loss_gain < 0 AND loss_gain >= -0.15 THEN 1 ELSE 0 END) AS loss_0,
			SUM(CASE WHEN loss_gain < 0.15 AND loss_gain >= 0 THEN 1 ELSE 0 END) AS gain_0,
			SUM(CASE WHEN loss_gain < 0.3 AND loss_gain >= 0.15 THEN 1 ELSE 0 END) AS gain_015,
			SUM(CASE WHEN loss_gain < 1 AND loss_gain >= 0.3 THEN 1 ELSE 0 END) AS gain_03,
			SUM(CASE WHEN loss_gain <= 2 AND loss_gain >= 1 THEN 1 ELSE 0 END) AS gain_1,
			SUM(CASE WHEN loss_gain > 2 THEN 1 ELSE 0 END) AS gain_2
		FROM (
			SELECT *,
				'MOR ' + LEFT(site_id,1) AS mor,
				drop_volume - dispatched_volume AS diff_volume,
				(drop_volume - dispatched_volume) / dispatched_volume AS loss_gain
			FROM tblt_tank_delivery 
			WHERE drop_date_time BETWEEN @start AND @end AND LEFT(site_id, 1) < 9 AND deleted = 0
		) x 
		GROUP BY mor ORDER BY mor
	END
	ELSE
	BEGIN
		SELECT mor, oil_company_code,
			COUNT(DISTINCT site_id) AS total_site,
			COUNT(site_id) AS total_delivery,
			SUM(CASE WHEN loss_gain < -2 THEN 1 ELSE 0 END) AS loss_2,
			SUM(CASE WHEN loss_gain < -1 AND loss_gain >= -2 THEN 1 ELSE 0 END) AS loss_1,
			SUM(CASE WHEN loss_gain < -0.3 AND loss_gain >= -1 THEN 1 ELSE 0 END) AS loss_03,
			SUM(CASE WHEN loss_gain < -0.15 AND loss_gain >= -0.3 THEN 1 ELSE 0 END) AS loss_015,
			SUM(CASE WHEN loss_gain < 0 AND loss_gain >= -0.15 THEN 1 ELSE 0 END) AS loss_0,
			SUM(CASE WHEN loss_gain < 0.15 AND loss_gain >= 0 THEN 1 ELSE 0 END) AS gain_0,
			SUM(CASE WHEN loss_gain < 0.3 AND loss_gain >= 0.15 THEN 1 ELSE 0 END) AS gain_015,
			SUM(CASE WHEN loss_gain < 1 AND loss_gain >= 0.3 THEN 1 ELSE 0 END) AS gain_03,
			SUM(CASE WHEN loss_gain <= 2 AND loss_gain >= 1 THEN 1 ELSE 0 END) AS gain_1,
			SUM(CASE WHEN loss_gain > 2 THEN 1 ELSE 0 END) AS gain_2
		FROM (
			SELECT *,
				'MOR ' + LEFT(site_id,1) AS mor,
				drop_volume - dispatched_volume AS diff_volume,
				(drop_volume - dispatched_volume) / dispatched_volume AS loss_gain
			FROM tblt_tank_delivery 
			WHERE drop_date_time BETWEEN @start AND @end AND LEFT(site_id, 1) < 9 AND deleted = 0
		) x 
		WHERE oil_company_code = @oil_company_code
		GROUP BY mor, oil_company_code ORDER BY mor
	END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Hose_Delivery_Summary]    Script Date: 07/09/2020 08:44:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Get_Hose_Delivery_Summary]
	@start datetime,
	@end datetime
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 'MOR ' + LEFT(site_id,1) AS mor,
		COUNT(DISTINCT site_id) AS total_site,
		COUNT(DISTINCT (CASE oil_company_code WHEN '12' THEN site_id END)) AS total_site_12,
		SUM(CASE oil_company_code WHEN '12' THEN total_prepurchase ELSE 0 END) AS total_prepurchase_12,
		SUM(CASE oil_company_code WHEN '12' THEN total_delivery ELSE 0 END) AS total_delivery_12,
		COUNT(DISTINCT (CASE oil_company_code WHEN '11' THEN site_id END)) AS total_site_11,
		SUM(CASE oil_company_code WHEN '11' THEN total_prepurchase ELSE 0 END) AS total_prepurchase_11,
		SUM(CASE oil_company_code WHEN '11' THEN total_delivery ELSE 0 END) AS total_delivery_11 
	FROM tblt_hose_delivery WHERE delivery_date BETWEEN @start AND @end AND LEFT(site_id, 1) < 9 
	GROUP BY LEFT(site_id, 1) ORDER BY mor
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Get_Hose_Delivery_Summary_Classification]    Script Date: 07/09/2020 08:44:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Get_Hose_Delivery_Summary_Classification]
	@start datetime,
	@end datetime
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 'MOR ' + LEFT(site_id, 1) AS mor,
		SUM(CASE WHEN oil_company_code = '12' AND perf >= 100 THEN 1 ELSE 0 END) AS perf_100_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf >= 100 THEN 1 ELSE 0 END) AS perf_100_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 90 AND 99 THEN 1 ELSE 0 END) AS perf_90_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 90 AND 99 THEN 1 ELSE 0 END) AS perf_90_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 80 AND 89 THEN 1 ELSE 0 END) AS perf_80_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 80 AND 89 THEN 1 ELSE 0 END) AS perf_80_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 70 AND 79 THEN 1 ELSE 0 END) AS perf_70_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 70 AND 79 THEN 1 ELSE 0 END) AS perf_70_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 60 AND 69 THEN 1 ELSE 0 END) AS perf_60_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 60 AND 69 THEN 1 ELSE 0 END) AS perf_60_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 50 AND 59 THEN 1 ELSE 0 END) AS perf_50_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 50 AND 59 THEN 1 ELSE 0 END) AS perf_50_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 40 AND 49 THEN 1 ELSE 0 END) AS perf_40_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 40 AND 49 THEN 1 ELSE 0 END) AS perf_40_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 30 AND 39 THEN 1 ELSE 0 END) AS perf_30_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 30 AND 39 THEN 1 ELSE 0 END) AS perf_30_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 20 AND 29 THEN 1 ELSE 0 END) AS perf_20_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 20 AND 29 THEN 1 ELSE 0 END) AS perf_20_11,
		SUM(CASE WHEN oil_company_code = '12' AND perf BETWEEN 10 AND 19 THEN 1 ELSE 0 END) AS perf_10_12,
		SUM(CASE WHEN oil_company_code = '11' AND perf BETWEEN 10 AND 19 THEN 1 ELSE 0 END) AS perf_10_11,
		SUM(CASE WHEN oil_company_code = '12' AND total_prepurchase > 0 AND perf BETWEEN 0 AND 9 THEN 1 ELSE 0 END) AS perf_0_12,
		SUM(CASE WHEN oil_company_code = '11' AND total_prepurchase > 0 AND perf BETWEEN 0 AND 9 THEN 1 ELSE 0 END) AS perf_0_11,
		SUM(CASE WHEN oil_company_code = '12' AND total_prepurchase <= 0 THEN 1 ELSE 0 END) AS perf_none_12,
		SUM(CASE WHEN oil_company_code = '11' AND total_prepurchase <= 0 THEN 1 ELSE 0 END) AS perf_none_11 
	FROM (
		SELECT site_id, oil_company_code, SUM(total_prepurchase) AS total_prepurchase, 
			ROUND(CAST(SUM(total_prepurchase) AS float) / SUM(total_delivery) * 100, 0) AS perf 
		FROM tblt_hose_delivery WHERE delivery_date BETWEEN @start AND @end AND LEFT(site_id, 1) < 9 GROUP BY site_id, oil_company_code
	) x GROUP BY LEFT(site_id, 1) ORDER BY mor
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Tank_Delivery_Save]    Script Date: 28/04/2020 19:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Tank_Delivery_Save]
	@Tank_Delivery_ID int,
	@Site_ID varchar(15),
	@Site_Name varchar(100),
	@Site_Address varchar(100),
	@Tank_ID int,
	@Tank_Name varchar(100),
	@Grade_ID int,
	@Grade_Name varchar(100),
	@Oil_Company_Code varchar(10),
	@Period_ID int,
	@Period_Number int,
	@Drop_Date_Time datetime,
	@Record_Entry_TS datetime,
	@Drop_Volume float,
	@Delivery_Note_Num varchar(10),
	@Drop_Volume_Theo float,
	@Unit_Cost_Price float,
	@Driver_ID_Code varchar(10),
	@Tanker_ID_Code varchar(10),
	@Delivery_Detail varchar(40),
	@Dispatched_Volume float,
	@Original_Invoice_Number varchar(10),
	@Received_Vol_At_Ref_Temp float,
	@Dispatched_Vol_At_Ref_Temp float,
	@Total_Variance float,
	@Variance_At_Ref_Temp float,
	@Temperature_Variance float,
	@Tank_Movement_Type_ID int,
	@User_ID int,
	@User_Name varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ID int = 0

	SELECT @ID = ID FROM tblT_Tank_Delivery
	WHERE Site_ID = @Site_ID AND Tank_ID=@Tank_ID 
	AND Record_Entry_TS = @Record_Entry_TS AND Drop_Date_Time=@Drop_Date_Time

	SET @ID=ISNULL(@ID, 0)

	IF @ID > 0
	BEGIN
		UPDATE tblT_Tank_Delivery
		SET Tank_Delivery_ID = @Tank_Delivery_ID, Site_ID = @Site_ID, Site_Name = @Site_Name
			,Site_Address = @Site_Address, Tank_ID = @Tank_ID, Tank_Name = @Tank_Name
			,Grade_ID = @Grade_ID, Grade_Name = @Grade_Name, Oil_Company_Code = @Oil_Company_Code
			,Period_ID = @Period_ID, Period_Number = @Period_Number
			,Drop_Date_Time = @Drop_Date_Time, Record_Entry_TS = @Record_Entry_TS
			,Drop_Volume = @Drop_Volume, Delivery_Note_Num = @Delivery_Note_Num
			,Drop_Volume_Theo = @Drop_Volume_Theo, Unit_Cost_Price = @Unit_Cost_Price
			,Driver_ID_Code = @Driver_ID_Code, Tanker_ID_Code = @Tanker_ID_Code
			,Delivery_Detail = @Delivery_Detail, Dispatched_Volume = @Dispatched_Volume
			,Original_Invoice_Number = @Original_Invoice_Number
			,Received_Vol_At_Ref_Temp = @Received_Vol_At_Ref_Temp
			,Dispatched_Vol_At_Ref_Temp = @Dispatched_Vol_At_Ref_Temp
			,Total_Variance = @Total_Variance, Variance_At_Ref_Temp = @Variance_At_Ref_Temp
			,Temperature_Variance = @Temperature_Variance, Tank_Movement_Type_ID = @Tank_Movement_Type_ID
			,User_ID = @User_ID, User_Name = @User_Name
			,Deleted=0, Update_Date=GETDATE()
		WHERE ID=@ID
	END
	ELSE
	BEGIN
		SELECT @ID=ISNULL(MAX(ID),0) + 1 FROM tblT_Tank_Delivery

		INSERT INTO tblT_Tank_Delivery(ID,Tank_Delivery_ID,Site_ID,Site_Name,Site_Address
           ,Tank_ID,Tank_Name,Grade_ID,Grade_Name,Oil_Company_Code,Period_ID,Period_Number
           ,Drop_Date_Time,Record_Entry_TS,Drop_Volume,Delivery_Note_Num,Drop_Volume_Theo
           ,Unit_Cost_Price,Driver_ID_Code,Tanker_ID_Code,Delivery_Detail
           ,Dispatched_Volume,Original_Invoice_Number,Received_Vol_At_Ref_Temp
           ,Dispatched_Vol_At_Ref_Temp,Total_Variance,Variance_At_Ref_Temp
           ,Temperature_Variance,Tank_Movement_Type_ID,User_ID,User_Name
           ,Deleted,Created_Date) 
		VALUES(@ID,@Tank_Delivery_ID,@Site_ID,@Site_Name,@Site_Address
           ,@Tank_ID,@Tank_Name,@Grade_ID,@Grade_Name,@Oil_Company_Code,@Period_ID,@Period_Number
           ,@Drop_Date_Time,@Record_Entry_TS,@Drop_Volume,@Delivery_Note_Num,@Drop_Volume_Theo
           ,@Unit_Cost_Price,@Driver_ID_Code,@Tanker_ID_Code,@Delivery_Detail
           ,@Dispatched_Volume,@Original_Invoice_Number,@Received_Vol_At_Ref_Temp
           ,@Dispatched_Vol_At_Ref_Temp,@Total_Variance,@Variance_At_Ref_Temp
           ,@Temperature_Variance,@Tank_Movement_Type_ID,@User_ID,@User_Name
           ,0,GETDATE())
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_User_Delete]    Script Date: 28/04/2020 19:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_User_Delete]
	@ID varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblM_User 
	SET Deleted=1, Update_Date=GETDATE()
	WHERE User_ID=@ID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_User_Save]    Script Date: 28/04/2020 19:09:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_User_Save]
	@ID varchar(100),
	@Name varchar(100),
	@Password varchar(1000),
	@Role varchar(2),
	@Access_1 varchar(max),
	@Access_2 varchar(max),
	@Active bit
AS
BEGIN
	SET NOCOUNT ON;

	SET @Access_2 = ''
	SET @Access_1 = ISNULL(@Access_1,'')
	
	IF @Access_1 <> ''
	BEGIN
		IF @Role = 1 --Transportir
		BEGIN
			SELECT @Access_2 = Supplier_Name FROM tblM_Supplier WHERE Supplier_ID = @Access_1
		END
		ELSE
		BEGIN
			IF @Role = 2 --Depot
			BEGIN
				SELECT @Access_2 = Supply_Point_Name FROM tblM_Supply_Point WHERE Supply_Point_ID = @Access_1
			END
			ELSE
			BEGIN
				IF @Role = 3 --Region
				BEGIN
					SELECT @Access_2 = Region_Name FROM tblM_Region WHERE Region_ID = @Access_1
				END
			END
		END
	END
	SET @Access_2 = ISNULL(@Access_2,'')

	IF EXISTS(SELECT User_ID FROM tblM_User WHERE User_ID=@ID)
	BEGIN
		UPDATE tblM_User 
		SET User_Name=@Name, User_Password=@Password, User_Role=@Role, User_Access_1=@Access_1, User_Access_2=@Access_2,
			Active=@Active, Deleted=0, Update_Date=GETDATE()
		WHERE User_ID=@ID
	END
	ELSE
	BEGIN
		INSERT INTO tblM_User(User_ID, User_Name, User_Password, User_Role, User_Access_1, User_Access_2, Active, Deleted, Created_Date)
		VALUES(@ID, @Name, @Password, @Role, @Access_1, @Access_2, @Active, 0, GETDATE())
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Grade_Delete]    Script Date: 29/04/2020 04:04:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Grade_Delete]
	@Oil_Company_Code varchar(2)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblM_Grade
	SET Deleted=1, Update_Date=GETDATE()
	WHERE Oil_Company_Code=@Oil_Company_Code
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Grade_Save]    Script Date: 29/04/2020 04:07:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Grade_Save]
	@Oil_Company_Code varchar(2),
	@Name varchar(50),
	@Material_Number VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT Oil_Company_Code FROM tblM_Grade WHERE Oil_Company_Code=@Oil_Company_Code)
	BEGIN
		UPDATE tblM_Grade 
		SET Grade_Name=@Name, Material_Number=@Material_Number, Deleted=0, Update_Date=GETDATE()
		WHERE Oil_Company_Code=@Oil_Company_Code
	END
	ELSE
	BEGIN
		INSERT INTO tblM_Grade(Oil_Company_Code, Grade_Name, Material_Number, Deleted, Created_Date)
		VALUES(@Oil_Company_Code, @Name, @Material_Number, 0, GETDATE())
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Region_Delete]    Script Date: 29/04/2020 04:15:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Region_Delete]
	@Region_ID varchar(10)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblM_Region
	SET Deleted=1, Update_Date=GETDATE()
	WHERE Region_ID=@Region_ID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Region_Save]    Script Date: 29/04/2020 04:16:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Region_Save]
	@ID varchar(10),
	@Name varchar(50),
	@Number int
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT Region_ID FROM tblM_Region WHERE Region_ID=@ID)
	BEGIN
		UPDATE tblM_Region 
		SET Region_Name=@Name, Region_Number=@Number, Deleted=0, Update_Date=GETDATE()
		WHERE Region_ID=@ID
	END
	ELSE
	BEGIN
		INSERT INTO tblM_Region(Region_ID, Region_Name, Region_Number, Deleted, Created_Date)
		VALUES(@ID, @Name, @Number, 0, GETDATE())
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Supply_Point_Delete]    Script Date: 29/04/2020 04:20:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Supply_Point_Delete]
	@ID varchar(10)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblM_Supply_Point
	SET Deleted=1, Update_Date=GETDATE()
	WHERE Supply_Point_ID=@ID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Supply_Point_Save]    Script Date: 29/04/2020 04:23:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_Supply_Point_Save]
	@ID varchar(10),
	@Name varchar(100),
	@Alt_Name varchar(MAX),
	@Region varchar(10)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT Supply_Point_ID FROM tblM_Supply_Point WHERE Supply_Point_ID=@ID)
	BEGIN
		UPDATE tblM_Supply_Point 
		SET Supply_Point_Name=@Name, Supply_Point_Alt_Name=@Alt_Name,
			  Region_ID=@Region, Deleted=0, Update_Date=GETDATE()
		WHERE Supply_Point_ID=@ID
	END
	ELSE
	BEGIN
		INSERT INTO tblM_Supply_Point(Supply_Point_ID, Supply_Point_Name, Supply_Point_Alt_Name, Region_ID, Deleted, Created_Date)
		VALUES(@ID, @Name, @Alt_Name, @Region, 0, GETDATE())
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Supplier_Delete]    Script Date: 04/05/2020 07:17:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Supplier_Delete]
	@Supplier_ID varchar(10)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE tblM_Supplier
	SET Deleted=1, Update_Date=GETDATE()
	WHERE Supplier_ID=@Supplier_ID
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Supplier_Save]    Script Date: 04/05/2020 07:18:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Supplier_Save]
	@ID varchar(10),
	@Name varchar(50),
	@Alt_Name varchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT Supplier_ID FROM tblM_Supplier WHERE Supplier_ID=@ID)
	BEGIN
		UPDATE tblM_Supplier 
		SET Supplier_Name=@Name, Supplier_Alt_Name=@Alt_Name, Deleted=0, Update_Date=GETDATE()
		WHERE Supplier_ID=@ID
	END
	ELSE
	BEGIN
		INSERT INTO tblM_Supplier(Supplier_ID, Supplier_Name, Supplier_Alt_Name, Deleted, Created_Date)
		VALUES(@ID, @Name, @Alt_Name, 0, GETDATE())
	END
END

GO
/****** Object:  StoredProcedure [dbo].[sp_Tank_Delivery_Sync_Save]    Script Date: 14/08/2020 12:50:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Tank_Delivery_Sync_Save]
	@Tank_Delivery_Index bigint
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ID bigint = 1

	SELECT @ID=ISNULL(MAX(ID),0) + 1 FROM tblR_Tank_Delivery_Sync

	INSERT INTO tblR_Tank_Delivery_Sync(ID, Tank_Delivery_Index, Sync_Date)
	VALUES(@ID, @Tank_Delivery_Index, GETDATE())
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Hose_Delivery_Save]    Script Date: 20/08/2020 09:34:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_Hose_Delivery_Save]
	@Site_ID varchar(20),
	@Site_Name varchar(100),
	@Site_Address varchar(100),
	@Delivery_Date datetime,
	@Oil_Company_Code varchar(10),
	@Grade_Name varchar(100),
	@Total_Delivery int,
	@Total_PrePurchase int
AS
BEGIN
	SET NOCOUNT ON;

	IF EXISTS(SELECT Site_ID FROM tblT_Hose_Delivery 
		WHERE Site_ID=@Site_ID AND Delivery_Date=@Delivery_Date AND Oil_Company_Code=@Oil_Company_Code)
	BEGIN
		UPDATE tblT_Hose_Delivery 
		SET Site_Name=@Site_Name, Site_Address=@Site_Address, 
			Grade_Name=@Grade_Name, Total_Delivery=@Total_Delivery, Total_PrePurchase=@Total_PrePurchase,
			Update_Date=GETDATE()
		WHERE Site_ID=@Site_ID AND Delivery_Date=@Delivery_Date AND Oil_Company_Code=@Oil_Company_Code
	END
	ELSE
	BEGIN
		INSERT INTO tblT_Hose_Delivery(Site_ID, Site_Name, Site_Address, Delivery_Date, 
			Oil_Company_Code, Grade_Name, Total_Delivery, Total_PrePurchase, Created_Date)
		VALUES(@Site_ID, @Site_Name, @Site_Address, @Delivery_Date, 
			@Oil_Company_Code, @Grade_Name, @Total_Delivery, @Total_PrePurchase, GETDATE())
	END
END
GO
/****** Object:  Function [dbo].[fn_Get_Sync_Date]  Script Date: 21/08/2020 10:26:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION fn_Get_Sync_Date 
(
	@Tank_Delivery_Index int
)
RETURNS datetime
AS
BEGIN
	DECLARE @Result datetime

	SELECT @Result=MAX(sync_date) 
	FROM tblr_tank_delivery_sync 
	WHERE tank_delivery_index = @Tank_Delivery_Index

	RETURN @Result
END
GO
/****** Object:  Insert initial data [dbo].[tblM_Grade]    Script Date: 28/04/2020 19:09:44 ******/
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('11','PREMIUM',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('12','BIO SOLAR',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('13','SOLAR',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('14','SOLAR NON PSO',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('15','PERTAMAX',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('16','PERTAMAX PLUS',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('17','PERTAMAX RACING',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('18','PERTAMINA DEX',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('19','VIGAS',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('20','BBG',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('21','BIO PREMIUM',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('22','BIO PERTAMAX',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('23','PERTAMINA DEX DRUM 200L',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('24','BIOSOLAR INDUSTRI',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('25','PERTALITE',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('26','DEXLITE',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('27','PERTAMAX TURBO',0,'2020-04-29')
INSERT INTO tblM_Grade(Oil_Company_Code,Grade_Name,Deleted,Created_Date) VALUES('28','BBG INDUSTRI',0,'2020-04-29')

GO
/****** Object:  Insert initial data [dbo].[tblM_Region]    Script Date: 28/04/2020 19:09:44 ******/
INSERT INTO tblM_Region(Region_ID,Region_Name,Deleted,Created_Date) VALUES('MOR 1','MOR I',0,'2020-04-29')
INSERT INTO tblM_Region(Region_ID,Region_Name,Deleted,Created_Date) VALUES('MOR 2','MOR II',0,'2020-04-29')
INSERT INTO tblM_Region(Region_ID,Region_Name,Deleted,Created_Date) VALUES('MOR 3','MOR III',0,'2020-04-29')
INSERT INTO tblM_Region(Region_ID,Region_Name,Deleted,Created_Date) VALUES('MOR 4','MOR IV',0,'2020-04-29')
INSERT INTO tblM_Region(Region_ID,Region_Name,Deleted,Created_Date) VALUES('MOR 5','MOR V',0,'2020-04-29')
INSERT INTO tblM_Region(Region_ID,Region_Name,Deleted,Created_Date) VALUES('MOR 6','MOR VI',0,'2020-04-29')
INSERT INTO tblM_Region(Region_ID,Region_Name,Deleted,Created_Date) VALUES('MOR 7','MOR VII',0,'2020-04-29')
INSERT INTO tblM_Region(Region_ID,Region_Name,Deleted,Created_Date) VALUES('MOR 8','MOR VIII',0,'2020-04-29')

GO

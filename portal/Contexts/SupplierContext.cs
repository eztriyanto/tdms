﻿using System;
using System.Data;
using System.Collections.Generic;
using TDMS.Portal.Models;
using static TDMS.Portal.Helper;
using static TDMS.Portal.DBHelper;

namespace TDMS.Portal.Contexts
{
  public class SupplierContext
  {
    private static string tSQL = "SELECT supplier_id,supplier_name,supplier_alt_name " + 
      "FROM tblm_supplier ";
    private static string tOrder = " ORDER BY supplier_name";

    private static Tuple<string, List<Parameter>> tFilter(string value = "")
    {
      string filter = " WHERE deleted=0";
      List<Parameter> prms = null;

      if (!string.IsNullOrEmpty(value))
      {
        filter += " AND (supplier_id LIKE CONCAT('%',@id,'%') OR supplier_name LIKE CONCAT('%',@name,'%')) ";
        prms = new List<Parameter>
        {
          new Parameter { Name = "@id", Value = value },
          new Parameter { Name = "@name", Value = value }
        };
      };

      return Tuple.Create(filter, prms);
    }

    private static string DoCommand(NonQueryEnum type, Supplier data)
    {
      string q = "sp_Supplier_Delete";
      List<Parameter> prms = new List<Parameter> {
        new Parameter { Name = "@id", Value = data.SupplierId.Trim().ToUpper() }
      };

      if (type != NonQueryEnum.Delete)
      {
        prms.Add(new Parameter { Name = "@name", Value = data.SupplierName.Trim().ToUpper() });
        prms.Add(new Parameter { Name = "@alt_name", Value = data.SupplierAltName?.Trim().ToUpper() ?? "" });

        q = "sp_Supplier_Save";
      }
      
      return execNonQuery(q, prms);
    }

    private static Supplier SetItem(DataRow dr)
    {
      return new Supplier {
        SupplierId = dr["supplier_id"].ToString(),
        SupplierName = dr["supplier_name"].ToString(),
        SupplierAltName = dr["supplier_alt_name"].ToString()
      };
    }
    public static List<Supplier> GetAllItems()
    {
      var result = new List<Supplier>();
      var filter = tFilter();

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder, null);
      if (dt != null && dt.Rows.Count > 0)
      {
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }
                          
      return result;
    }
    public static List<Supplier> GetItems(int row = 10, int page = 1, string searchValue = "")
    {
      List<Supplier> result = null;
      var filter = tFilter(searchValue);

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder +
        $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<Supplier>();
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    public static Supplier GetItem(string id)
    {
      List<Parameter> prms = new List<Parameter>
      {
        new Parameter { Name = "@id", Value = id }
      };

      DataTable dt = execQuery(tSQL + " WHERE deleted=0 AND supplier_id=@id", prms);
      if (dt != null && dt.Rows.Count > 0)
        return SetItem(dt.Rows[0]);

      return null;
    }

    public static int ItemsCount(string searchValue = "")
    {
      var filter = tFilter(searchValue);

      DataTable dt = execQuery("SELECT COUNT(*) FROM tblm_supplier " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static string Save(Supplier sup)
    {
      return DoCommand(NonQueryEnum.Insert, sup);      
    }
    public static string Delete(Supplier sup)
    {
      return DoCommand(NonQueryEnum.Delete, sup);
    }
  }
}

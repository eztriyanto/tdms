﻿using System;
using System.Data;
using System.Collections.Generic;

using TDMS.Portal.Models;
using static TDMS.Portal.Helper;
using static TDMS.Portal.DBHelper;

namespace TDMS.Portal.Contexts
{
  public class RegionContext
  {
    private static string tSQL = "SELECT region_id,region_name,region_number FROM tblm_region ";
    private static string tOrder = " ORDER BY region_number";

    private static Tuple<string, List<Parameter>> tFilter(string value = "")
    {
      string filter = " WHERE deleted=0";
      List<Parameter> prms = null;

      if (!string.IsNullOrEmpty(value))
      {
        filter += " AND (region_id LIKE CONCAT('%',@id,'%') OR region_name LIKE CONCAT('%',@name,'%')) ";
        prms = new List<Parameter>
        {
          new Parameter { Name = "@id", Value = value },
          new Parameter { Name = "@name", Value = value }
        };
      };

      return Tuple.Create(filter, prms);
    }

    private static string DoCommand(NonQueryEnum type, Region data)
    {
      string q = "sp_Region_Delete";
      List<Parameter> prms = new List<Parameter> {
        new Parameter { Name = "@id", Value = data.RegionId.Trim().ToUpper() }
      };

      if (type != NonQueryEnum.Delete)
      {
        prms.Add(new Parameter { Name = "@name", Value = data.RegionName.Trim().ToUpper() });
        prms.Add(new Parameter { Name = "@number", Value = data.RegionNumber });

        q = "sp_Region_Save";
      }
      
      return execNonQuery(q, prms);
    }

    private static Region SetItem(DataRow dr)
    {
      return new Region {
        RegionId = dr["region_id"].ToString(),
        RegionName = dr["region_name"].ToString(),
        RegionNumber = (int)dr["region_number"]
      };
    }
    public static List<Region> GetAllItems()
    {
      var result = new List<Region>();
      var filter = tFilter();

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder, null);
      if (dt != null && dt.Rows.Count > 0)
      {
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }
                          
      return result;
    }
    public static List<Region> GetItems(int row = 10, int page = 1, string searchValue = "")
    {
      List<Region> result = null;
      var filter = tFilter(searchValue);

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder +
        $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<Region>();
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    public static Region GetItem(string id)
    {
      List<Parameter> prms = new List<Parameter>
      {
        new Parameter { Name = "@id", Value = id }
      };

      DataTable dt = execQuery(tSQL + " WHERE deleted=0 AND region_id=@id", prms);
      if (dt != null && dt.Rows.Count > 0)
        return SetItem(dt.Rows[0]);

      return null;
    }

    public static int ItemsCount(string searchValue = "")
    {
      var filter = tFilter(searchValue);

      DataTable dt = execQuery("SELECT COUNT(*) FROM tblm_region " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static string Save(Region reg)
    {
      return DoCommand(NonQueryEnum.Insert, reg);      
    }
    public static string Delete(Region reg)
    {
      return DoCommand(NonQueryEnum.Delete, reg);
    }
  }
}

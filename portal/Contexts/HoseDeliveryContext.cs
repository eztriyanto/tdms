﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using TDMS.Portal.Models;
using static TDMS.Portal.Helper;
using static TDMS.Portal.DBHelper;

namespace TDMS.Portal.Contexts
{
  public class HoseDeliveryContext
  {
    private static string tSQL = "SELECT a.site_id,a.site_name,a.site_address" +
      ",a.delivery_date,a.oil_company_code,a.grade_name" +
      ",ISNULL(b.material_number,'') AS material_number,total_delivery,total_prepurchase" + 
      ",a.created_date,a.update_date FROM tblt_hose_delivery a " +
      "LEFT JOIN tblm_grade b ON a.oil_company_code = b.oil_company_code";
    private static string tOrder = " ORDER BY a.site_id,a.delivery_date,a.oil_company_code";

    private static void searchFilter(string gradeId, string search, ref string f, ref List<Parameter> p)
    {
      if (!string.IsNullOrEmpty(gradeId))
      {
        //BIO SOLAR
        if (gradeId == "12")
        {
          f += " AND a.oil_company_code IN ('12','13') ";
        }
        else
        {
          f += " AND (a.oil_company_code=@oic) ";
          p = new List<Parameter> { new Parameter { Name = "@oic", Value = gradeId } };
        }
      }

      if (!string.IsNullOrEmpty(search))
      {
        if (p == null)
          p = new List<Parameter>();

        f += " AND (a.site_id LIKE CONCAT('%',@id,'%') OR a.grade_name LIKE CONCAT('%',@name,'%')) ";
        p.Add(new Parameter { Name = "@id", Value = search });
        p.Add(new Parameter { Name = "@name", Value = search });
      };
    }
    static Tuple<string, List<Parameter>> tFilter(string gradeId, DateTime startDT, DateTime endDT, string value = "")
    {
      List<Parameter> prms = null;
      string filter = " WHERE CAST(delivery_date AS DATE) " +
        $"BETWEEN '{startDT.ToString(SQLDate)}' AND '{endDT.ToString(SQLDate)}' ";

      searchFilter(gradeId, value, ref filter, ref prms);

      return Tuple.Create(filter, prms);
    }

    private static HoseDelivery SetItem(DataRow dr)
    {
      return new HoseDelivery {
        SiteId = dr["site_id"].ToString().Trim(),
        SiteName = dr["site_name"].ToString().Trim(),
        SiteAddress = dr["site_address"].ToString().Trim(),
        DeliveryDate = (DateTime)dr["delivery_date"],
        OilCompanyCode = dr["oil_company_code"].ToString().Trim(),
        GradeName = dr["grade_name"].ToString().Trim(),
        MaterialNumber = dr["material_number"].ToString().Trim(),
        TotalDelivery = (int)dr["total_delivery"],
        TotalPrePurchaseDelivery = (int)dr["total_prepurchase"],
        CreatedDate = (DateTime)dr["created_date"],
        UpdateDate = DBNull.Value.Equals(dr["update_date"]) ? DateTime.MinValue : (DateTime)dr["update_date"]
      };
    }
    public static List<HoseDelivery> GetItems(string gradeId, DateTime startDT, DateTime endDT, int row = 10, int page = 1, string searchValue = "", string orderBy = "")
    {
      List<HoseDelivery> result = null;
      var filter = tFilter(gradeId, startDT, endDT, searchValue);
      var order = tOrder;
      if (!string.IsNullOrEmpty(orderBy))
        order = " ORDER BY " + orderBy;

      DataTable dt = execQuery(tSQL + filter.Item1 + order +
        $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<HoseDelivery>();
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    
    public static int ItemsCount(string gradeId, DateTime startDT, DateTime endDT, string searchValue = "")
    {
      var filter = tFilter(gradeId, startDT, endDT, searchValue);

      DataTable dt = execQuery("SELECT COUNT(*) FROM tblt_hose_delivery a " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static int SiteCount(string gradeId, DateTime startDT, DateTime endDT, string searchValue = "")
    {
      var filter = tFilter(gradeId, startDT, endDT, searchValue);

      DataTable dt = execQuery("SELECT COUNT(DISTINCT site_id) FROM tblt_hose_delivery a " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static List<HoseDeliverySummary> GetSummaryItems(DateTime startDT, DateTime endDT)
    {
      var result = new List<HoseDeliverySummary>();
      var prms = new List<Parameter>
      {
        new Parameter { Name="@start", Value=startDT.Date },
        new Parameter { Name="@end", Value=endDT.Date }
      };
      DataTable dt = execQueryFromSP("sp_Get_Hose_Delivery_Summary", prms);

      for (int i = 1; i <= 8; i++)
      {
        var del = new HoseDeliverySummary { MOR = "MOR " + i };
        var rows = dt?.Select("mor='MOR " + i + "'");
        if (rows.Length > 0)
        {
          del.TotalSite = (int)rows[0]["total_site"];
          del.TotalSite12 = (int)rows[0]["total_site_12"];
          del.TotalPrePurchase12 = (int)rows[0]["total_prepurchase_12"];
          del.TotalDelivery12 = (int)rows[0]["total_delivery_12"];
          del.TotalSite11 = (int)rows[0]["total_site_11"];
          del.TotalPrePurchase11 = (int)rows[0]["total_prepurchase_11"];
          del.TotalDelivery11 = (int)rows[0]["total_delivery_11"];
        }
          
        result.Add(del);
      }

      result.Add(new HoseDeliverySummary
      {
        MOR = "NASIONAL",
        TotalSite = result.Sum(x => x.TotalSite),
        TotalSite12 = result.Sum(x => x.TotalSite12),
        TotalPrePurchase12 = result.Sum(x => x.TotalPrePurchase12),
        TotalDelivery12 = result.Sum(x => x.TotalDelivery12),
        TotalSite11 = result.Sum(x => x.TotalSite11),
        TotalPrePurchase11 = result.Sum(x => x.TotalPrePurchase11),
        TotalDelivery11 = result.Sum(x => x.TotalDelivery11)
      });

      return result;
    }

    public static List<HoseDeliverySummaryClassification> GetSummaryClassificationItems(DateTime startDT, DateTime endDT)
    {
      var result = new List<HoseDeliverySummaryClassification> {
        new HoseDeliverySummaryClassification { Display = "100%" },
        new HoseDeliverySummaryClassification { Display = "90% - 99%" },
        new HoseDeliverySummaryClassification { Display = "80% - 89%" },
        new HoseDeliverySummaryClassification { Display = "70% - 79%" },
        new HoseDeliverySummaryClassification { Display = "60% - 69%" },
        new HoseDeliverySummaryClassification { Display = "50% - 59%" },
        new HoseDeliverySummaryClassification { Display = "40% - 49%" },
        new HoseDeliverySummaryClassification { Display = "30% - 39%" },
        new HoseDeliverySummaryClassification { Display = "20% - 29%" },
        new HoseDeliverySummaryClassification { Display = "10% - 19%" },
        new HoseDeliverySummaryClassification { Display = "0% - 9%" },
        new HoseDeliverySummaryClassification { Display = "0" }
      };
      var prms = new List<Parameter>
      {
        new Parameter { Name="@start", Value=startDT.Date },
        new Parameter { Name="@end", Value=endDT.Date }
      };
      DataTable dt = execQueryFromSP("sp_Get_Hose_Delivery_Summary_Classification", prms);
      var fields = new string[] { "perf_100", "perf_90", "perf_80", "perf_70", "perf_60", 
        "perf_50", "perf_40", "perf_30", "perf_20", "perf_10", "perf_0", "perf_none" };

      for (int i = 1; i <= 8; i++)
      {
        var rows = dt?.Select("mor='MOR " + i + "'");
        if (rows.Length > 0)
        {
          for (int j = 0; j < fields.Length; j++)
          {
            var prop = result[j].GetType().GetProperty("Mor_" + i + "_12");
            prop.SetValue(result[j], (int)rows[0][fields[j] + "_12"], null);

            prop = result[j].GetType().GetProperty("Mor_" + i + "_11");
            prop.SetValue(result[j], (int)rows[0][fields[j] + "_11"], null);
          }
        }
      }

      var del = new HoseDeliverySummaryClassification { Display = "TOTAL" };
      del.Mor_1_12 = result.Sum(x => x.Mor_1_12);
      del.Mor_1_11 = result.Sum(x => x.Mor_1_11);
      del.Mor_2_12 = result.Sum(x => x.Mor_2_12);
      del.Mor_2_11 = result.Sum(x => x.Mor_2_11);
      del.Mor_3_12 = result.Sum(x => x.Mor_3_12);
      del.Mor_3_11 = result.Sum(x => x.Mor_3_11);
      del.Mor_4_12 = result.Sum(x => x.Mor_4_12);
      del.Mor_4_11 = result.Sum(x => x.Mor_4_11);
      del.Mor_5_12 = result.Sum(x => x.Mor_5_12);
      del.Mor_5_11 = result.Sum(x => x.Mor_5_11);
      del.Mor_6_12 = result.Sum(x => x.Mor_6_12);
      del.Mor_6_11 = result.Sum(x => x.Mor_6_11);
      del.Mor_7_12 = result.Sum(x => x.Mor_7_12);
      del.Mor_7_11 = result.Sum(x => x.Mor_7_11);
      del.Mor_8_12 = result.Sum(x => x.Mor_8_12);
      del.Mor_8_11 = result.Sum(x => x.Mor_8_11);
      result.Add(del);

      return result;
    }

    public static string Save(List<HoseDelivery> items)
    {
      var q = Enumerable.Repeat("sp_Hose_Delivery_Save", items.Count).ToList();

      var prms = new List<Parameter>[items.Count];
      for (int i = 0; i < items.Count; i++)
        prms[i] = new List<Parameter> {
          new Parameter { Name = "@Site_ID", Value = items[i].SiteId.Trim().Replace(".","").Replace("-","") },
          new Parameter { Name = "@Site_Name", Value = items[i].SiteName.Trim() },
          new Parameter { Name = "@Site_Address", Value = items[i].SiteAddress.Trim() },
          new Parameter { Name = "@Delivery_Date", Value = items[i].DeliveryDate },
          new Parameter { Name = "@Oil_Company_Code", Value = items[i].OilCompanyCode.Trim() },
          new Parameter { Name = "@Grade_Name", Value = items[i].GradeName.Trim() },
          new Parameter { Name = "@Total_Delivery", Value = items[i].TotalDelivery },
          new Parameter { Name = "@Total_PrePurchase", Value = items[i].TotalPrePurchaseDelivery }
        };

      return execNonQueries(q, prms);
    }
  }
}

﻿using System;
using System.Data;
using System.Collections.Generic;
using TDMS.Portal.Models;
using static TDMS.Portal.Helper;
using static TDMS.Portal.DBHelper;

namespace TDMS.Portal.Contexts
{
  public class SupplyPointContext
  {
    private static string tSQL = "SELECT supply_point_id,supply_point_name,supply_point_alt_name,region_id,region_name " + 
      "FROM vw_supply_point";
    private static string tOrder = " ORDER BY region_name,supply_point_name";

    private static Tuple<string, List<Parameter>> tFilter(string value = "")
    {
      string filter = " WHERE deleted=0";
      List<Parameter> prms = null;

      if (!string.IsNullOrEmpty(value))
      {
        filter += " AND (supply_point_id LIKE CONCAT('%',@id,'%') " + 
          "OR supply_point_name LIKE CONCAT('%',@name,'%') " +
          "OR region_id LIKE CONCAT('%',@region_id,'%') " +
          "OR region_name LIKE CONCAT('%',@region_name,'%')) ";
        prms = new List<Parameter>
        {
          new Parameter { Name = "@id", Value = value },
          new Parameter { Name = "@name", Value = value },
          new Parameter { Name = "@region_id", Value = value },
          new Parameter { Name = "@region_name", Value = value }
        };
      };

      return Tuple.Create(filter, prms);
    }

    private static string DoCommand(NonQueryEnum type, SupplyPoint data)
    {
      string q = "sp_Supply_Point_Delete";
      List<Parameter> prms = new List<Parameter> {
        new Parameter { Name = "@id", Value = data.SupplyPointId.Trim().ToUpper() }
      };

      if (type != NonQueryEnum.Delete)
      {
        prms.Add(new Parameter { Name = "@name", Value = data.SupplyPointName.Trim().ToUpper() });
        prms.Add(new Parameter { Name = "@alt_name", Value = data.SupplyPointAltName?.Trim().ToUpper() ?? "" });
        prms.Add(new Parameter { Name = "@region", Value = data.SupplyPointRegion.RegionId });

        q = "sp_Supply_Point_Save";
      }
      
      return execNonQuery(q, prms);
    }

    private static SupplyPoint SetItem(DataRow dr)
    {
      return new SupplyPoint {
        SupplyPointId = dr["supply_point_id"].ToString(),
        SupplyPointName = dr["supply_point_name"].ToString(),
        SupplyPointAltName = dr["supply_point_alt_name"].ToString(),
        SupplyPointRegion = new Region {
          RegionId = dr["region_id"].ToString(),
          RegionName = dr["region_name"].ToString(),
        }
      };
    }
    public static List<SupplyPoint> GetAllItems()
    {
      var result = new List<SupplyPoint>();
      var filter = tFilter();

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder, null);
      if (dt != null && dt.Rows.Count > 0)
      {
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }
                          
      return result;
    }
    public static List<SupplyPoint> GetAllItems(string RegionId)
    {
      var result = new List<SupplyPoint>();
      var filter = tFilter();
      var prms = new List<Parameter>
      {
        new Parameter { Name = "@region", Value = RegionId }
      };

      DataTable dt = execQuery(tSQL + filter.Item1 + " AND region_id=@region " + tOrder, prms);
      if (dt != null && dt.Rows.Count > 0)
      {
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    public static List<SupplyPoint> GetItems(int row = 10, int page = 1, string searchValue = "")
    {
      List<SupplyPoint> result = null;
      var filter = tFilter(searchValue);

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder +
        $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<SupplyPoint>();
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    public static SupplyPoint GetItem(string id)
    {
      List<Parameter> prms = new List<Parameter>
      {
        new Parameter { Name = "@id", Value = id }
      };

      DataTable dt = execQuery(tSQL + " WHERE deleted=0 AND supply_point_id=@id", prms);
      if (dt != null && dt.Rows.Count > 0)
        return SetItem(dt.Rows[0]);

      return null;
    }

    public static int ItemsCount(string searchValue = "")
    {
      var filter = tFilter(searchValue);

      DataTable dt = execQuery("SELECT COUNT(*) FROM vw_supply_point " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static string Save(SupplyPoint sp)
    {
      return DoCommand(NonQueryEnum.Insert, sp);      
    }
    public static string Delete(SupplyPoint sp)
    {
      return DoCommand(NonQueryEnum.Delete, sp);
    }

    public static string GetIdByName(string name)
    {
      if (!string.IsNullOrEmpty(name))
      {
        name = name.Trim().ToUpper();

        var items = GetAllItems();
        if (items != null || items.Count > 0)
          foreach (var item in items)
          {
            if (item.SupplyPointName == name)
              return item.SupplyPointId;

            if (item.SupplyPointAltNameList != null || item.SupplyPointAltNameList.Count > 0)
              foreach (var altName in item.SupplyPointAltNameList)
                if (altName == name)
                  return item.SupplyPointId;
          }
      }

      return "";
    }
  }
}

﻿using System;
using System.Data;
using System.Collections.Generic;
using TDMS.Portal.Models;
using static TDMS.Portal.Helper;
using static TDMS.Portal.DBHelper;

namespace TDMS.Portal.Contexts
{
  public class UserContext
  {
    private static string tSQL = "SELECT user_id,user_name,user_password,user_role,active,user_access_1,user_access_2 " + 
      "FROM tblm_user ";
    private static string tOrder = " ORDER BY user_name";

    private static Tuple<string, List<Parameter>> tFilter(string value = "")
    {
      string filter = " WHERE deleted=0";
      List<Parameter> prms = null;

      if (!string.IsNullOrEmpty(value))
      {
        filter += " AND (user_id LIKE CONCAT('%',@id,'%') OR user_name LIKE CONCAT('%',@name,'%')) ";
        prms = new List<Parameter>
        {
          new Parameter { Name = "@id", Value = value },
          new Parameter { Name = "@name", Value = value }
        };
      };

      return Tuple.Create(filter, prms);
    }

    private static string DoCommand(NonQueryEnum type, User data)
    {
      string q = "sp_User_Delete";
      List<Parameter> prms = new List<Parameter> {
        new Parameter { Name = "@id", Value = data.UserId }
      };

      if (type != NonQueryEnum.Delete)
      {
        var Access = data.AccessId ?? "";
        if (data.Role == User.RoleEnum.Super_User || data.Role == User.RoleEnum.Administrator)
          Access = "";

        prms.Add(new Parameter { Name = "@name", Value = data.UserName });
        prms.Add(new Parameter { Name = "@password", Value = Encrypt(data.Password) });
        prms.Add(new Parameter { Name = "@role", Value = data.Role });
        prms.Add(new Parameter { Name = "@access_1", Value = Access });
        prms.Add(new Parameter { Name = "@access_2", Value = "" });
        prms.Add(new Parameter { Name = "@active", Value = data.IsActive });

        q = "sp_User_Save";
      }
      
      return execNonQuery(q, prms);
    }

    private static User SetItem(DataRow dr)
    {
      int.TryParse(dr["user_role"].ToString(), out int role);
      return new User {
        UserId = dr["user_id"].ToString(),
        UserName = dr["user_name"].ToString(),
        Password = Decrypt(dr["user_password"].ToString()),
        Role = (User.RoleEnum)role,
        AccessId = dr["user_access_1"].ToString(),
        AccessName = dr["user_access_2"].ToString(),
        IsActive = (bool)dr["active"]
      };
    }
    public static List<User> GetAllItems()
    {
      List<User> result = null;

      DataTable dt = execQuery(tSQL + tOrder, null);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<User>();
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }
                          
      return result;
    }
    public static List<User> GetItems(int row = 10, int page = 1, string searchValue = "")
    {
      var result = new List<User>();
      var filter = tFilter(searchValue);

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder +
        $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    public static User GetItem(string id)
    {
      List<Parameter> prms = new List<Parameter>
      {
        new Parameter { Name = "@id", Value = id }
      };

      DataTable dt = execQuery(tSQL + " WHERE deleted=0 AND user_id=@id", prms);
      if (dt != null && dt.Rows.Count > 0)
        return SetItem(dt.Rows[0]);

      return null;
    }

    public static int ItemsCount(string searchValue = "")
    {
      var filter = tFilter(searchValue);

      DataTable dt = execQuery("SELECT COUNT(*) FROM tblm_user " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static string Save(User user)
    {
      return DoCommand(NonQueryEnum.Insert, user);      
    }
    public static string Delete(User user)
    {
      return DoCommand(NonQueryEnum.Delete, user);
    }
  }
}

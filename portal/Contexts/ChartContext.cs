﻿using System;
using System.Data;
using System.Collections.Generic;

using TDMS.Portal.Models;
using static TDMS.Portal.Helper;
using static TDMS.Portal.DBHelper;

namespace TDMS.Portal.Contexts
{
  public class ChartContext
  {
    public static List<Chart.dataSet> GetTankDeliveryWeeklyData(DateTime startDate)
    {
      List<Chart.dataSet> result = new List<Chart.dataSet>();

      DataTable dt = execQuery("SELECT del_date,del_volume " + 
        $"FROM dbo.fc_Weekly_Sum('{startDate.ToString(SQLDate)}')", null);

      for (int i = -6; i <= 0; i++)
      {
        var runningDate = startDate.AddDays(i);
        var ds = new Chart.dataSet {
          label = runningDate.ToString("dd MMM yy")
        };

        if (dt != null && dt.Rows.Count > 0)
        {
          DataRow[] rows = dt.Select($"del_date='{runningDate.ToString(SQLDate)}'");
          if (rows != null && rows.Length > 0)
            ds.value = Convert.ToDouble(rows[0]["del_volume"]);
        }

        result.Add(ds);
      }
      
      return result;
    }
  }
}

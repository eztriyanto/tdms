﻿using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using TDMS.Portal.Models;
using static TDMS.Portal.Helper;
using static TDMS.Portal.DBHelper;

namespace TDMS.Portal.Contexts
{
  public class TankDeliveryContext
  {
    private static string tSQL = "SELECT a.id,tank_delivery_id,site_id,site_name,site_address" +
      ",tank_id,tank_name,grade_id,a.grade_name,a.oil_company_code,ISNULL(material_number,'') material_number,period_id,period_number" +
      ",drop_date_time,record_entry_ts,drop_volume,delivery_note_num,drop_volume_theo,unit_cost_price" +
      ",driver_id_code,tanker_id_code,delivery_detail,dispatched_volume,original_invoice_number" +
      ",received_vol_at_ref_temp,dispatched_vol_at_ref_temp,total_variance,variance_at_ref_temp" +
      ",temperature_variance,tank_movement_type_id,user_id,user_name,a.created_date,a.update_date" +
      ",dbo.fn_get_sync_date(a.id) AS sync_date " +
      "FROM tblt_tank_delivery a LEFT JOIN tblm_grade b ON a.oil_company_code = b.oil_company_code";
    private static string tOrder = " ORDER BY site_id,drop_date_time,a.oil_company_code";

    private static void searchFilter(string supplierId, string search, ref string f, ref List<Parameter> p)
    {
      if (!string.IsNullOrEmpty(supplierId))
      {
        var sup = SupplierContext.GetItem(supplierId);
        if (sup != null)
        {
          if (p == null) 
            p = new List<Parameter>();

          f += " AND (UPPER(delivery_detail)=UPPER(@supplier) ";
          p.Add(new Parameter { Name = "@supplier", Value = sup.SupplierName });
          for (int i = 0; i < sup.SupplierAltNameList.Count; i++)
          {
            f += $" OR UPPER(delivery_detail)=UPPER(@supplier{i}) ";
            p.Add(new Parameter { Name = $"@supplier{i}", Value = sup.SupplierAltNameList[i] });
          }

          f += ") ";
        }
        
      }

      if (!string.IsNullOrEmpty(search))
      {
        if (p == null)
          p = new List<Parameter>();

        f += " AND (site_id LIKE CONCAT('%',@id,'%') OR a.grade_name LIKE CONCAT('%',@name,'%') " +
          "OR (original_invoice_number LIKE CONCAT('%',@id,'%'))) ";
        p.Add(new Parameter { Name = "@id", Value = search });
        p.Add(new Parameter { Name = "@name", Value = search });
      };
    }
    static Tuple<string, List<Parameter>> tFilter(string regionId, string supplyPointId, string supplierId, DateTime startDT, DateTime endDT, string value = "")
    {
      List<Parameter> prms = null;
      string filter = " WHERE a.deleted=0 AND CAST(drop_date_time AS DATE) " +
        $"BETWEEN '{startDT.ToString(SQLDate)}' AND '{endDT.ToString(SQLDate)}' ";

      if (!string.IsNullOrEmpty(supplyPointId))
      {
        //Filter by Depot
        var sp = SupplyPointContext.GetItem(supplyPointId);
        if (sp != null)
        {
          filter += "AND (UPPER(delivery_note_num)=UPPER(@supplyPoint) ";
          prms = new List<Parameter>
          {
            new Parameter { Name = "@supplyPoint", Value = sp.SupplyPointName }
          };
          for (int i = 0; i < sp.SupplyPointAltNameList.Count; i++)
          {
            var itm = sp.SupplyPointAltNameList[i];
            filter += $" OR UPPER(delivery_note_num)=UPPER(@supplyPoint{i}) ";
            prms.Add(new Parameter { Name = $"@supplyPoint{i}", Value = itm });
          }

          filter += ")";
        }
      }

      //Filter by Region
      else if (!string.IsNullOrEmpty(regionId))
      {
        prms = new List<Parameter>
          {
            new Parameter { Name = "@region", Value = regionId }
          };
        filter += " AND LEFT(site_id,1) IN (SELECT region_number FROM tblM_Region WHERE Region_ID=@region)";
      }

      searchFilter(supplierId, value, ref filter, ref prms);

      return Tuple.Create(filter, prms);
    }

    private static void searchFilterSiteOnly(string supplierId, string search, ref string f, ref List<Parameter> p)
    {
      if (!string.IsNullOrEmpty(supplierId))
      {
        var sup = SupplierContext.GetItem(supplierId);
        if (sup != null)
        {
          if (p == null)
            p = new List<Parameter>();

          f += " AND (UPPER(delivery_detail)=UPPER(@supplier) ";
          p.Add(new Parameter { Name = "@supplier", Value = sup.SupplierName });
          for (int i = 0; i < sup.SupplierAltNameList.Count; i++)
          {
            f += $" OR UPPER(delivery_detail)=UPPER(@supplier{i}) ";
            p.Add(new Parameter { Name = $"@supplier{i}", Value = sup.SupplierAltNameList[i] });
          }

          f += ") ";
        }

      }

      if (!string.IsNullOrEmpty(search))
      {
        if (p == null)
          p = new List<Parameter>();

        f += " AND (site_id LIKE CONCAT('%',@id,'%')) ";
        p.Add(new Parameter { Name = "@id", Value = search });
      };
    }
    static Tuple<string, List<Parameter>> tFilterSiteOnly(string regionId, string supplyPointId, string supplierId, DateTime startDT, DateTime endDT, string value = "")
    {
      List<Parameter> prms = null;
      string filter = " WHERE a.deleted=0 AND CAST(drop_date_time AS DATE) " +
        $"BETWEEN '{startDT.ToString(SQLDate)}' AND '{endDT.ToString(SQLDate)}' ";

      if (!string.IsNullOrEmpty(supplyPointId))
      {
        //Filter by Depot
        var sp = SupplyPointContext.GetItem(supplyPointId);
        if (sp != null)
        {
          filter += "AND (UPPER(delivery_note_num)=UPPER(@supplyPoint) ";
          prms = new List<Parameter>
          {
            new Parameter { Name = "@supplyPoint", Value = sp.SupplyPointName }
          };
          for (int i = 0; i < sp.SupplyPointAltNameList.Count; i++)
          {
            var itm = sp.SupplyPointAltNameList[i];
            filter += $" OR UPPER(delivery_note_num)=UPPER(@supplyPoint{i}) ";
            prms.Add(new Parameter { Name = $"@supplyPoint{i}", Value = itm });
          }

          filter += ")";
        }
      }

      //Filter by Region
      else if (!string.IsNullOrEmpty(regionId))
      {
        prms = new List<Parameter>
          {
            new Parameter { Name = "@region", Value = regionId }
          };
        filter += " AND LEFT(site_id,1) IN (SELECT region_number FROM tblM_Region WHERE Region_ID=@region)";
      }

      searchFilterSiteOnly(supplierId, value, ref filter, ref prms);

      return Tuple.Create(filter, prms);
    }

    private static TankDelivery SetItem(DataRow dr)
    {
      return new TankDelivery {
        Index = Convert.ToInt64(dr["id"]),
        TankDeliveryId = (int)dr["tank_delivery_id"],
        SiteId = dr["site_id"].ToString().Trim(),
        SiteName = dr["site_name"].ToString().Trim(),
        SiteAddress = dr["site_address"].ToString().Trim(),
        TankId = (int)dr["tank_id"],
        TankName = dr["tank_name"].ToString().Trim(),
        GradeId = (int)dr["grade_id"],
        GradeName = dr["grade_name"].ToString().Trim(),
        OilCompanyCode = dr["oil_company_code"].ToString().Trim(),
        MaterialNumber = dr["material_number"].ToString().Trim(),
        PeriodId = (int)dr["period_id"],
        PeriodNumber = (int)dr["period_number"],
        DropDateTime = (DateTime)dr["drop_date_time"],
        RecordEntryTS = (DateTime)dr["record_entry_ts"],
        DropVolume = Convert.ToDouble(dr["drop_volume"]),
        DeliveryNoteNum = dr["delivery_note_num"].ToString().Trim(),
        DropVolumeTheo = Convert.ToDouble(dr["drop_volume_theo"]),
        UnitCostPrice = Convert.ToDouble(dr["unit_cost_price"]),
        DriverIdCode = dr["driver_id_code"].ToString().Trim(),
        TankerIdCode = dr["tanker_id_code"].ToString().Trim(),
        DeliveryDetail = dr["delivery_detail"].ToString().Trim(),
        DispatchedVolume = Convert.ToDouble(dr["dispatched_volume"]),
        OriginalInvoiceNumber = dr["original_invoice_number"].ToString().Trim(),
        ReceivedVolAtRefTemp = Convert.ToDouble(dr["received_vol_at_ref_temp"]),
        DispatchedVolAtRefTemp = Convert.ToDouble(dr["dispatched_vol_at_ref_temp"]),
        TotalVariance = Convert.ToDouble(dr["total_variance"]),
        VarianceAtRefTemp = Convert.ToDouble(dr["variance_at_ref_temp"]),
        TemperatureVariance = Convert.ToDouble(dr["temperature_variance"]),
        TankMovementTypeId = (int)dr["tank_movement_type_id"],
        UserId = (int)dr["user_id"],
        UserName = dr["user_name"].ToString().Trim(),
        CreatedDate = (DateTime)dr["created_date"],
        UpdateDate = DBNull.Value.Equals(dr["update_date"]) ? DateTime.MinValue : (DateTime)dr["update_date"],
        OdiSyncDate = DBNull.Value.Equals(dr["sync_date"]) ? DateTime.MinValue : (DateTime)dr["sync_date"],
      };
    }
    public static List<TankDelivery> GetItems(string regionId, string supplyPointId, string supplierId, DateTime startDT, DateTime endDT, int row = 10, int page = 1, string searchValue = "", string orderBy = "")
    {
      List<TankDelivery> result = null;
      var filter = tFilter(regionId, supplyPointId, supplierId, startDT, endDT, searchValue);
      var order = tOrder; 
      if (!string.IsNullOrEmpty(orderBy))
        order = " ORDER BY " + orderBy;
      
      DataTable dt = execQuery(tSQL + filter.Item1 + order +
        $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<TankDelivery>();
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    public static TankDelivery GetItem(int id)
    {
      List<Parameter> prms = new List<Parameter>
      {
        new Parameter { Name = "@id", Value = id }
      };

      DataTable dt = execQuery(tSQL + " WHERE a.deleted=0 AND id=@id", prms);
      if (dt != null && dt.Rows.Count > 0)
        return SetItem(dt.Rows[0]);

      return null;
    }
    public static TankDelivery GetItem(string DONumber)
    {
      List<Parameter> prms = new List<Parameter>
      {
        new Parameter { Name = "@number", Value = DONumber }
      };

      DataTable dt = execQuery(tSQL + " WHERE a.deleted=0 AND original_invoice_number=@number", prms);
      if (dt != null && dt.Rows.Count > 0)
        return SetItem(dt.Rows[0]);

      return null;
    }

    public static int ItemsCount(string regionId, string supplyPointId, string supplierId, DateTime startDT, DateTime endDT, string searchValue = "")
    {
      var filter = tFilter(regionId, supplyPointId, supplierId, startDT, endDT, searchValue);

      DataTable dt = execQuery("SELECT COUNT(*) FROM tblt_tank_delivery a " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static int SiteCount(string regionId, string supplyPointId, string supplierId, DateTime startDT, DateTime endDT, string searchValue = "")
    {
      var filter = tFilter(regionId, supplyPointId, supplierId, startDT, endDT, searchValue);

      DataTable dt = execQuery("SELECT COUNT(DISTINCT site_id) FROM tblt_tank_delivery a " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static string Save(List<TankDelivery> items)
    {
      var q = Enumerable.Repeat("sp_Tank_Delivery_Save", items.Count).ToList();

      var prms = new List<Parameter>[items.Count];
      for(int i = 0; i < items.Count; i++)
        prms[i] = new List<Parameter> {
          new Parameter { Name = "@Tank_Delivery_ID", Value = items[i].TankDeliveryId },
          new Parameter { Name = "@Site_ID", Value = items[i].SiteId.Trim().Replace(".","").Replace("-","") },
          new Parameter { Name = "@Site_Name", Value = items[i].SiteName.Trim() },
          new Parameter { Name = "@Site_Address", Value = items[i].SiteAddress.Trim() },
          new Parameter { Name = "@Tank_ID", Value = items[i].TankId },
          new Parameter { Name = "@Tank_Name", Value = items[i].TankName.Trim() },
          new Parameter { Name = "@Grade_ID", Value = items[i].GradeId },
          new Parameter { Name = "@Grade_Name", Value = items[i].GradeName.Trim() },
          new Parameter { Name = "@Oil_Company_Code", Value = items[i].OilCompanyCode.Trim() },
          new Parameter { Name = "@Period_ID", Value = items[i].PeriodId },
          new Parameter { Name = "@Period_Number", Value = items[i].PeriodNumber },
          new Parameter { Name = "@Drop_Date_Time", Value = items[i].DropDateTime },
          new Parameter { Name = "@Record_Entry_TS", Value = items[i].RecordEntryTS },
          new Parameter { Name = "@Drop_Volume", Value = items[i].DropVolume },
          new Parameter { Name = "@Delivery_Note_Num", Value = items[i].DeliveryNoteNum.Trim() },
          new Parameter { Name = "@Drop_Volume_Theo", Value = items[i].DropVolumeTheo },
          new Parameter { Name = "@Unit_Cost_Price", Value = items[i].UnitCostPrice },
          new Parameter { Name = "@Driver_ID_Code", Value = items[i].DriverIdCode.Trim() },
          new Parameter { Name = "@Tanker_ID_Code", Value = items[i].TankerIdCode.Trim() },
          new Parameter { Name = "@Delivery_Detail", Value = items[i].DeliveryDetail.Trim() },
          new Parameter { Name = "@Dispatched_Volume", Value = items[i].DispatchedVolume },
          new Parameter { Name = "@Original_Invoice_Number", Value = items[i].OriginalInvoiceNumber.Trim() },
          new Parameter { Name = "@Received_Vol_At_Ref_Temp", Value = items[i].ReceivedVolAtRefTemp },
          new Parameter { Name = "@Dispatched_Vol_At_Ref_Temp", Value = items[i].DispatchedVolAtRefTemp },
          new Parameter { Name = "@Total_Variance", Value = items[i].TotalVariance },
          new Parameter { Name = "@Variance_At_Ref_Temp", Value = items[i].VarianceAtRefTemp },
          new Parameter { Name = "@Temperature_Variance", Value = items[i].TemperatureVariance },
          new Parameter { Name = "@Tank_Movement_Type_ID", Value = items[i].TankMovementTypeId },
          new Parameter { Name = "@User_ID", Value = items[i].UserId },
          new Parameter { Name = "@User_Name", Value = items[i].UserName.Trim() }
        };

      return execNonQueries(q, prms);
    }

    public static List<TankDelivery> GetUnsyncItem()
    {
      List<TankDelivery> result = null;
      DataTable dt = execQuery(tSQL + " LEFT JOIN tblR_Tank_Delivery_Sync c ON a.ID = c.Tank_Delivery_Index " +
        " WHERE a.deleted=0 AND c.sync_date IS NULL " +
        " ORDER BY drop_date_time,a.id OFFSET 0 ROWS FETCH NEXT 50 ROWS ONLY", null);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<TankDelivery>();
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    public static string UpdateSync(long index)
    {
      return execNonQuery("sp_Tank_Delivery_Sync_Save", new List<Parameter> { 
        new Parameter { Name = "@Tank_Delivery_Index", Value = index }
      });
    }

    public static List<TankDeliverySummary> GetSummaryItems(string ProductId, DateTime startDT, DateTime endDT)
    {
      var del = new TankDeliverySummary();
      var result = new List<TankDeliverySummary>();
      var prms = new List<Parameter>
      {
        new Parameter { Name="@oil_company_code", Value=ProductId ?? "" },
        new Parameter { Name="@start", Value=startDT.Date },
        new Parameter { Name="@end", Value=endDT.Date }
      };
      DataTable dt = execQueryFromSP("sp_Get_Tank_Delivery_Summary", prms);

      for (int i = 1; i <= 8; i++)
      {
        del = new TankDeliverySummary { MOR = "MOR " + i };
        var rows = dt?.Select("mor='MOR " + i + "'");
        if (rows.Length > 0)
        {
          del.TotalSite = Convert.ToInt32(rows[0]["total_site"]);
          del.TotalDelivery = Convert.ToInt32(rows[0]["total_delivery"]);
          del.Loss_2 = Convert.ToDouble(rows[0]["loss_2"]);
          del.Loss_1 = Convert.ToDouble(rows[0]["loss_1"]);
          del.Loss_03 = Convert.ToDouble(rows[0]["loss_03"]);
          del.Loss_015 = Convert.ToDouble(rows[0]["loss_015"]);
          del.Loss_0 = Convert.ToDouble(rows[0]["loss_0"]);
          del.Gain_0 = Convert.ToDouble(rows[0]["gain_0"]);
          del.Gain_015 = Convert.ToDouble(rows[0]["gain_015"]);
          del.Gain_03 = Convert.ToDouble(rows[0]["gain_03"]);
          del.Gain_1 = Convert.ToDouble(rows[0]["gain_1"]);
          del.Gain_2 = Convert.ToDouble(rows[0]["gain_2"]);
        }

        result.Add(del);
      }

      //Total All MOR
      del = new TankDeliverySummary {
        MOR = "NASIONAL",
        TotalSite = result.Sum(x => x.TotalSite),
        TotalDelivery = result.Sum(x => x.TotalDelivery),
        Loss_2 = result.Sum(x => x.Loss_2),
        Loss_1 = result.Sum(x => x.Loss_1),
        Loss_03 = result.Sum(x => x.Loss_03),
        Loss_015 = result.Sum(x => x.Loss_015),
        Loss_0 = result.Sum(x => x.Loss_0),
        Gain_0 = result.Sum(x => x.Gain_0),
        Gain_015 = result.Sum(x => x.Gain_015),
        Gain_03 = result.Sum(x => x.Gain_03),
        Gain_1 = result.Sum(x => x.Gain_1),
        Gain_2 = result.Sum(x => x.Gain_2)
      };
      result.Add(del);

      //Percentage
      var idx = result.Count - 1;
      del = new TankDeliverySummary { MOR = "% per category shipment" };
      if (result[idx].TotalDelivery != 0)
      {
        del.TotalSite = result[idx].TotalSite;
        del.TotalDelivery = result[idx].TotalDelivery;
        del.Loss_2 = result[idx].Loss_2 / result[idx].TotalDelivery * 100;
        del.Loss_1 = result[idx].Loss_1 / result[idx].TotalDelivery * 100;
        del.Loss_03 = result[idx].Loss_03 / result[idx].TotalDelivery * 100;
        del.Loss_015 = result[idx].Loss_015 / result[idx].TotalDelivery * 100;
        del.Loss_0 = result[idx].Loss_0 / result[idx].TotalDelivery * 100;
        del.Gain_0 = result[idx].Gain_0 / result[idx].TotalDelivery * 100;
        del.Gain_015 = result[idx].Gain_015 / result[idx].TotalDelivery * 100;
        del.Gain_03 = result[idx].Gain_03 / result[idx].TotalDelivery * 100;
        del.Gain_1 = result[idx].Gain_1 / result[idx].TotalDelivery * 100;
        del.Gain_2 = result[idx].Gain_2 / result[idx].TotalDelivery * 100;
      }
      result.Add(del);

      return result;
    }


    public static int ItemsCountForSummaryBySite(string productId, string regionId, string supplyPointId, string supplierId, DateTime startDT, DateTime endDT, string searchValue = "")
    {
      var tSQL = "SELECT COUNT(*) FROM (SELECT site_id, " +
        "SUM(drop_volume) AS total_volume, SUM(dispatched_volume) AS total_do_volume " +
        "FROM tblt_tank_delivery a ";

      var tSQL2 = "	GROUP BY site_id) x ";

      var objFilter = tFilterSiteOnly(regionId, supplyPointId, supplierId, startDT, endDT, searchValue);
      var filter = objFilter.Item1;
      var prms = objFilter.Item2 ?? new List<Parameter>();

      if (!string.IsNullOrEmpty(productId))
      {
        filter += " AND oil_company_code=@product ";
        prms.Add(new Parameter { Name = "@product", Value = productId });
      }

      DataTable dt = execQuery(tSQL + filter + tSQL2, prms);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }
    public static List<TankDeliverySummaryBySite> GetSummaryBySiteItems(string productId, string regionId, string supplyPointId, string supplierId, DateTime startDT, DateTime endDT, int row = 10, int page = 1, string searchValue = "", string orderBy = "")
    {
      List<TankDeliverySummaryBySite> result = null;
      var tSQL = "SELECT * FROM (SELECT site_id, " +
        "SUM(drop_volume) AS total_volume, SUM(dispatched_volume) AS total_do_volume " +
        "FROM tblt_tank_delivery a ";

      var tSQL2 = "	GROUP BY site_id) x ";

      var objFilter = tFilterSiteOnly(regionId, supplyPointId, supplierId, startDT, endDT, searchValue);
      var filter = objFilter.Item1;
      var prms = objFilter.Item2 ?? new List<Parameter>();

      if (!string.IsNullOrEmpty(productId))
      {
        filter += " AND oil_company_code=@product ";
        prms.Add(new Parameter { Name = "@product", Value = productId });
      }
        
      var order = "ORDER BY site_id";
      if (!string.IsNullOrEmpty(orderBy))
        order = " ORDER BY " + orderBy;

      DataTable dt = execQuery(tSQL + filter + tSQL2 + order +
        $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", prms);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<TankDeliverySummaryBySite>();
        foreach (DataRow item in dt.Rows)
        {
          result.Add(new TankDeliverySummaryBySite { 
            SiteId = item["site_id"].ToString().Trim(),
            TotalVolume = Convert.ToDouble(item["total_volume"]),
            TotalDOVolume = Convert.ToDouble(item["total_do_volume"])
          });
        }
      }

      return result;
    }

    public static int ItemsCountForListBySite(string searchValue = "")
    {
      List<Parameter> prms = null;
      var tSQL = "SELECT COUNT(*) FROM (SELECT site_id, MIN(created_date) AS inserted_date, " +
        "MAX(created_date) AS updated_date FROM tblt_tank_delivery WHERE deleted = 0 ";
      
      if (!string.IsNullOrEmpty(searchValue))
      {
        tSQL += " AND site_id LIKE CONCAT('%',@id,'%')";
        prms =  new List<Parameter> { new Parameter { Name = "@id", Value = searchValue } };
      }

      tSQL += " GROUP BY site_id) x";

      DataTable dt = execQuery(tSQL, prms);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }
    public static List<TankDelivery> GetListBySiteItems(int row = 10, int page = 1, string searchValue = "")
    {
      List<TankDelivery> result = null;
      List<Parameter> prms = null;
      var tSQL = "SELECT site_id, MIN(created_date) AS inserted_date, " +
        "MAX(created_date) AS updated_date FROM tblt_tank_delivery WHERE deleted = 0 ";

      if (!string.IsNullOrEmpty(searchValue))
      {
        tSQL += " AND site_id LIKE CONCAT('%',@id,'%')";
        prms = new List<Parameter> { new Parameter { Name = "@id", Value = searchValue } };
      }

      tSQL += " GROUP BY site_id ORDER BY site_id ";

      DataTable dt = execQuery(tSQL + $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", prms);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<TankDelivery>();
        foreach (DataRow item in dt.Rows)
        {
          result.Add(new TankDelivery
          {
            SiteId = item["site_id"].ToString().Trim(),
            CreatedDate = (DateTime)item["inserted_date"],
            UpdateDate = (DateTime)item["updated_date"]
          });
        }
      }

      return result;
    }
  }
}

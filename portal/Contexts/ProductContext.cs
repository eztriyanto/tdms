﻿using System;
using System.Data;
using System.Collections.Generic;

using TDMS.Portal.Models;
using static TDMS.Portal.Helper;
using static TDMS.Portal.DBHelper;

namespace TDMS.Portal.Contexts
{
  public class ProductContext
  {
    private static string tSQL = "SELECT oil_company_code,grade_name,material_number FROM tblm_grade ";
    private static string tOrder = " ORDER BY grade_name";

    private static Tuple<string, List<Parameter>> tFilter(string value = "")
    {
      string filter = " WHERE deleted=0";
      List<Parameter> prms = null;

      if (!string.IsNullOrEmpty(value))
      {
        filter += " AND (oil_company_code LIKE CONCAT('%',@id,'%') OR " +
          "grade_name LIKE CONCAT('%',@name,'%')) OR material_number LIKE CONCAT('%',@matnum,'%')) ";
        prms = new List<Parameter>
        {
          new Parameter { Name = "@id", Value = value },
          new Parameter { Name = "@name", Value = value },
          new Parameter { Name = "@matnum", Value = value }
        };
      };

      return Tuple.Create(filter, prms);
    }

    private static string DoCommand(NonQueryEnum type, Product data)
    {
      string q = "sp_Grade_Delete";
      List<Parameter> prms = new List<Parameter> {
        new Parameter { Name = "@oil_company_code", Value = data.ProductId.Trim().ToUpper() }
      };

      if (type != NonQueryEnum.Delete)
      {
        prms.Add(new Parameter { Name = "@name", Value = data.ProductName.Trim().ToUpper() });
        prms.Add(new Parameter { Name = "@material_number", Value = data.MaterialNumber.Trim().ToUpper() });

        q = "sp_Grade_Save";
      }
      
      return execNonQuery(q, prms);
    }

    private static Product SetItem(DataRow dr)
    {
      return new Product {
        ProductId = dr["oil_company_code"].ToString(),
        ProductName = dr["grade_name"].ToString(),
        MaterialNumber = dr["material_number"].ToString()
      };
    }
    public static List<Product> GetAllItems()
    {
      var result = new List<Product>();
      var filter = tFilter();

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder, null);
      if (dt != null && dt.Rows.Count > 0)
      {
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }
                          
      return result;
    }
    public static List<Product> GetItems(int row = 10, int page = 1, string searchValue = "")
    {
      List<Product> result = null;
      var filter = tFilter(searchValue);

      DataTable dt = execQuery(tSQL + filter.Item1 + tOrder +
        $" OFFSET {(page - 1) * row} ROWS FETCH NEXT {row} ROWS ONLY ", filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        result = new List<Product>();
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }
    public static Product GetItem(string id)
    {
      List<Parameter> prms = new List<Parameter>
      {
        new Parameter { Name = "@id", Value = id }
      };

      DataTable dt = execQuery(tSQL + " WHERE deleted=0 AND oil_company_code=@id", prms);
      if (dt != null && dt.Rows.Count > 0)
        return SetItem(dt.Rows[0]);

      return null;
    }

    public static int ItemsCount(string searchValue = "")
    {
      var filter = tFilter(searchValue);

      DataTable dt = execQuery("SELECT COUNT(*) FROM tblm_grade " + filter.Item1, filter.Item2);
      if (dt != null && dt.Rows.Count > 0)
      {
        return (int)dt.Rows[0][0];
      }

      return 0;
    }

    public static List<Product> GetPSOItems()
    {
      var result = new List<Product>();

      DataTable dt = execQuery(tSQL + " WHERE deleted=0 AND oil_company_code IN ('11', '12')", null);
      if (dt != null && dt.Rows.Count > 0)
      {
        for (int i = 0; i < dt.Rows.Count; i++)
          result.Add(SetItem(dt.Rows[i]));
      }

      return result;
    }

    public static string Save(Product reg)
    {
      return DoCommand(NonQueryEnum.Insert, reg);      
    }
    public static string Delete(Product reg)
    {
      return DoCommand(NonQueryEnum.Delete, reg);
    }
  }
}

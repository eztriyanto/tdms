﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TDMS.Portal.Contexts;
using TDMS.Portal.Models;

namespace TDMS.Portal.BackgroundServices
{
  public class ODIService : IHostedService, IDisposable
  {
    private int executionCount = 0;
    private readonly ILogger<ODIService> _logger;
    private Timer _timer;
    private ConfigJson _config;

    private string ConvertItem(TankDelivery data)
    {
      var uid = data.UserId <= 0 ? 1 : data.UserId;
      var result = $"{data.Index},{data.TankDeliveryId},'{data.SiteId}'" +
        $",'{data.SiteName}','{data.SiteAddress}',{data.TankId},'{data.TankName}'" +
        $",{data.GradeId},'{data.MaterialNumber}','{data.GradeName}','{data.OilCompanyCode}'" +
        $",'{data.RecordEntryTS:yyyy-MM-dd HH:mm:ss}','{data.DropDateTime:yyyy-MM-dd HH:mm:ss}'" +
        $",'{data.OriginalInvoiceNumber}',{data.DispatchedVolume},'{data.PlantCode}'" + 
        $",'{data.DeliveryNoteNum}','{data.DeliveryDetail}','{data.DriverIdCode}','{data.TankerIdCode}'" +
        $",{data.VarianceAtRefTemp},{data.TemperatureVariance},{data.ReceivedVolAtRefTemp}" + 
        $",{data.DispatchedVolAtRefTemp},{data.DropVolume},{data.TotalVariance},{data.DiffVolume}" +
        $",'{data.UserName}','{data.CreatedDate:yyyy-MM-dd HH:mm:ss}','{data.UpdateDate:yyyy-MM-dd HH:mm:ss}'" + 
        $",{uid},'data'";

      return result;
    }

    public ODIService(ILogger<ODIService> logger)
    {
      _logger = logger;
      _config = JsonConvert.DeserializeObject<ConfigJson>(File.ReadAllText($"{AppContext.BaseDirectory}config.json"));
    }

    public Task StartAsync(CancellationToken stoppingToken)
    {
      _logger.LogInformation("ODI Service running.");

      var interval = _config.OdiIntervalInSecond <= 0 ? 600 : _config.OdiIntervalInSecond;
      _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(interval));

      return Task.CompletedTask;
    }

    private async void DoWork(object state)
    {
      var count = Interlocked.Increment(ref executionCount);

      //Get Unsync Tank Delivery to ODI
      var items = TankDeliveryContext.GetUnsyncItem();
      if (items != null && items.Count > 0)
      {
        var Client = new HttpClient { Timeout = TimeSpan.FromSeconds(5) };
        var auth64 = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_config.OdiUserId}:{_config.OdiPassword}"));

        Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", auth64);
        Client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");

        foreach (var item in items)
        {
          var endPoint = new Uri($"{_config.OdiEndpoint}/rest/create_tank_delivery");
          try
          {
            var parameters = new Dictionary<string, string>
            {
              { "out","application/json" },
              { "params", ConvertItem(item) }
            };
            var content = new FormUrlEncodedContent(parameters);

            HttpResponseMessage resp = await Client.PostAsync(endPoint, content).ConfigureAwait(false);
            if (resp.IsSuccessStatusCode && resp.Content != null)
            {
              var body = await resp.Content.ReadAsStringAsync().ConfigureAwait(false);
              var result = JsonConvert.DeserializeObject<OdiResponse>(body);
              if (result.status == "201")
              {
                var s = TankDeliveryContext.UpdateSync(item.Index);
                if (!string.IsNullOrEmpty(s))
                  _logger.LogError(s);
              }
              else
              {
                _logger.LogError($"Odi error {result.status}: {result.message}");
              }
            }
          }
          catch (Exception ex)
          {
            _logger.LogError(ex.Message);
          }
        }
      }

      _logger.LogInformation("ODI Service is working. Count: {Count}", count);
    }

    public Task StopAsync(CancellationToken stoppingToken)
    {
      _logger.LogInformation("ODI Service is stopping.");

      _timer?.Change(Timeout.Infinite, 0);

      return Task.CompletedTask;
    }

    public void Dispose()
    {
      _timer?.Dispose();
    }
  }
}

﻿/*!
 * Start Bootstrap - SB Admin 2 v4.0.2 (https://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2019 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap-sb-admin-2/blob/master/LICENSE)
 */

function blockIt(timeOut = 2000) {
  $.blockUI({
    css: {
      border: 'none',
      padding: '15px',
      backgroundColor: '#000',
      '-webkit-border-radius': '10px',
      '-moz-border-radius': '10px',
      opacity: .5,
      color: '#fff'
    }
  });
  setTimeout($.unblockUI, timeOut);
}

function isNumber(input) {
  return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
}

function strToNumber(value) {
  var v = value;
  if (isNumber(v)) { v = value.toString(); }

  return parseInt(v.replace(/[^0-9.-]+/g, ""), 10);
}

//function formatNString(value) {
//  var v = value;
//  if (isNumber(v)) { v = value.toString(); }

//  return v.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//}

function formatNString(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

function NumberFormatter(value, row, index) {
  return formatNString(value);
}

function NumberFormatter2(value, row, index) {
  return formatNString(value,2);
}

function IndexFormatter(value, row, index) {
  var tableOptions = $('#dtTable').bootstrapTable('getOptions');

  return ((tableOptions.pageNumber - 1) * tableOptions.pageSize) + (1 + index);
}

function DateFormatter(value, row, index) {
  return moment(value).format('YYYY-MM-DD HH:mm:ss');
}

function ShortDateFormatter(value, row, index) {
  return moment(value).format('YYYY-MM-DD');
}

!function () {
  function showAlert(alert) {
    const alertContainer = $('.alert-container');

    const alertElement = $('<div class="alert alert-${alert.type} alert-dismissible" role="alert">' +
      '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
      '<strong>${alert.title}</strong> ${alert.body}' +
      '</div>');

    alertContainer.append(alertElement);
    alertElement.alert();
  }

  $(document).ajaxComplete((event, xhr) => {
    if (xhr.getResponseHeader('x-alert-type')) {
      const alert = {
        type: xhr.getResponseHeader('x-alert-type'),
        title: xhr.getResponseHeader('x-alert-title'),
        body: xhr.getResponseHeader('x-alert-body')
      };

      showAlert(alert);
    }
  });
}(jQuery);

!function (t) { "use strict"; t("#sidebarToggle, #sidebarToggleTop").on("click", function (o) { t("body").toggleClass("sidebar-toggled"), t(".sidebar").toggleClass("toggled"), t(".sidebar").hasClass("toggled") && t(".sidebar .collapse").collapse("hide") }), t(window).resize(function () { t(window).width() < 768 && t(".sidebar .collapse").collapse("hide") }), t("body.fixed-nav .sidebar").on("mousewheel DOMMouseScroll wheel", function (o) { if (768 < t(window).width()) { var e = o.originalEvent, l = e.wheelDelta || -e.detail; this.scrollTop += 30 * (l < 0 ? 1 : -1), o.preventDefault() } }), t(document).on("scroll", function () { 100 < t(this).scrollTop() ? t(".scroll-to-top").fadeIn() : t(".scroll-to-top").fadeOut() }), t(document).on("click", "a.scroll-to-top", function (o) { var e = t(this); t("html, body").stop().animate({ scrollTop: t(e.attr("href")).offset().top }, 1e3, "easeInOutExpo"), o.preventDefault() }) }(jQuery);

$(document).ready(function () {
  $('.alert-container').first().hide().fadeIn(200).delay(2000).fadeOut(2000, function () {
    $(this).remove();
  });

  $('input.number').keyup(function (event) {
    if (event.which >= 37 && event.which <= 40) return;

    $(this).val(function (index, value) {
      return formatNString(value);
    });
  });
});
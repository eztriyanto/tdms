﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace TDMS.Portal
{
  public class DBHelper
  {
        public static string ConnectionString
        {
            get => GetConnectionString();
        }

        private static string GetConnectionString()
        {
            ConfigJson result = JsonConvert.DeserializeObject<ConfigJson>(File.ReadAllText($"{AppContext.BaseDirectory}config.json"));
            return result.ConnectionString;
        }
        public enum NonQueryEnum { Insert, Update, Delete }

        public static DataTable execQuery(string query, IList<Parameter> parameters)
        {
          DataTable result = new DataTable();
      
          using (SqlConnection con = new SqlConnection(ConnectionString))
          {
            using (SqlCommand cmd = new SqlCommand(query, con))
            {
              cmd.CommandType = CommandType.Text;

              if (parameters != null)
                foreach (Parameter p in parameters)
                  cmd.Parameters.AddWithValue(p.Name, p.Value);

              using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
              {
                sda.Fill(result);
              }
            }
          }
      
          return result;
        }
        public static DataTable execQueryFromSP(string spName, IList<Parameter> parameters)
        {
          DataTable result = new DataTable();

          using (SqlConnection con = new SqlConnection(ConnectionString))
          {
            using (SqlCommand cmd = new SqlCommand(spName, con))
            {
              cmd.CommandType = CommandType.StoredProcedure;

              if (parameters != null)
                foreach (Parameter p in parameters)
                  cmd.Parameters.AddWithValue(p.Name, p.Value);

              using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
              {
                sda.Fill(result);
              }
            }
          }

      return result;
    }

    public static string execNonQuery(string query, IList<Parameter> parameters)
        {
          string result = "";
          SqlTransaction trans = null;

          using (SqlConnection con = new SqlConnection(ConnectionString))
          {
            try
            {
              con.Open();
              trans = con.BeginTransaction();

              using (SqlCommand cmd = new SqlCommand(query, con))
              {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Transaction = trans;

              if (parameters != null)
                  foreach (Parameter p in parameters)
                    cmd.Parameters.AddWithValue(p.Name, p.Value);

                cmd.ExecuteNonQuery();
                trans.Commit();
              }
            }
      
            catch (Exception ex)
            {
              if (trans != null)
                trans.Rollback();

              result = ex.Message;
            }
          }

          return result;
        }
        public static string execNonQueries(IList<string> queries, IList<Parameter>[] parameters)
        {
          string result = "";
          SqlTransaction trans = null;

          using (SqlConnection con = new SqlConnection(ConnectionString))
          {
            try
            {
              con.Open();
              trans = con.BeginTransaction();

              using (SqlCommand cmd = new SqlCommand())
              {
                cmd.Connection = con;
                cmd.Transaction = trans;

                for (int i = 0; i < queries.Count; i++)
                {
                  cmd.CommandType = CommandType.StoredProcedure;
                  cmd.CommandText = queries[i];

                  cmd.Parameters.Clear(); 
                  if (parameters[i] != null)
                    foreach (Parameter p in parameters[i])
                      cmd.Parameters.AddWithValue(p.Name, p.Value);

                  cmd.ExecuteNonQuery();
                }
              }

              trans.Commit();
            }

            catch (Exception ex)
            {
              if (trans != null)
                trans.Rollback();

              result = ex.Message;
            }
          }

          return result;
        }

        public class Parameter
        {
          public string Name { get; set; }
          public dynamic Value { get; set; }
        }
  }
}

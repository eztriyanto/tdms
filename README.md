﻿﻿# Tank Delivery Monitoring System (TDMS)

System for receiving and monitoring tank delivery data from P-Insyst Point of Sales 

## Getting the sample

The easiest way to get is by cloning the samples repository with git, using the following instructions.

```console
git clone https://gitlab.com/eztriyanto/tdms.git/
```

You can also [download the repository as a zip](https://gitlab.com/eztriyanto/tdms.git/repository/master/archive.zip).

## Build and run the sample locally

Before buid and run locally you must set environtment variabel.
Setting the environment variable in **Windows**

```console
setx ASPNETCORE_ENVIRONMENT "Development"

setx DB__ConnectionString "<Database_Connetion_String>"
# Ex. Value: "Server=.; Database=db_tank_delivery; User Id=sa; Password=password;"

setx Token__Key "<Token_Key_Value>"
# Ex. Value: "2e7c6cde-6f93-4571-ad6b-da78b345ac95"
```

You can build and run locally with the [.NET Core 2.2.8 SDK](https://www.microsoft.com/net/download/core) using the following commands. The commands assume that you are in the root of the repository.

```console
cd tank_monitoring\portal
dotnet run
```

After the application starts, visit `http://localhost:5000` in your web browser.

You can produce an application that is ready to deploy to production locally using the following command.

```console
dotnet publish -c release -o out
```

You can run the application on **Windows** using the following command.

```console
dotnet out\tdms.portal.dll
```

You can run the application on **Linux or macOS** using the following command.

```console
dotnet out/tdms.portal.dll
```

Note: The `-c release` argument builds the application in release mode (the default is debug mode). See the [dotnet run reference](https://docs.microsoft.com/dotnet/core/tools/dotnet-run) for more information on commandline parameters.

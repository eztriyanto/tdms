﻿namespace TDMS.Agent
{
  partial class ProjectInstaller
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.o_ServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
      this.o_ServiceInstaller = new System.ServiceProcess.ServiceInstaller();
      // 
      // o_ServiceProcessInstaller
      // 
      this.o_ServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.NetworkService;
      this.o_ServiceProcessInstaller.Password = null;
      this.o_ServiceProcessInstaller.Username = null;
      // 
      // o_ServiceInstaller
      // 
      this.o_ServiceInstaller.Description = "Agent for sending tank delivery data";
      this.o_ServiceInstaller.DisplayName = "TDMS.Agent";
      this.o_ServiceInstaller.ServiceName = "TDMS.Agent";
      this.o_ServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
      // 
      // ProjectInstaller
      // 
      this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.o_ServiceProcessInstaller,
            this.o_ServiceInstaller});

    }

    #endregion

    private System.ServiceProcess.ServiceProcessInstaller o_ServiceProcessInstaller;
    private System.ServiceProcess.ServiceInstaller o_ServiceInstaller;
  }
}
﻿using System;
using System.IO;
using System.ServiceProcess;
using System.Timers;
using Microsoft.Owin.Hosting;
using NLog;

using TDMS.Agent.Models;
using TDMS.Agent.Controllers;

namespace TDMS.Agent
{
  public partial class ClientService : ServiceBase
  {
    private readonly string m_TitleApp = "Tank Delivery Monitoring System";
    private Logger o_Log;
    
    #region PROPERTY AND VARIABLE
    private Timer o_TimerAgent = new Timer();
    
    private Library.Models.Site o_Site = new Library.Models.Site();
    private DateTime d_Tank_Delivery_DT = new DateTime(2020, 1, 1);
    private DateTime d_Hose_Delivery_DT = new DateTime(2020, 1, 1);
    private Config o_Config = new Config();
    
    private readonly string s_LongDateFormat = "yyyy-MM-dd HH:mm:ss.fff";
    private readonly string s_ShortDateFormat = "yyyy-MM-dd";

    private string s_FileConfig = "";
    private string s_FileID = "";

    private IDisposable o_Server;
    #endregion

    #region CONFIGURATION
    private const string str_db_instance = "db_instance";
    private const string str_db_name = "db_name";
    private const string str_portal_host_name = "portal_host_name";
    private const string str_portal_port = "portal_port";
    private const string str_portal_use_ssl = "portal_use_ssl";
    private const string str_portal_enabled = "portal_enabled";
    private const string str_push_timer_in_second = "push_timer_in_second";
    private const string str_push_last_date = "push_last_date";
    #endregion
       
    #region FUNCTION
    private string ExePath()
    {
      string s_codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
      UriBuilder o_Uri = new UriBuilder(s_codeBase);
      string s_Path = Uri.UnescapeDataString(o_Uri.Path);

      return Path.GetDirectoryName(s_Path);
    }

    private void WriteConfig(Config conf)
    {
      string conf_data = str_db_instance + "=" + conf.db_instance + "\n" +
        str_db_name + "=" + conf.db_name + "\n" +
        str_portal_host_name + "=" + conf.portal_host_name + "\n" +
        str_portal_port + "=" + conf.portal_port + "\n" +
        str_portal_use_ssl + "=" + (conf.portal_use_ssl ? "1" : "0") + "\n" +
        str_portal_enabled + "=" + (conf.portal_enabled ? "1" : "0") + "\n" +
        str_push_timer_in_second + "=" + conf.push_timer_in_second + "\n" +
        str_push_last_date + "=" + conf.push_last_date.ToString(s_ShortDateFormat) + "\n";

      File.WriteAllText(s_FileConfig, conf_data);
    }
    private void ParsingConfig()
    {
      bool isUpdate = false;

      o_Config = new Config();

      string[] lines = File.ReadAllLines(s_FileConfig);
      foreach (string line in lines)
      {
        if (line[0] != '#' && line.Contains("="))
        {
          string[] config = line.Split('=');
          if (config.Length == 2)
          {
            switch (config[0].ToLower().Trim())
            {
              case str_db_instance:
                o_Config.db_instance = config[1].Trim();
                o_Log.Info(str_db_instance + ": " + o_Config.db_instance);
                break;

              case str_db_name:
                o_Config.db_name = config[1].Trim();
                o_Log.Info(str_db_name + ": " + o_Config.db_name);
                break;

              case str_portal_host_name:
                o_Config.portal_host_name = config[1].Trim();
                o_Log.Info(str_portal_host_name + ": " + o_Config.portal_host_name);
                break;

              case str_portal_port:
                int.TryParse(config[1].Trim(), out int i_portal_port);
                o_Config.portal_port = i_portal_port;

                o_Log.Info(str_portal_port + ": " + o_Config.portal_port);
                break;

              case str_portal_use_ssl:
                int.TryParse(config[1].Trim(), out int i_portal_use_ssl);
                if (i_portal_use_ssl != 1)
                  o_Config.portal_use_ssl = false;

                o_Log.Info(str_portal_use_ssl + ": " + o_Config.portal_use_ssl);
                break;

              case str_portal_enabled:
                int.TryParse(config[1].Trim(), out int i_portal_enabled);
                if (i_portal_enabled != 1)
                  o_Config.portal_enabled = false;

                o_Log.Info(str_portal_enabled + ": " + o_Config.portal_enabled);
                break;

              case str_push_timer_in_second:
                int.TryParse(config[1].Trim(), out int i_interval);
                if (i_interval > 0)
                  o_Config.push_timer_in_second = i_interval;

                o_Log.Info(str_push_timer_in_second + ": " + o_Config.push_timer_in_second);
                break;

              case str_push_last_date:
                if (string.IsNullOrEmpty(config[1]) || config[1].Trim() == "")
                {
                  o_Config.push_last_date = DateTime.Today.AddDays(-1);
                  isUpdate = true;
                }

                else if (DateTime.TryParse(config[1].Trim(), out DateTime d_date))
                  o_Config.push_last_date = d_date;

                o_Log.Info(str_push_last_date + ": " + o_Config.push_last_date.ToString(s_ShortDateFormat));
                break;
            }
          }
        }
      }

      if (isUpdate)
      {
        WriteConfig(o_Config);
      }
    }

    #endregion

    #region Portal
    private void GetLastKey()
    {
      try
      {
        string s_Data = Library.Controllers.SecurityController.Decrypt(File.ReadAllText(s_FileID));
        if (!string.IsNullOrEmpty(s_Data) && s_Data.Contains("#"))
        {
          string[] s_ID = s_Data.Split('#');
          if (!DateTime.TryParse(s_ID[1], out DateTime d_Tank_Delivery_DT))
            d_Tank_Delivery_DT = new DateTime(2020, 1, 1);

          if (!DateTime.TryParse(s_ID[2], out DateTime d_Hose_Delivery_DT))
            d_Hose_Delivery_DT = new DateTime(2020, 1, 1);
        }

        if (o_Config.push_last_date > d_Tank_Delivery_DT)
        {
          d_Tank_Delivery_DT = o_Config.push_last_date;
        }

        if (o_Config.push_last_date.Date > d_Hose_Delivery_DT.Date)
        {
          d_Hose_Delivery_DT = o_Config.push_last_date;
        }
      }
      catch { }
    }
    private bool PushTankDelivery()
    {
      var o_DB = new DBController();
      var o_Deliveries = o_DB.GetTankDeliveries(d_Tank_Delivery_DT, o_Site);
      if (o_Deliveries.Count > 0)
      {
        string log = $"Posting {o_Deliveries.Count} tank delivery data to portal " +
          $" from drop_date_time {o_Deliveries[0].DropDateTime.ToString(s_LongDateFormat)} " + 
          $"to {o_Deliveries[o_Deliveries.Count - 1].DropDateTime.ToString(s_LongDateFormat)}";
        o_Log.Info(log);

        PortalController o_Portal = new PortalController(o_Config.portal_host_name, o_Config.portal_port, o_Config.portal_use_ssl);
        PortalResponse res = o_Portal.SendTankDelivery(new TankDeliveryRequest { items = o_Deliveries });
        if (res.status == 200)
        {
          d_Tank_Delivery_DT = o_Deliveries[o_Deliveries.Count - 1].DropDateTime;
          o_Log.Info("Posting tank delivery data successfully.");

          return true;
        }
          
        else
          throw new Exception($"Error {res.status}: {res.title}");
      }
      else
      {
        o_Log.Warn("No tank delivery data to be posted to portal.");
      }

      return false;
    }
    private bool PushHoseDelivery()
    {
      var o_DB = new DBController();
      var o_Deliveries = o_DB.GetHoseDeliveries(d_Hose_Delivery_DT, o_Site);
      if (o_Deliveries.Count > 0)
      {
        string log = $"Posting {o_Deliveries.Count} hose delivery data to portal " +
         $" from completed_ts {o_Deliveries[0].DeliveryDate.ToString(s_ShortDateFormat)} " +
         $"to {o_Deliveries[o_Deliveries.Count - 1].DeliveryDate.ToString(s_ShortDateFormat)}";
        o_Log.Info(log);

        PortalController o_Portal = new PortalController(o_Config.portal_host_name, o_Config.portal_port, o_Config.portal_use_ssl);
        PortalResponse res = o_Portal.SendHoseDelivery(new HoseDeliveryRequest { items = o_Deliveries });
        if (res.status == 200)
        {
          d_Hose_Delivery_DT = o_Deliveries[o_Deliveries.Count - 1].DeliveryDate.AddDays(1);
          o_Log.Info("Posting hose delivery data successfully.");

          return true;
        }

        else
          throw new Exception($"Error {res.status}: {res.title}");
      }
      else
      {
        o_Log.Warn("No hose delivery data to be posted to portal.");
      }

      return false;
    }
    private void UpdatePushKey()
    {
      File.WriteAllText(s_FileID, Library.Controllers.SecurityController
        .Encrypt(DateTime.Now + 
          "#" + d_Tank_Delivery_DT.ToString(s_LongDateFormat) +
          "#" + d_Hose_Delivery_DT.ToString(s_ShortDateFormat)));

      o_Log.Info("Saving last data pointer");
    }
    #endregion
    
    public ClientService()
    {
      InitializeComponent();
    }

    protected void o_TimerAgent_Elapsed(object sender, ElapsedEventArgs e)
    {
      o_TimerAgent.Stop();

      if (o_Config.portal_enabled)
        try
        {
          var o_DB = new DBController();
          o_Site =  o_DB.GetSite();

          GetLastKey();

          if(PushTankDelivery())
            UpdatePushKey();
          
          if (PushHoseDelivery())
            UpdatePushKey();
        }
        catch (Exception ex)
        {
          o_Log.Error(ex.Message);
        }

      o_TimerAgent.Start();
    }
    
    protected override void OnStart(string[] args)
    {
      try
      {
        var o_Culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
        System.Globalization.CultureInfo.DefaultThreadCurrentCulture = o_Culture;

        o_Log = LogManager.GetCurrentClassLogger();
        o_Log.Info("****  Start initializing " + m_TitleApp + " ****");

        //ASP WEB API
        o_Log.Info("Starting API Server");
        var baseAddress = "http://+:13501/api";
        o_Server = WebApp.Start<Startup>(url: baseAddress);
        o_Log.Info("API Server running @ " + baseAddress);

        //Config.dat parsing
        s_FileConfig = ExePath() + "\\config.dat";
        s_FileID = ExePath() + "\\last_id.dat";
        ParsingConfig();

        //Init Database controller
        ApiHelper.StrConn = "Server=" + o_Config.db_instance + ";Database=" + o_Config.db_name + ";Trusted_Connection=True;";
        var o_DB = new DBController();

        //Get site data
        o_Site = o_DB.GetSite();
        o_Log.Info("Site data: " + o_Site.SiteId + " [" + o_Site.SiteName + "]");

        o_TimerAgent.Interval = o_Config.push_timer_in_second * 1000;
        o_TimerAgent.Elapsed += new ElapsedEventHandler(o_TimerAgent_Elapsed);
        o_TimerAgent.Start();
      }
      catch (Exception ex)
      {
        if (ex.InnerException != null)
          o_Log.Error(ex.InnerException.Message);
        else
          o_Log.Error(ex.Message);

        ExitCode = 13816;
        Stop();
      }
    }

    protected override void OnStop()
    {
      o_Log.Info("****  Stoping " + m_TitleApp + " ****");

      try
      {
        if (o_Server != null)
        {
          o_Server.Dispose();
        }
      }
      catch { }

      try
      {
        o_TimerAgent.Stop();
        o_TimerAgent.Dispose();
      }
      catch { }

      o_Log.Info("****  " + m_TitleApp + " Stopped ****");
    }
  }
}

﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Newtonsoft.Json;

using TDMS.Agent.Models;

namespace TDMS.Agent.Controllers
{
  public class PortalController
  {
    private string hostName = "custodytransfer.pertamina.com";
    private int portNumber = 0;
    private bool useSSL = false;
    
    public PortalController(string host, int port, bool ssl)
    {
      hostName = host;
      portNumber = port;
      useSSL = ssl;
    }

    private HttpWebRequest SetServer(string method)
    {
      string port = portNumber <= 0 ? "" : ":" + portNumber;
      string BaseURL = (useSSL ? "https://" : "http://") + hostName + port + "/api/" + method;

      ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls |
        SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

      var result = WebRequest.CreateHttp(BaseURL);
      result.ContentType = "application/json; charset=utf-8";
      result.Method = "POST";
      result.Timeout = 300000; //5 minutes

      return result;
    }

    public PortalResponse SendTankDelivery(TankDeliveryRequest request)
    {
      var response = new PortalResponse();
      try
      {
        var service = SetServer("SendTankDelivery");
        using (var streamWriter = new StreamWriter(service.GetRequestStream()))
        {
          streamWriter.Write(JsonConvert.SerializeObject(request));
          streamWriter.Flush();
        }

        var httpResponse = (HttpWebResponse)service.GetResponse();
        if (httpResponse.StatusCode != HttpStatusCode.OK && httpResponse.ContentLength > 0)
          using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            response = JsonConvert.DeserializeObject<PortalResponse>(streamReader.ReadToEnd());

        else
          response = new PortalResponse
          {
            status = (int)httpResponse.StatusCode,
            title = httpResponse.StatusDescription
          };
      }
      catch (Exception ex)
      {
        response.status = 500;
        response.title = ex.Message;
        response.errors = new List<PortalErrorDetail> {
          new PortalErrorDetail { message = ex.InnerException?.Message ?? ex.Message }
        };
      }

      return response;
    }

    public PortalResponse SendHoseDelivery(HoseDeliveryRequest request)
    {
      var response = new PortalResponse();
      try
      {
        var service = SetServer("SendHoseDelivery");
        using (var streamWriter = new StreamWriter(service.GetRequestStream()))
        {
          streamWriter.Write(JsonConvert.SerializeObject(request));
          streamWriter.Flush();
        }

        var httpResponse = (HttpWebResponse)service.GetResponse();
        if (httpResponse.StatusCode != HttpStatusCode.OK && httpResponse.ContentLength > 0)
          using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            response = JsonConvert.DeserializeObject<PortalResponse>(streamReader.ReadToEnd());

        else
          response = new PortalResponse
          {
            status = (int)httpResponse.StatusCode,
            title = httpResponse.StatusDescription
          };
      }
      catch (Exception ex)
      {
        response.status = 500;
        response.title = ex.Message;
        response.errors = new List<PortalErrorDetail> {
          new PortalErrorDetail { message = ex.InnerException?.Message ?? ex.Message }
        };
      }

      return response;
    }
  }
}

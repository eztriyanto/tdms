﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TDMS.Library.Models;

namespace TDMS.Agent.Controllers
{
  public class DBController
  {
    private string StrConn = "Server=.\\SQLEXPRESS;Database=EnablerDB;Trusted_Connection=True;";

    private readonly string LongDateFormat = "yyyy-MM-dd HH:mm:ss.fff";
    private readonly string ShortDateFormat = "yyyy-MM-dd";

    private string tankQuery = "SELECT a.Tank_ID,Tank_Name,Tank_Number,Capacity,Tank_Probe_Status_ID," +
      "Tank_Readings_DT,Gauge_Volume,Ullage,Tank_Type_ID,a.Grade_ID,ISNULL(b.Grade_Name, '') AS Grade_Name," +
      "ISNULL(b.Oil_Company_Code, '') AS Company_Code,ISNULL(c.Grade_Price, 0) AS Grade_Price," +
      "ISNULL(Tank_Delivery_ID,0) AS Tank_Delivery_ID " +
      "FROM Tanks a LEFT JOIN Grades b ON a.Grade_ID = b.Grade_ID " +
      "LEFT JOIN Price_Levels c ON b.Price_Profile_ID = c.Price_Profile_ID " +
      "LEFT JOIN (SELECT MAX(Tank_Delivery_ID) AS Tank_Delivery_ID,Tank_ID FROM Tank_Delivery " +
      "WHERE Drop_Date_Time IS NULL AND Tank_Movement_Type_ID <> 0 GROUP BY Tank_ID" +
      ") d ON a.Tank_ID = d.Tank_ID WHERE a.Deleted = 0 ";

    private string tankDeliveryQuery = "SELECT TOP 50 ISNULL(a.tank_delivery_id, 0) AS tank_delivery_id, " +
      "ISNULL(c.oil_company_code, '') AS oil_company_code, ISNULL(a.period_id, 0) AS period_id, " +
      "ISNULL(e.period_number, 0) AS period_number, ISNULL(a.tank_id, 0) AS tank_id, ISNULL(b.tank_name, '') AS tank_name, " +
      "ISNULL(b.grade_id, 0) AS grade_id, ISNULL(c.grade_name, '') AS grade_name, a.drop_date_time, " +
      "a.record_entry_ts, ISNULL(a.drop_volume, 0) AS drop_volume, ISNULL(a.delivery_note_num, '') AS delivery_note_num, " +
      "ISNULL(a.drop_volume_theo, 0) AS drop_volume_theo, ISNULL(a.unit_cost_price, 0) AS unit_cost_price, " +
      "ISNULL(a.driver_id_code, '') AS driver_id_code, ISNULL(a.tanker_id_code, '') AS tanker_id_code, " +
      "ISNULL(a.delivery_detail, '') AS delivery_detail, ISNULL(a.dispatched_volume, 0) AS dispatched_volume, " +
      "ISNULL(a.original_invoice_number, '') AS original_invoice_number, " +
      "ISNULL(a.received_vol_at_ref_temp, 0) AS received_vol_at_ref_temp, " +
      "ISNULL(a.dispatched_vol_at_ref_temp, 0) AS dispatched_vol_at_ref_temp, " +
      "ISNULL(a.total_variance, 0) AS total_variance, " +
      "ISNULL(a.variance_at_ref_temp, 0) AS variance_at_ref_temp, " +
      "ISNULL(a.temperature_variance, 0) AS temperature_variance, " +
      "ISNULL(a.tank_movement_type_id, 0) AS tank_movement_type_id, " +
      "ISNULL(a.user_id, 0) AS user_id,  ISNULL(d.attendant_name, '') AS attendant_name, " +
      "ISNULL(a.total_variance, 0) AS volume_sales, ISNULL(b.gauge_volume,0) AS gauge_volume, " +
      "ISNULL(b.theoretical_volume,0) AS theoretical_volume, " +
      "ISNULL(b.capacity,0) AS tank_capacity " +
      "FROM Tank_Delivery AS a " +
      "LEFT JOIN Tanks AS b ON a.Tank_ID = b.Tank_ID " +
      "LEFT JOIN Grades AS c ON b.Grade_ID = c.Grade_ID " +
      "LEFT JOIN Attendant AS d ON a.User_ID = d.Attendant_ID " +
      "LEFT JOIN Periods AS e ON a.period_id = e.period_id ";

    public DBController()
    {
      StrConn = ApiHelper.StrConn;
    }
    public DBController(string StringConnection)
    {
      StrConn = StringConnection;
      ApiHelper.StrConn = StrConn;
    }

    public Attendant AttendantLogin(string LogonId, string Pwd)
    {
      if (string.IsNullOrEmpty(LogonId) || string.IsNullOrEmpty(Pwd))
        return null;

      string pwdDecrypted = Library.Controllers.SecurityController.Decrypt(Pwd);
      Attendant result = null;
      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();

        string sql = "SELECT TOP 1 a.Attendant_ID, Attendant_Name, Attendant_Logon_ID, ISNULL(Role_ID,0) AS Role_ID " +
          "FROM Attendant a LEFT JOIN Attendant_Role b ON a.Attendant_ID = b.Attendant_ID " +
          "WHERE Deleted=0 AND Attendant_Logon_ID=@Id AND Attendant_Password=@Password";
        using (SqlCommand o_Cmd = new SqlCommand(sql, o_Conn))
        {
          o_Cmd.Parameters.AddWithValue("@Id", LogonId);
          o_Cmd.Parameters.AddWithValue("@Password", pwdDecrypted);
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
            {
              if (o_Read.Read() && (int)o_Read["Attendant_ID"] > 0)
              {
                result = new Attendant
                {
                  Id = (int)o_Read["Attendant_ID"],
                  Name = o_Read["Attendant_Name"].ToString().Trim(),
                  LogonId = o_Read["Attendant_Logon_ID"].ToString().Trim(),
                  Password = Pwd,
                  Role = (Attendant.eRole)o_Read["Role_ID"]
                };
              }
            }
            else
            {
              result = null;
            }
          }
        }
      }

      return result;
    }

    public Site GetSite()
    {
      var result = new Site();
      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();

        string sql = "SELECT TOP 1 site_id, site_address, site_name FROM Site_Profile_ex";
        using (SqlCommand o_Cmd = new SqlCommand(sql, o_Conn))
        {
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
            {
              if (o_Read.Read())
              {
                result.SiteId = o_Read["site_id"].ToString().Trim();
                result.SiteName = o_Read["site_name"].ToString().Trim();
                result.SiteAddress = o_Read["site_address"].ToString().Trim();
              }
            }
          }
        }
      }

      return result;
    }

    public Tank GetTank(int Number)
    {
      var result = new Tank();
      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();

        using (SqlCommand o_Cmd = new SqlCommand(tankQuery + "AND a.Tank_Number=@number", o_Conn))
        {
          o_Cmd.Parameters.AddWithValue("@number", Number);
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
            {
              if (o_Read.Read())
              {
                result.Id = (int)o_Read["Tank_ID"];
                result.Name = o_Read["Tank_Name"].ToString().Trim();
                result.Number = (int)o_Read["Tank_Number"];
                result.Type = (int)o_Read["Tank_Type_ID"];
                result.Volume = (decimal)o_Read["Gauge_Volume"];
                result.Capacity = (decimal)o_Read["Capacity"];
                result.Ullage = (decimal)o_Read["Ullage"];
                result.ProbeStatus = (int)o_Read["Tank_Probe_Status_ID"];
                result.LastReading = o_Read["Tank_Readings_DT"] == DBNull.Value ? DateTime.MinValue : (DateTime)o_Read["Tank_Readings_DT"];
                result.DeliveryId = (int)o_Read["Tank_Delivery_ID"];
                result.Grade = new Product {
                  ProductId = (int)o_Read["Grade_ID"],
                  ProductName = o_Read["Grade_Name"].ToString().Trim(),
                  CompanyCode = o_Read["Company_Code"].ToString().Trim(),
                  Price = (decimal)o_Read["Grade_Price"]
                };
              }
            }
            else
            {
              result = null;
            }
          }
        }
      }

      return result;
    }

    public List<Tank> GetTanks()
    {
      var result = new List<Tank>();
      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();

        using (SqlCommand o_Cmd = new SqlCommand(tankQuery + "ORDER BY Tank_Number", o_Conn))
        {
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
            {
              while (o_Read.Read())
              {
                result.Add(new Tank {
                  Id = (int)o_Read["Tank_ID"],
                  Name = o_Read["Tank_Name"].ToString().Trim(),
                  Number = (int)o_Read["Tank_Number"],
                  Type = (int)o_Read["Tank_Type_ID"],
                  Volume = (decimal)o_Read["Gauge_Volume"],
                  Capacity = (decimal)o_Read["Capacity"],
                  Ullage = (decimal)o_Read["Ullage"],
                  ProbeStatus = (int)o_Read["Tank_Probe_Status_ID"],
                  LastReading = o_Read["Tank_Readings_DT"] == DBNull.Value ? DateTime.MinValue : (DateTime)o_Read["Tank_Readings_DT"],
                  DeliveryId = (int)o_Read["Tank_Delivery_ID"],
                  Grade = new Product
                  {
                    ProductId = (int)o_Read["Grade_ID"],
                    ProductName = o_Read["Grade_Name"].ToString().Trim(),
                    CompanyCode = o_Read["Company_Code"].ToString().Trim(),
                    Price = (decimal)o_Read["Grade_Price"]
                  }
                });
              }
            }
            else
            {
              result = null;
            }
          }
        }
      }

      return result;
    }

    public List<TankDelivery> GetTankDeliveries(DateTime DropDate, Site site)
    {
      var result = new List<TankDelivery>();
      var sql = tankDeliveryQuery + "WHERE a.Tank_Movement_Type_ID = 1 AND Drop_Date_Time IS NOT NULL " +
        "AND Drop_Date_Time > '" + DropDate.ToString(LongDateFormat) + "' " +
        "ORDER BY Drop_Date_Time";

      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();
        using (SqlCommand o_Cmd = new SqlCommand(sql, o_Conn))
        {
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
            {
              while (o_Read.Read())
              {
                DateTime dt = DateTime.MinValue;
                if (o_Read["drop_date_time"] != DBNull.Value)
                  dt = Convert.ToDateTime(o_Read["drop_date_time"]);

                DateTime entry_dt = DateTime.MinValue;
                if (o_Read["record_entry_ts"] != DBNull.Value)
                  entry_dt = Convert.ToDateTime(o_Read["record_entry_ts"]);

                result.Add(new TankDelivery
                {
                  SiteId = site.SiteId,
                  SiteName = site.SiteName,
                  SiteAddress = site.SiteAddress,
                  TankDeliveryId = Convert.ToInt32(o_Read["tank_delivery_id"]),
                  OilCompanyCode = o_Read["oil_company_code"].ToString().Trim(),
                  PeriodId = Convert.ToInt32(o_Read["period_id"]),
                  PeriodNumber = Convert.ToInt32(o_Read["period_number"]),
                  TankId = Convert.ToInt32(o_Read["tank_id"]),
                  TankName = o_Read["tank_name"].ToString().Trim(),
                  GradeId = Convert.ToInt32(o_Read["grade_id"]),
                  GradeName = o_Read["grade_name"].ToString().Trim(),
                  DropDateTime = dt,
                  RecordEntryTS = entry_dt,
                  DropVolume = Convert.ToDouble(o_Read["drop_volume"]),
                  DeliveryNoteNum = o_Read["delivery_note_num"].ToString().Trim(),
                  DropVolumeTheo = Convert.ToDouble(o_Read["drop_volume_theo"]),
                  UnitCostPrice = Convert.ToDouble(o_Read["unit_cost_price"]),
                  DriverIdCode = o_Read["driver_id_code"].ToString().Trim(),
                  TankerIdCode = o_Read["tanker_id_code"].ToString().Trim(),
                  DeliveryDetail = o_Read["delivery_detail"].ToString().Trim(),
                  DispatchedVolume = Convert.ToDouble(o_Read["dispatched_volume"]),
                  OriginalInvoiceNumber = o_Read["original_invoice_number"].ToString().Trim(),
                  ReceivedVolAtRefTemp = Convert.ToDouble(o_Read["received_vol_at_ref_temp"]),
                  DispatchedVolAtRefTemp = Convert.ToDouble(o_Read["dispatched_vol_at_ref_temp"]),
                  TotalVariance = Convert.ToDouble(o_Read["total_variance"]),
                  VarianceAtRefTemp = Convert.ToDouble(o_Read["variance_at_ref_temp"]),
                  TemperatureVariance = Convert.ToDouble(o_Read["temperature_variance"]),
                  TankMovementTypeId = Convert.ToInt32(o_Read["tank_movement_type_id"]),
                  UserId = Convert.ToInt32(o_Read["user_id"]),
                  UserName = o_Read["attendant_name"].ToString().Trim()
                });
              }
            }
          }
        }
      }

      return result;
    }

    public TankDelivery GetTankDelivery(int DeliveryID)
    {
      var result = new TankDelivery();
      var sql = tankDeliveryQuery + "WHERE a.tank_delivery_id=@id";

      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();
        using (SqlCommand o_Cmd = new SqlCommand(sql, o_Conn))
        {
          o_Cmd.Parameters.AddWithValue("@id", DeliveryID);
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
            {
              if (o_Read.Read())
              {
                DateTime dt = DateTime.MinValue;
                if (o_Read["drop_date_time"] != DBNull.Value)
                  dt = Convert.ToDateTime(o_Read["drop_date_time"]);

                DateTime entry_dt = DateTime.MinValue;
                if (o_Read["record_entry_ts"] != DBNull.Value)
                  entry_dt = Convert.ToDateTime(o_Read["record_entry_ts"]);

                result.TankDeliveryId = Convert.ToInt32(o_Read["tank_delivery_id"]);
                result.OilCompanyCode = o_Read["oil_company_code"].ToString().Trim();
                result.PeriodId = Convert.ToInt32(o_Read["period_id"]);
                result.PeriodNumber = Convert.ToInt32(o_Read["period_number"]);
                result.TankId = Convert.ToInt32(o_Read["tank_id"]);
                result.TankName = o_Read["tank_name"].ToString().Trim();
                result.GradeId = Convert.ToInt32(o_Read["grade_id"]);
                result.GradeName = o_Read["grade_name"].ToString().Trim();
                result.DropDateTime = dt;
                result.RecordEntryTS = entry_dt;
                result.DropVolume = Convert.ToDouble(o_Read["drop_volume"]);
                result.DeliveryNoteNum = o_Read["delivery_note_num"].ToString().Trim();
                result.DropVolumeTheo = Convert.ToDouble(o_Read["drop_volume_theo"]);
                result.UnitCostPrice = Convert.ToDouble(o_Read["unit_cost_price"]);
                result.DriverIdCode = o_Read["driver_id_code"].ToString().Trim();
                result.TankerIdCode = o_Read["tanker_id_code"].ToString().Trim();
                result.DeliveryDetail = o_Read["delivery_detail"].ToString().Trim();
                result.DispatchedVolume = Convert.ToDouble(o_Read["dispatched_volume"]);
                result.OriginalInvoiceNumber = o_Read["original_invoice_number"].ToString().Trim();
                result.ReceivedVolAtRefTemp = Convert.ToDouble(o_Read["received_vol_at_ref_temp"]);
                result.DispatchedVolAtRefTemp = Convert.ToDouble(o_Read["dispatched_vol_at_ref_temp"]);
                result.TotalVariance = Convert.ToDouble(o_Read["total_variance"]);
                result.VarianceAtRefTemp = Convert.ToDouble(o_Read["variance_at_ref_temp"]);
                result.TemperatureVariance = Convert.ToDouble(o_Read["temperature_variance"]);
                result.TankMovementTypeId = Convert.ToInt32(o_Read["tank_movement_type_id"]);
                result.UserId = Convert.ToInt32(o_Read["user_id"]);
                result.UserName = o_Read["attendant_name"].ToString().Trim();
              }
            }
            else
            {
              result = null;
            }
          }
        }
      }

      return result;
    }

    private int GetPeriodId()
    {
      int result = 0;

      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();
        using (SqlCommand o_Cmd = new SqlCommand("SELECT Period_ID FROM vw1_Periods " + 
          "WHERE Period_ID=(SELECT MAX(Period_ID) FROM Periods WHERE Period_Type=1", o_Conn))
        {
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
              if (o_Read.Read())
                result = (int)o_Read["Period_ID"];
          }
        }
      }

      return result;
    }
    public string StartTankDelivery(int TankID, string doNumber, double doVolume, string driverName,
      string vehicleNumber, string supplier, string supplyPoint, double Stock, int attId)
    {
      string result = "";
      SqlTransaction m_Trans;

      try
      {
        using (SqlConnection o_Conn = new SqlConnection(StrConn))
        {
          o_Conn.Open();
          m_Trans = o_Conn.BeginTransaction();

          using (SqlCommand o_Cmd = new SqlCommand("sp1_Start_Tank_Delivery", o_Conn))
          {
            try
            {
              o_Cmd.CommandType = System.Data.CommandType.StoredProcedure;
              o_Cmd.Parameters.AddWithValue("@tank_id", TankID);
              o_Cmd.Parameters.AddWithValue("@period_id", GetPeriodId());
              o_Cmd.Parameters.AddWithValue("@do_number", doNumber);
              o_Cmd.Parameters.AddWithValue("@do_volume", doVolume);
              o_Cmd.Parameters.AddWithValue("@driver_name", driverName);
              o_Cmd.Parameters.AddWithValue("@vehicle_number", vehicleNumber);
              o_Cmd.Parameters.AddWithValue("@supplier", supplier);
              o_Cmd.Parameters.AddWithValue("@supply_point", supplyPoint);
              o_Cmd.Parameters.AddWithValue("@density", 0);
              o_Cmd.Parameters.AddWithValue("@price", 0);
              o_Cmd.Parameters.AddWithValue("@attendant_id", attId);
              o_Cmd.Parameters.AddWithValue("@stock", Stock);
              o_Cmd.ExecuteNonQuery();

              m_Trans.Commit();
            }
            catch (Exception ex)
            {
              m_Trans.Rollback();
              result = Library.Helper.ErrorMsg(ex);
            }
          }
        }
      }
      catch (Exception ex)
      {
        result = Library.Helper.ErrorMsg(ex);
      }

      return result;
    }

    private double SalesVolume(int TankID, DateTime PeriodDate)
    {
      double result = 0;

      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();
        using (SqlCommand o_Cmd = new SqlCommand("SELECT ISNULL(SUM(Delivery_Volume),0) " + 
          "FROM Hose_Delivery a INNER JOIN (SELECT Hose_ID FROM Hoses WHERE Tank_ID=@tank_id) AS b " + 
          "ON a.Hose_ID = b.Hose_ID WHERE (Completed_TS BETWEEN @start_date AND GETDATE())", o_Conn))
        {
          o_Cmd.Parameters.AddWithValue("@tank_id", TankID);
          o_Cmd.Parameters.AddWithValue("@start_date", PeriodDate);
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
              if (o_Read.Read())
                result = Convert.ToDouble(o_Read[0]);
          }
        }
      }

      return result;
    }
    private double SalesVolume(int TankID, int DeliveryID)
    {
      double result = 0;

      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();
        using (SqlCommand o_Cmd = new SqlCommand("SELECT ISNULL(SUM(Delivery_Volume),0) " +
          "FROM Hose_Delivery a INNER JOIN (SELECT Hose_ID FROM Hoses WHERE Tank_ID=@tank_id) AS b ON a.Hose_ID = b.Hose_ID " + 
          "WHERE (Completed_TS BETWEEN ISNULL((SELECT Record_Entry_TS FROM Tank_Delivery " +
          "WHERE Delivery_ID=@delivery_id),GETDATE()) AND GETDATE())", o_Conn))
        {
          o_Cmd.Parameters.AddWithValue("@tank_id", TankID);
          o_Cmd.Parameters.AddWithValue("@delivery_id", DeliveryID);
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
              if (o_Read.Read())
                result = Convert.ToDouble(o_Read[0]);
          }
        }
      }

      return result;
    }
    public string FinishTankDelivery(int TankID, int DeliveryID, double Stock)
    {
      string result = "";
      SqlTransaction m_Trans;

      try
      {
        using (SqlConnection o_Conn = new SqlConnection(StrConn))
        {
          o_Conn.Open();
          m_Trans = o_Conn.BeginTransaction();

          using (SqlCommand o_Cmd = new SqlCommand("sp1_Stop_Tank_Delivery", o_Conn))
          {
            try
            {
              o_Cmd.CommandType = System.Data.CommandType.StoredProcedure;
              o_Cmd.Parameters.AddWithValue("@tank_delivery_id", DeliveryID);
              o_Cmd.Parameters.AddWithValue("@stock", Stock);
              o_Cmd.Parameters.AddWithValue("@sales_total", SalesVolume(TankID, DeliveryID));
              o_Cmd.ExecuteNonQuery();

              m_Trans.Commit();
            }
            catch (Exception ex)
            {
              m_Trans.Rollback();
              result = Library.Helper.ErrorMsg(ex);
            }
          }
        }
      }
      catch (Exception ex)
      {
        result = Library.Helper.ErrorMsg(ex);
      }

      return result;
    }

    public string CancelTankDelivery(int DeliveryID)
    {
      string result = "";

      try
      {
        using (SqlConnection o_Conn = new SqlConnection(StrConn))
        {
          o_Conn.Open();
          using (SqlCommand o_Cmd = new SqlCommand("UPDATE Tank_Delivery SET Tank_Movement_Type_ID=0 WHERE Tank_Delivery_ID=@id", o_Conn))
          {
            o_Cmd.Parameters.AddWithValue("@id", DeliveryID);
            o_Cmd.ExecuteNonQuery();
          }
        }
      }
      catch (Exception ex)
      {
        result = Library.Helper.ErrorMsg(ex);
      }

      return result;
    }

    public List<HoseDelivery> GetHoseDeliveries(DateTime CompletedTS, Site site)
    {
      var result = new List<HoseDelivery>();

      var sqlFrom = "COUNT(*) AS total_delivery, " +
        "SUM(CASE a.delivery_type WHEN 7 THEN 1 ELSE 0 END) AS total_pre " +
        "FROM Hose_Delivery a " +
        "INNER JOIN hoses b ON a.hose_id = b.hose_id " +
        "INNER JOIN grades c ON b.grade_id = c.grade_id ";

      var sqlWhere = "WHERE CAST(a.completed_ts AS DATE) BETWEEN @date AND DATEADD(DAY,30,@date) " +
        "AND CAST(a.completed_ts AS DATE) < CAST(GETDATE() AS DATE) ";

    var sql = "DECLARE @date datetime = '" + CompletedTS.ToString(ShortDateFormat) + "'; " +
        "SELECT @date = MIN(CAST(Completed_TS AS DATE)) FROM Hose_Delivery a " +
        "INNER JOIN hoses b ON a.hose_id = b.hose_id " +
        "INNER JOIN grades c ON b.grade_id = c.grade_id " +
        "WHERE CAST(Completed_TS AS DATE) >= @date AND c.Oil_Company_Code IN('11','12','13'); " +
        "SELECT CAST(a.completed_ts AS DATE) AS delivery_date, oil_company_code, c.grade_name, " +
        sqlFrom + sqlWhere + "AND c.oil_company_code = '11' " +
        "GROUP BY oil_company_code, CAST(a.completed_ts AS DATE), grade_name " +
        "UNION ALL " +
        "SELECT CAST(a.completed_ts AS DATE) AS delivery_date, '12' AS oil_company_code, " + 
        "'BIO_SOLAR' AS grade_name, " + sqlFrom + sqlWhere + "AND c.oil_company_code IN ('12', '13') " +
        "GROUP BY CAST(a.completed_ts AS DATE) " +
        "ORDER BY CAST(a.completed_ts AS DATE), oil_company_code;";

      using (SqlConnection o_Conn = new SqlConnection(StrConn))
      {
        o_Conn.Open();
        using (SqlCommand o_Cmd = new SqlCommand(sql, o_Conn))
        {
          using (SqlDataReader o_Read = o_Cmd.ExecuteReader())
          {
            if (o_Read != null)
              while (o_Read.Read())
              {
                DateTime dt = DateTime.MinValue;
                if (o_Read["delivery_date"] != DBNull.Value)
                  dt = Convert.ToDateTime(o_Read["delivery_date"]);

                result.Add(new HoseDelivery
                {
                  SiteId = site.SiteId,
                  SiteName = site.SiteName,
                  SiteAddress = site.SiteAddress,
                  DeliveryDate = dt,
                  OilCompanyCode = o_Read["oil_company_code"].ToString().Trim(),
                  GradeName = o_Read["grade_name"].ToString().Trim(),
                  TotalDelivery = Convert.ToInt32(o_Read["total_delivery"]),
                  TotalPrePurchaseDelivery = Convert.ToInt32(o_Read["total_pre"])
                });
              }
          }
        }
      }

      return result;
    }
  }
}

﻿using System;
using System.Web.Http;

using TDMS.Library.Models;

namespace TDMS.Agent.Controllers
{
  public class LoginController : ApiController
  {
    public ApiResponse Post([FromBody]ApiLoginRequest req)
    {
      try
      {
        if (req == null || string.IsNullOrEmpty(req.token))
          return new ApiResponse(402, "error", "Invalid parameter");
        
        if (req.token != ApiHelper.Token || string.IsNullOrEmpty(req.attId) || string.IsNullOrEmpty(req.attPwd))
          return new ApiResponse(403, "error", "Access denied");

        var o_DB = new DBController();
        var att = o_DB.AttendantLogin(req.attId, req.attPwd);
        if (att == null)
          return new ApiResponse(403, "error", "Invalid user id or password");

        return new ApiResponse(200, "success", "", att);
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }
  }
}

﻿using System;
using System.Web.Http;

using TDMS.Library.Models;

namespace TDMS.Agent.Controllers
{
  public class TankController : ApiController
  {
    public ApiResponse Post([FromBody]ApiTankRequest req)
    {
      try
      {
        if (req == null || req.tankNumber <= 0 || string.IsNullOrEmpty(req.token) || string.IsNullOrEmpty(req.sessionId))
          return new ApiResponse(402, "error", "Invalid parameter");

        if (req.token != ApiHelper.Token || !ApiHelper.SessionValidate(req.sessionId))
          return new ApiResponse(403, "error", "Access denied");

        var o_DB = new DBController();
        var tank = o_DB.GetTank(req.tankNumber);
        if (tank == null)
          return new ApiResponse(501, "error", "Can't get tank data");

        return new ApiResponse(200, "success", "", tank);
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }

    [Route("api/tanks")]
    public ApiResponse Post([FromBody]ApiRequest req)
    {
      try
      {
        if (req == null || string.IsNullOrEmpty(req.token) || string.IsNullOrEmpty(req.sessionId))
          return new ApiResponse(402, "error", "Invalid parameter");
        
        if (req.token != ApiHelper.Token || !ApiHelper.SessionValidate(req.sessionId))
          return new ApiResponse(403, "error", "Access denied");

        var o_DB = new DBController();
        var tanks = o_DB.GetTanks();
        if (tanks == null)
          return new ApiResponse(501, "error", "Can't get tanks data");

        return new ApiResponse(200, "success", "", tanks);
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }

    [Route("api/tank/delivery")]
    public ApiResponse Post([FromBody] ApiTankDeliveryRequest req)
    {
      try
      {
        if (req == null || string.IsNullOrEmpty(req.token) || string.IsNullOrEmpty(req.sessionId))
          return new ApiResponse(402, "error", "Invalid parameter");

        if (req.token != ApiHelper.Token || !ApiHelper.SessionValidate(req.sessionId))
          return new ApiResponse(403, "error", "Access denied");

        if (req.deliveryId <= 0)
          return new ApiResponse(501, "error", "Can't get tank delivery data");

        var o_DB = new DBController();
        var del = o_DB.GetTankDelivery(req.deliveryId);
        if (del == null)
          return new ApiResponse(501, "error", "Can't get tank delivery data");

        return new ApiResponse(200, "success", "", del);
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }

    [Route("api/tank/delivery/start")]
    public ApiResponse Post([FromBody] ApiStartTankDeliveryRequest req)
    {
      try
      {
        if (req == null || string.IsNullOrEmpty(req.token) || string.IsNullOrEmpty(req.sessionId))
          return new ApiResponse(402, "error", "Invalid parameter");

        if (req.token != ApiHelper.Token || !ApiHelper.SessionValidate(req.sessionId))
          return new ApiResponse(403, "error", "Access denied");

        if (req.tankId <= 0)
          return new ApiResponse(501, "error", "Can't get tank data");

        var o_DB = new DBController();
        var del = o_DB.StartTankDelivery(req.tankId, req.doNumber, req.doVolume, req.driverName, 
          req.vehicleNumber, req.supplier, req.supplyPoint, req.stock, req.attendantId);
        if (string.IsNullOrEmpty(del))
          return new ApiResponse(501, "error", del);

        return new ApiResponse(200, "success", "");
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }

    [Route("api/tank/delivery/finish")]
    public ApiResponse Post([FromBody] ApiFinishTankDeliveryRequest req)
    {
      try
      {
        if (req == null || string.IsNullOrEmpty(req.token) || string.IsNullOrEmpty(req.sessionId))
          return new ApiResponse(402, "error", "Invalid parameter");

        if (req.token != ApiHelper.Token || !ApiHelper.SessionValidate(req.sessionId))
          return new ApiResponse(403, "error", "Access denied");

        if (req.deliveryId <= 0)
          return new ApiResponse(501, "error", "Can't get tank delivery data");

        var o_DB = new DBController();
        var del = o_DB.FinishTankDelivery(req.tankId, req.deliveryId, req.stock);
        if (string.IsNullOrEmpty(del))
          return new ApiResponse(501, "error", del);

        return new ApiResponse(200, "success", "");
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }

    [Route("api/tank/delivery/void")]
    public ApiResponse Post([FromBody] ApiVoidTankDeliveryRequest req)
    {
      try
      {
        if (req == null || string.IsNullOrEmpty(req.token) || string.IsNullOrEmpty(req.sessionId))
          return new ApiResponse(402, "error", "Invalid parameter");

        if (req.token != ApiHelper.Token || !ApiHelper.SessionValidate(req.sessionId))
          return new ApiResponse(403, "error", "Access denied");

        if (req.deliveryId <= 0)
          return new ApiResponse(501, "error", "Can't get tank delivery data");

        var o_DB = new DBController();
        var del = o_DB.CancelTankDelivery(req.deliveryId);
        if (string.IsNullOrEmpty(del))
          return new ApiResponse(501, "error", del);

        return new ApiResponse(200, "success", "");
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }
  }
}

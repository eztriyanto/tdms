﻿using System;
using System.Web.Http;

using TDMS.Library.Models;

namespace TDMS.Agent.Controllers
{
  public class SessionController : ApiController
  {
    public ApiResponse Post([FromBody]ApiRequest req)
    {
      try
      {
        if (req == null || string.IsNullOrEmpty(req.token) || string.IsNullOrEmpty(req.sessionId))
          return new ApiResponse(402, "error", "Invalid parameter");
        
        if (req.token != ApiHelper.Token)
          return new ApiResponse(403, "error", $"Access denied");

        if (ApiHelper.SessionValidate(req.sessionId))
          return new ApiResponse(200, "success", "");

        return new ApiResponse(403, "error", "Session expired");
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }
  }
}

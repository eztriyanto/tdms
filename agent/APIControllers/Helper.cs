﻿using System;


namespace TDMS.Agent.Controllers
{
  public class ApiHelper
  {
    public static string Token = Environment.GetEnvironmentVariable("TDMS__token");
    public static string StrConn = "";

    public static bool SessionValidate(string sessionId)
    {
      if (!string.IsNullOrEmpty(sessionId))
      {
        var splitId = Library.Controllers.SecurityController.Decrypt(sessionId).Split(new string[] { "#" }, StringSplitOptions.RemoveEmptyEntries);
        if (splitId.Length == 5)
        {
          //Checking Session Expired TimeStamp
          var sessionDT = DateTime.ParseExact(splitId[2], "yyyyssMMmmddHH", null);
          if (sessionDT >= DateTime.Now)
          {
            //Checking User Id & Password
            var o_DB = new DBController();
            var att = o_DB.AttendantLogin(splitId[1], splitId[3]);
            if (att != null)
            {
              return true;
            }
          }
        }
      }
      
      return false;
    }
  }
}

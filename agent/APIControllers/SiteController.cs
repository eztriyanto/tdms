﻿using System;
using System.Web.Http;

using TDMS.Library.Models;

namespace TDMS.Agent.Controllers
{
  public class SiteController : ApiController
  {
    public ApiResponse Post([FromBody]ApiRequest req)
    {
      try
      {
        if (req == null || string.IsNullOrEmpty(req.token) || string.IsNullOrEmpty(req.sessionId))
          return new ApiResponse(402, "error", "Invalid parameter");
        
        if (req.token != ApiHelper.Token || !ApiHelper.SessionValidate(req.sessionId))
          return new ApiResponse(403, "error", "Access denied");

        var o_DB = new DBController();
        var site = o_DB.GetSite();
        if (site == null)
          return new ApiResponse(501, "error", "Can't get site data");

        return new ApiResponse(200, "success", "", site);
      }
      catch (Exception ex)
      {
        return new ApiResponse(500, "error", Library.Helper.ErrorMsg(ex));
      }
    }
  }
}

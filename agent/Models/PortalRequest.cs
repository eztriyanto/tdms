﻿using System;
using System.Collections.Generic;
using TDMS.Library.Models;

namespace TDMS.Agent.Models
{
  public class PortalRequest
  {
    public string token { get => "cf22b969-3e68-4aa5-a888-8a1d525d29f9"; }
  }

  public class TankDeliveryRequest : PortalRequest
  {
    public List<TankDelivery> items { get; set; }

    public TankDeliveryRequest()
    {
      items = new List<TankDelivery>();
    }
  }

  public class HoseDeliveryRequest : PortalRequest
  {
    public List<HoseDelivery> items { get; set; }

    public HoseDeliveryRequest()
    {
      items = new List<HoseDelivery>();
    }
  }
}

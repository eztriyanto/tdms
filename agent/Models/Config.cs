﻿using System;

namespace TDMS.Agent.Models
{
  public class Config
  {
    public string db_instance { get; set; } = ".\\SQLEXPRESS";
    public string db_name { get; set; } = "EnablerDB";
    public string portal_host_name { get; set; } = "tdms.pertamina.com";
    public int portal_port { get; set; } = 5000;
    public bool portal_use_ssl { get; set; } = true;
    public bool portal_enabled { get; set; } = true;
    public int push_timer_in_second { get; set; } = 60;
    public DateTime push_last_date { get; set; } = new DateTime(2019, 10, 01);
  }
}

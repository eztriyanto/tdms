﻿using System;
using System.Collections.Generic;

namespace TDMS.Agent.Models
{
  public class PortalErrorDetail
  {
    public string message { get; set; } = "";
  }

  public class PortalResponse
  {
    public string title { get; set; }
    public int status { get; set; }
    public List<PortalErrorDetail> errors { get; set; } = new List<PortalErrorDetail>();
  }
}

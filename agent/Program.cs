﻿using System;
using System.ServiceProcess;

namespace TDMS.Agent
{
  static class Program
  {
    static void Main()
    {
      ServiceBase[] ServicesToRun;
      ServicesToRun = new ServiceBase[]
      {
        new ClientService()
      };
      ServiceBase.Run(ServicesToRun);
    }
  }
}
